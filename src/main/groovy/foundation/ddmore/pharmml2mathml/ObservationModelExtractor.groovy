/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import foundation.ddmore.pharmml08.dom.commontypes.VariableDefinitionType
import foundation.ddmore.pharmml08.dom.modeldefn.CategoricalDataType
import foundation.ddmore.pharmml08.dom.modeldefn.ContinuousObservationModelType
import foundation.ddmore.pharmml08.dom.modeldefn.CorrelatedRandomVarType
import foundation.ddmore.pharmml08.dom.modeldefn.CorrelationType
import foundation.ddmore.pharmml08.dom.modeldefn.CountDataType
import foundation.ddmore.pharmml08.dom.modeldefn.DiscreteType
import foundation.ddmore.pharmml08.dom.modeldefn.GeneralObsError
import foundation.ddmore.pharmml08.dom.modeldefn.ModelDefinitionType
import foundation.ddmore.pharmml08.dom.modeldefn.ObservationErrorType
import foundation.ddmore.pharmml08.dom.modeldefn.ObservationModelType
import foundation.ddmore.pharmml08.dom.modeldefn.ParameterModelType
import foundation.ddmore.pharmml08.dom.modeldefn.StructuredObsError
import foundation.ddmore.pharmml08.dom.modeldefn.TimeToEventDataType
import foundation.ddmore.pharmml08.dom.trialdesign.ContinuousObservationType
import foundation.ddmore.pharmml08.dom.trialdesign.DiscreteObservationType

/**
 * Created by stumoodie on 05/01/2017.
 */
class ObservationModelExtractor extends BaseParameterModelExtractor{

    final String defaultIdv

    ObservationModelExtractor(String defaultIdv){
        super()
        this.defaultIdv = defaultIdv
    }

    static extractObservationModels(ModelDefinitionType node){
        def walker = new ObservationModelExtractor()
        walker.extractAllDefns(node)
    }

    def extractAllDefns(ModelDefinitionType node){
        visit(node)
    }

    def visit(ModelDefinitionType node){
        def retVal = [ observationModels : [] ]
        for(def cm : node.getObservationModel()){
            retVal.observationModels.add(visit(cm))
        }
        retVal
    }


    def visit(ObservationModelType node){
        def retVal = [
                continuousObs : null,
                discreteObs : null
        ]
        if(node.continuousData){
            retVal.continuousObs = visit(node.continuousData)
        }
        else if(node.discrete){
            retVal.discreteObs = visit(node.discrete)
        }
        retVal.blkId = node.blkId
        retVal
    }


    def visit(ContinuousObservationModelType node) {
        def retVal = [
                parameters : [],
                correlations : []
        ]
        if (node.getCommonParameterElementAndAssignStatementAndConditionalStatement()) {
            node.getCommonParameterElementAndAssignStatementAndConditionalStatement().each {p->
                if(p.value instanceof CorrelationType){
                    retVal.correlations.add(visit(p))
                }
                else{
                    retVal.parameters.add(visit(p))
                }
            }
        }
        retVal.observation = visit(node.observationError)
        retVal
    }


    def visit(StructuredObsError node){
        def retVal = [ : ]
        retVal.symbId = node.symbId
        retVal.maths = ObservationErrorExtractor.extractObservationError(node)
        retVal.type = 'structured'


        retVal
    }


    def visit(GeneralObsError node){
        def retVal = [ : ]
        retVal.symbId = node.symbId
        retVal.maths = ObservationErrorExtractor.extractObservationError(node)
        retVal.type = 'general'
        retVal
    }

    def visit(DiscreteType node) {
        def retVal = [
                parameters : [],
                correlations : []
        ]

        if(node.categoricalData){
            retVal = visit(node.categoricalData)
        }
        else if(node.countData){
            retVal = visit(node.countData)
        }
        else if(node.timeToEventData){
            retVal = visit(node.timeToEventData)
        }

        retVal
    }

    def visit(CountDataType node){
        def retVal = [
                parameters : [],
                correlations : [],
                observation : [ : ]
        ]
        if (node.getCommonParameterElementAndAssignStatementAndConditionalStatement()) {
            node.getCommonParameterElementAndAssignStatementAndConditionalStatement().each {p->
                if(p.value instanceof CorrelationType){
                    retVal.correlations.add(visit(p))
                }
                else{
                    retVal.parameters.add(visit(p))
                }
            }
        }

        retVal.observation.symbId = node.countVariable.symbId
        retVal.observation.maths = ObservationErrorExtractor.extractCountObservation(node)
        retVal.observation.type = "count"

        retVal
    }

    def visit(CategoricalDataType node){
        def retVal = [
                parameters : [],
                correlations : [],
                observation : [ : ]
        ]
        if (node.getCommonParameterElementAndAssignStatementAndConditionalStatement()) {
            node.getCommonParameterElementAndAssignStatementAndConditionalStatement().each {p->
                if(p.value instanceof CorrelationType){
                    retVal.correlations.add(visit(p))
                }
                else{
                    retVal.parameters.add(visit(p))
                }
            }
        }

        retVal.observation.symbId = node.categoryVariable.symbId
        retVal.observation.maths = ObservationErrorExtractor.extractCategoricalObservation(node)
        retVal.observation.type = "categorical"

        retVal
    }

    def visit(TimeToEventDataType node){
        def retVal = [
                parameters : [],
                correlations : [],
                observation : [ : ]
        ]
        if (node.getCommonParameterElementAndAssignStatementAndConditionalStatement()) {
            node.getCommonParameterElementAndAssignStatementAndConditionalStatement().each {p->
                if(p.value instanceof CorrelationType){
                    retVal.correlations.add(visit(p))
                }
                else{
                    retVal.parameters.add(visit(p))
                }
            }
        }

        retVal.observation.symbId = node.eventVariable.symbId
        retVal.observation.maths = ObservationErrorExtractor.extractTteHazardFuncn(node)
        retVal.observation.type = "tte"

        retVal
    }

    def visit(VariableDefinitionType node){
        def retVal = [:]
        retVal.symbId = node.symbId
        retVal.maths = CommonVariableDefinitionExtractor.extractCommonVarDefn(node, defaultIdv)
        retVal.type = 'varDefn'
        retVal
    }

}
