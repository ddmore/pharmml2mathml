/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import foundation.ddmore.pharmml08.dom.commontypes.LevelReferenceType
import foundation.ddmore.pharmml08.dom.modeldefn.*
import foundation.ddmore.pharmml08.dom.modeldefn.StructuredObsError.ResidualError

import javax.xml.bind.JAXBElement

/**
 * Created by stumoodie on 05/01/2017.
 */
class ObservationErrorExtractor extends DistributionExtractor {

    ObservationErrorExtractor() {
        super()
    }

    static String extractObservationError(ObservationErrorType node) {
        def walker = new ObservationErrorExtractor()
        walker.extract(node)
    }

    static String extractCountObservation(CountDataType node) {
        def walker = new ObservationErrorExtractor()
        walker.extract(node)
    }

    static String extractCategoricalObservation(CategoricalDataType node) {
        def walker = new ObservationErrorExtractor()
        walker.extract(node)
    }


    static String extractTteHazardFuncn(TimeToEventDataType node) {
        def walker = new ObservationErrorExtractor()
        walker.extract(node)
    }


    String extract(ObservationErrorType node) {
        def covDefn = builder.bind {
            mrow {
                visitClosure(visit(node, node), delegate)
            }
        }

        covDefn.toString()
    }


    String extract(CountDataType node) {
        def covDefn = builder.bind {
            mrow {
                visitClosure(visit(node, node), delegate)
            }
        }

        covDefn.toString()
    }

    String extract(CategoricalDataType node) {
        def covDefn = builder.bind {
            mrow {
                visitClosure(visit(node, node), delegate)
            }
        }

        covDefn.toString()
    }

    String extract(TimeToEventDataType node) {
        def covDefn = builder.bind {
            mrow {
                visitClosure(visit(node, node), delegate)
            }
        }

        covDefn.toString()
    }


    def visit(StructuredObsError node, parent) {
        return {
            if(node.transformation){
                visitClosure(visitTransform(node.transformation, {
                    mi(node.symbId)
                }), delegate)
            }
            else{
                mi(node.symbId)
            }
            mo("=")
            visitClosure(visit(node.output, node.transformation), delegate)
            mo("+")
            visitClosure(visit(node.errorModel, node), delegate)
            mo("+")
            visitClosure(visit(node.residualError, node), delegate)
        }
    }

    def visit(StructuredObsError.Output node, transform){
        return {
            def nameClosure = visit(node.symbRef, node)
            if(transform){
                visitClosure(visitTransform(transform, nameClosure), delegate)
            }
            else {
                visitClosure(nameClosure, delegate)
            }
        }
    }


    def visit(StructuredObsError.ErrorModel node, parent){
        return{
            visitClosure(visit(node.assign, node), delegate)
        }
    }


    def visit(ResidualError node, parent){
        return{
            visitClosure(visit(node.symbRef, node), delegate)
        }
    }


    def visit(GeneralObsError node, parent) {
        return {
            if(node.LHSTransformation){
                visitClosure(visitTransform(node.LHSTransformation, {
                    mi(node.symbId)
                }), delegate)
            }
            else {
                mi(node.symbId)
            }
            if(node.assign){
                mo("=")
                visitClosure(visit(node.assign, node), delegate)
            }
            else{
                mo("~")
                visitClosure(visit(node.distribution, node), delegate)
            }
        }
    }

    def visitTransform(node, Closure nameClosure){
        return {
            String opName = ""
            switch (node.type) {
                case TransformationType.BOX_COX:
                    opName = "BoxCox"
                    break
                case TransformationType.LOG:
                    opName = "ln"
                    break
                case TransformationType.LOGIT:
                    opName = "logit"
                    break
                case TransformationType.PROBIT:
                    opName = "probit"
                    break
            }
            if (opName) {
                visitClosure(functionCall(opName, [nameClosure]), delegate)
            }
            else {
                visitClosure(nameClosure, delegate)
            }
        }
    }


    def visit(CountDataType node, parent){
        return {
            mi(node.countVariable.symbId)
            mo("~")
            if(node.getPMF()){
                if(node.getPMF().head().distribution){
                    visitClosure(visit(node.getPMF().head().distribution, node), delegate)
                }
            }
        }
    }


    def visit(CategoricalDataType node, parent){
        return {
            mi(node.categoryVariable.symbId)
            mo("~")
            if(node.getPMF()){
                if(node.getPMF().head().distribution){
                    visitClosure(visit(node.getPMF().head().distribution, node), delegate)
                }
            }
        }
    }

    def visit(TimeToEventDataType node, parent){
        return {
            if(node.getHazardFunction()){
                def haz = node.getHazardFunction().head()
                mi(haz.symbId)
                if(haz.distribution){
                    mo("~")
                    visitClosure(visit(haz.distribution, node), delegate)
                }
                else if(haz.assign){
                    mo("=")
                    visitClosure(visit(haz.assign, node), delegate)
                }
            }
        }
    }

}
