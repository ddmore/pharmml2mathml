/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import foundation.ddmore.pharmml08.dom.maths.UnaryOperator
import foundation.ddmore.pharmml08.dom.modeldefn.CovariateType
import foundation.ddmore.pharmml08.dom.probonto.DistroNameType
import foundation.ddmore.pharmml08.dom.probonto.DistroType
import foundation.ddmore.pharmml08.dom.probonto.ParameterNameType
import foundation.ddmore.pharmml08.builder.CovariateModelBuilder
import foundation.ddmore.pharmml08.builder.MathsBuilder
import foundation.ddmore.pharmml08.builder.PharmMlBuilder
import foundation.ddmore.pharmml08.builder.ProbontoBuilder
import org.junit.Test

import static foundation.ddmore.pharmml2mathml.PharmMlUtils.getCategoricalCovariates
import static foundation.ddmore.pharmml2mathml.PharmMlUtils.getContinuousCovariates
import static foundation.ddmore.pharmml08.builder.CovariateModelBuilder.*
import static foundation.ddmore.pharmml08.builder.MathsBuilder.*
import static foundation.ddmore.pharmml08.builder.PharmMlBuilder.createModelDefinition
import static foundation.ddmore.pharmml08.builder.ProbontoBuilder.createDistnParameter
import static foundation.ddmore.pharmml08.builder.ProbontoBuilder.createProbonto
import static org.junit.Assert.assertEquals

/**
 * Created by stumoodie on 05/01/2017.
 */
class CovariateModelExtractorTest {


    @Test
    void testSingleSimpleDefinition(){
        def root = PharmMlBuilder.createModelDefinition(
                CovariateModelBuilder.createCovariateModel("cm",
                        CovariateModelBuilder.createCovariateDefinition(
                                "W",
                                CovariateModelBuilder.createContinuousCovariate()
                        ),
                )
        )

        def actualResult = CovariateModelExtractor.extractCovariateModels(root)
        assertEquals(1, actualResult.covariateModels.size())
        assertEquals("cm", actualResult.covariateModels.get(0).blkId)
        def defnList = actualResult.covariateModels.get(0)
        def expectedDefns = [
                [
                        symbId : "W",
                        maths : "<mrow><mi>W</mi></mrow>",
                        type : "continuous",
                        varType : CovariateType.CONSTANT
                ]
        ]
        assertEquals(expectedDefns.size(), defnList.covariates.size())
        assertEquals(0, defnList.parameters.size())
        for(int i; i < expectedDefns.size(); i++){
            assert expectedDefns[i] == defnList.covariates.get(i)
        }
    }


    @Test
    void testMultipleDefinitions(){
        def root = PharmMlBuilder.createModelDefinition(CovariateModelBuilder.createCovariateModel("cm",
                CovariateModelBuilder.createCovariateDefinition(
                        "W",
                        CovariateModelBuilder.createContinuousCovariate()
                ),
                CovariateModelBuilder.createCovariateDefinition(
                        "Y",
                        CovariateModelBuilder.createContinuousCovariate(
                                MathsBuilder.createRhs(
                                        MathsBuilder.createReal(0)
                                )
                        )
                ),
                CovariateModelBuilder.createCovariateDefinition(
                        "Z",
                        CovariateModelBuilder.createContinuousCovariate(
                                createDistribution(
                                        ProbontoBuilder.createProbonto(DistroNameType.NORMAL_1, DistroType.UNIVARIATE,
                                                ProbontoBuilder.createDistnParameter(ParameterNameType.MEAN, MathsBuilder.createRhs(
                                                        MathsBuilder.createReal(0)
                                                )),
                                                ProbontoBuilder.createDistnParameter(ParameterNameType.STDEV, MathsBuilder.createRhs(
                                                        MathsBuilder.createReal(1)
                                                ))
                                        )
                                )
                        )
                )
        )
        )

        def defnList = CovariateModelExtractor.extractCovariateModels(root).covariateModels.get(0)
        def expectedDefns = [
                "<mrow><mi>W</mi></mrow>",
                "<mrow><mi>Y</mi><mo>=</mo><mn>0.0</mn></mrow>",
                "<mrow><mi>Z</mi><mo>~</mo><mrow><mi>Normal1</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>mean</mi><mo>=</mo><mn>0.0</mn></mrow><mrow><mi>stdev</mi><mo>=</mo><mn>1.0</mn></mrow></mfenced></mrow></mrow>"
        ]
        assertEquals(expectedDefns.size(), defnList.covariates.size())
        for(int i; i < expectedDefns.size(); i++){
            assertEquals(expectedDefns[i], defnList.covariates.get(i).maths)
        }
    }

    @Test
    void testMultipleModels(){
        def root = PharmMlBuilder.createModelDefinition(
                CovariateModelBuilder.createCovariateModel("cm",
                        CovariateModelBuilder.createCovariateDefinition(
                                "W",
                                CovariateModelBuilder.createContinuousCovariate()
                        ),
                        CovariateModelBuilder.createCovariateDefinition(
                                "Y",
                                CovariateModelBuilder.createContinuousCovariate(
                                        MathsBuilder.createRhs(
                                                MathsBuilder.createReal(0)
                                        )
                                )
                        ),
                        CovariateModelBuilder.createCovariateDefinition(
                                "Z",
                                CovariateModelBuilder.createContinuousCovariate(
                                        createDistribution(
                                                ProbontoBuilder.createProbonto(DistroNameType.NORMAL_1, DistroType.UNIVARIATE,
                                                        ProbontoBuilder.createDistnParameter(ParameterNameType.MEAN, MathsBuilder.createRhs(
                                                                MathsBuilder.createReal(0)
                                                        )),
                                                        ProbontoBuilder.createDistnParameter(ParameterNameType.STDEV, MathsBuilder.createRhs(
                                                                MathsBuilder.createReal(1)
                                                        ))
                                                )
                                        )
                                )
                        )
                )
        )

        def defnList = CovariateModelExtractor.extractCovariateModels(root).covariateModels
        assertEquals(1, defnList.size())
        def expectedDefns = [
                "<mrow><mi>W</mi></mrow>",
                "<mrow><mi>Y</mi><mo>=</mo><mn>0.0</mn></mrow>",
                "<mrow><mi>Z</mi><mo>~</mo><mrow><mi>Normal1</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>mean</mi><mo>=</mo><mn>0.0</mn></mrow><mrow><mi>stdev</mi><mo>=</mo><mn>1.0</mn></mrow></mfenced></mrow></mrow>"
        ]
        for(int i; i < expectedDefns.size(); i++){
            assertEquals(expectedDefns[i], defnList[0].covariates.get(i).maths)
        }
    }

    @Test
    void testModelWithCovTransformation(){
        def root = createModelDefinition(
                createCovariateModel("cm",
                        createCovariateDefinition(
                                "W",
                                createContinuousCovariate(null,
                                    [createTransformedCovariate(
                                            'logW',
                                            createRhs(
                                                    createUniop(
                                                            UnaryOperator.LOG,
                                                            createSymbRef("W")
                                                    )
                                            )
                                    )]
                                )
                        ),
                )
        )

        def defnList = CovariateModelExtractor.extractCovariateModels(root).covariateModels.get(0)
        def expectedDefns = [
                "<mrow><mi>W</mi></mrow>",
                "<mrow><mi>logW</mi><mo>=</mo><mi>ln</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>W</mi></mrow></mfenced></mrow>"
        ]
        assertEquals(expectedDefns.size(), defnList.covariates.size())
        for(int i; i < expectedDefns.size(); i++){
            assertEquals(expectedDefns[i], defnList.covariates.get(i).maths)
        }
    }

    @Test
    void testMultipleContCatModels(){
        def root = createModelDefinition(
                createCovariateModel("cm",
                        createCovariateDefinition(
                                "W",
                                createContinuousCovariate()
                        ),
                        createCovariateDefinition(
                                "S",
                                createCategoricalCovariate(
                                        createCategory('a'),
                                        createCategory('b'),
                                        createCategory('c')
                                )
                        ),
                )
        )

        def defnList = CovariateModelExtractor.extractCovariateModels(root).covariateModels
        assertEquals(1, defnList.size())
        def catCovs = getCategoricalCovariates(defnList[0])
        def contCovs = getContinuousCovariates(defnList[0])
        def expectedDefns = [
                [
                    symbId : "W",
                    maths : "<mrow><mi>W</mi></mrow>",
                ]
        ]
        def expectedCatDefns = [
                [
                        symbId : "S",
                        categories : "a, b, c"
                ]
        ]
        for(int i; i < expectedDefns.size(); i++){
            assertEquals(expectedDefns[i].symbId, contCovs.get(i).symbId)
            assertEquals(expectedDefns[i].maths, contCovs.get(i).maths)
        }
        for(int i; i < expectedCatDefns.size(); i++){
            assertEquals(expectedCatDefns[i].symbId, catCovs.get(i).symbId)
            assertEquals(expectedCatDefns[i].categories, catCovs.get(i).categories.join(", "))
        }
    }

}
