/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import foundation.ddmore.pharmml08.dom.maths.BinaryOperator
import foundation.ddmore.pharmml08.dom.maths.LogicalBinOperator
import foundation.ddmore.pharmml08.dom.maths.LogicalUniOperator
import foundation.ddmore.pharmml08.dom.maths.UnaryOperator

/**
 * Created by stumoodie on 13/05/2017.
 */
class OperatorComparator implements Comparator<Object> {

    /*
    Predence
        2   Unary ops  +, -, NOT, ! (factorial)
        3   Exponents: ^, root, exp(x), sqrt(x)
        4   *, /, %
        5   + -
        6   Relations: <, >, <=, >=
        7   Equality: ==, !=
        8   AND
        9   OR
        10   Functions, eg tan, sin, min, max, |abs|,

     */

    OperatorComparator(){
//        PRECEDENCE_TABLE.put(BinaryOperator.PLUS, PrecedenceLevels.L5.ordinal())
//        BinaryOperator.MINUS :  PrecedenceLevels.L5.ordinal(),
//        BinaryOperator.DIVIDE : PrecedenceLevels.L4.ordinal(),
    }

    @Override
    int compare(Object o1, Object o2) {
        return opPrecedenceComparator(o1, o2)
    }

    static enum PrecedenceLevels{
        L2(10), L3(20), L4(30), L5(40), L6(50), L7(60), L8(70), L9(80), L10(90)

        final int precedence

        PrecedenceLevels(int prec){
            this.precedence = prec
        }
    }

    final def Map PRECEDENCE_TABLE = [
            (BinaryOperator.PLUS) : PrecedenceLevels.L5.precedence,
            (BinaryOperator.MINUS) :  PrecedenceLevels.L5.precedence,
            (BinaryOperator.DIVIDE) : PrecedenceLevels.L4.precedence,
            (BinaryOperator.TIMES) : PrecedenceLevels.L4.precedence,
            (BinaryOperator.ROOT) : PrecedenceLevels.L3.precedence,
            (BinaryOperator.LOGX) : PrecedenceLevels.L10.precedence,
            (BinaryOperator.MIN) : PrecedenceLevels.L10.precedence,
            (BinaryOperator.LOGX) : PrecedenceLevels.L10.precedence,
            (BinaryOperator.ATAN_2) : PrecedenceLevels.L10.precedence,
            (BinaryOperator.MAX) : PrecedenceLevels.L10.precedence,
            (BinaryOperator.POWER) : PrecedenceLevels.L3.precedence,
            (BinaryOperator.REM) : PrecedenceLevels.L4.precedence,
            (UnaryOperator.ABS) : PrecedenceLevels.L10.precedence,
            (UnaryOperator.EXP) : PrecedenceLevels.L3.precedence,
            (UnaryOperator.MINUS) : PrecedenceLevels.L2.precedence,
            (UnaryOperator.SQRT) : PrecedenceLevels.L3.precedence,
            (UnaryOperator.FACTORIAL) : PrecedenceLevels.L2.precedence,
            (UnaryOperator.FACTLN) : PrecedenceLevels.L2.precedence,
            (LogicalUniOperator.NOT) : PrecedenceLevels.L2.precedence,
            (LogicalBinOperator.LEQ) : PrecedenceLevels.L6.precedence,
            (LogicalBinOperator.GEQ) : PrecedenceLevels.L6.precedence,
            (LogicalBinOperator.LT) : PrecedenceLevels.L6.precedence,
            (LogicalBinOperator.GT) : PrecedenceLevels.L6.precedence,
            (LogicalBinOperator.EQ) : PrecedenceLevels.L7.precedence,
            (LogicalBinOperator.NEQ) : PrecedenceLevels.L7.precedence,
            (LogicalBinOperator.AND) : PrecedenceLevels.L8.precedence,
            (LogicalBinOperator.OR) : PrecedenceLevels.L9.precedence,
    ]


    /**
     * Compare operators based on their precedence
     * @param thisOp
     * @param thatOp
     * @return  0 if they are the same precedence, -1 if thisOp has lower prec that thatOp, 1 otherwise
     */
    def int opPrecedenceComparator(thisOp, thatOp){
        assert isValidOp(thisOp)
        assert isValidOp(thatOp)

        def thisPrec = PRECEDENCE_TABLE.get(thisOp) ?: PrecedenceLevels.L10.precedence
        def thatPrec = PRECEDENCE_TABLE.get(thatOp) ?: PrecedenceLevels.L10.precedence

        // not that priority is in ascending order
        return thisPrec > thatPrec ? -1 : thisPrec < thatPrec ? 1 : 0
    }

    private def isValidOp(op){
        (op instanceof BinaryOperator || op instanceof UnaryOperator || op instanceof LogicalUniOperator
            || op instanceof LogicalBinOperator)
    }


}
