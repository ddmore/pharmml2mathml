/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import foundation.ddmore.pharmml08.dom.modeldefn.CorrelationType
import foundation.ddmore.pharmml08.dom.modeldefn.ModelDefinitionType
import foundation.ddmore.pharmml08.dom.modeldefn.ParameterModelType

/**
 * Created by stumoodie on 05/01/2017.
 */
class ParameterModelExtractor extends BaseParameterModelExtractor{

    ParameterModelExtractor(){
        super()
    }

    static extractParametersModels(ModelDefinitionType node){
        def walker = new ParameterModelExtractor()
        walker.extractAllDefns(node)
    }

    def extractAllDefns(ModelDefinitionType node){
        visit(node)
    }

    def visit(ModelDefinitionType node){
        def retVal = [ parameterModels : [] ]
        for(def cm : node.getParameterModel()){
            retVal.parameterModels.add(visit(cm))
        }
        retVal
    }



    def visit(ParameterModelType node){
        def retVal = [
                parameters : [],
                correlations : []
        ]
        node.getConditionalStatementAndCommonParameterElementAndAssignStatement().each {n->
            if(n.value instanceof CorrelationType){
                retVal.correlations.add(visit(n))
            }
            else{
                retVal.parameters.add(visit(n))
            }
        }
        retVal.blkId = node.blkId
        retVal
    }

}
