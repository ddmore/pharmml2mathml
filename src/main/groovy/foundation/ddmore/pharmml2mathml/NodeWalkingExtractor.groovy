/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import groovy.xml.StreamingMarkupBuilder

import javax.swing.tree.TreeNode

/**
 * Created by stumoodie on 05/01/2017.
 */
class NodeWalkingExtractor {
    final StreamingMarkupBuilder builder

    NodeWalkingExtractor() {
        this.builder = new StreamingMarkupBuilder()
        builder.setEncoding('UTF-8')
        builder.setUseDoubleQuotes(true)
    }


    def visitChildren(TreeNode node) {
        return {
            for (child in node.children()) {
                visitClosure(visit(child), delegate)
            }
        }
    }

    def visit(Object node) {
        return {
            visitClosure(visitChildren(node), delegate)
        }
    }


    protected visitClosure(Closure vc, delegate){
        vc.delegate = delegate
        vc.call()
    }
}
