/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import foundation.ddmore.pharmml08.dom.maths.BinaryOperator
import foundation.ddmore.pharmml08.dom.modeldefn.AbsorptionOralMacroArgumentType
import org.junit.Test

import static foundation.ddmore.pharmml08.builder.MathsBuilder.*
import static foundation.ddmore.pharmml08.builder.ParameterModelBuilder.createPopulationParam
import static foundation.ddmore.pharmml08.builder.PharmMlBuilder.createModelDefinition
import static foundation.ddmore.pharmml08.builder.StructuralModelBuilder.*
import static org.junit.Assert.assertEquals

/**
 * Created by stumoodie on 05/01/2017.
 */
class StructuralModelExtractorTest {


    @Test
    void testSingleSimpleDefinition(){
        def root = createModelDefinition(
                createStructuralModel("sm",
                    createVariable(
                            "W"
                    )
            )
        )

        def actualResult = StructuralModelExtractor.extractStructuralModels(root, "t")
        assertEquals(1, actualResult.structuralModels.size())
        assertEquals("sm", actualResult.structuralModels.get(0).blkId)
        def defnList = actualResult.structuralModels.get(0)
        def expectedDefns = [
                [
                        symbId : "W",
                        maths : "<mrow><mi>W</mi></mrow>",
                        type : "varDefn",
                ]
        ]
        assertEquals(expectedDefns.size(), defnList.variables.size())
        for(int i; i < expectedDefns.size(); i++){
            assert expectedDefns[i].symbId == defnList.variables.get(i).symbId
            assert expectedDefns[i].maths == defnList.variables.get(i).maths
            assert expectedDefns[i].varDefn == defnList.variables.get(i).varDefn
        }
    }


    @Test
    void testMultipleDefinitions(){
        def root = createModelDefinition(createStructuralModel("sm",
                createPopulationParam(
                        "pop"
                ),
                createVariable("v",
                        createRhs(
                                createBinop(BinaryOperator.TIMES,
                                        createSymbRef("pm", "a"),
                                        createSymbRef("b")
                                )
                        )
                ),
                createDerivative("y",
                        createRhs(
                                createBinop(BinaryOperator.TIMES,
                                        createSymbRef("v"),
                                        createSymbRef("y")
                                )
                        ),
                        createRhs(createInt(0)),
                        createRhs(createInt(0)),
                        "T"
                ),
                createPkMacro(
                        createAbsorptionMacro([AbsorptionOralMacroArgumentType.TLAG], [createRhs(createSymbRef("t0"))])
                )
        )
        )

        def defnList = StructuralModelExtractor.extractStructuralModels(root, "t")
        assertEquals(1, defnList.structuralModels.size())
        assertEquals("sm", defnList.structuralModels.get(0).blkId)
        assertEquals(1, defnList.structuralModels[0].parameters.size())
        assertEquals(2, defnList.structuralModels[0].variables.size())
        assertEquals(1, defnList.structuralModels[0].pkmacros.size())

        def expectedDefns = [
                [
                    symbId : "pop",
                    maths : "<mrow><mi>pop</mi></mrow>",
                    type : "popParam"
                ],
        ]

        for(int i; i < expectedDefns.size(); i++){
            assertEquals(expectedDefns[i].symbId, defnList.structuralModels[0].parameters.get(i).symbId)
            assertEquals(expectedDefns[i].maths, defnList.structuralModels[0].parameters.get(i).maths)
            assertEquals(expectedDefns[i].type, defnList.structuralModels[0].parameters.get(i).type)
        }

        def expectedVarDefns = [
                [
                        symbId : "v",
                        maths : "<mrow><mi>v</mi><mo>=</mo><mi>pm.a</mi><mo>&#x022C5;</mo><mi>b</mi></mrow>",
                        type : "varDefn"
                ],
                [
                        symbId : "y",
                        maths : "<mrow><mtable groupalign=\"{left}\"><mtr><mtd><mrow><mfrac><mrow><mo>&DifferentialD;</mo></mrow><mrow><mo>&DifferentialD;</mo><mi>T</mi></mrow></mfrac><mi>y</mi><mrow><maligngroup/><mo>=</mo><mi>v</mi><mo>&#x022C5;</mo><mi>y</mi></mrow></mrow></mtd></mtr><mtr><mtd><mrow><mi>y</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>T</mi><mo>=</mo><mn>0</mn></mrow></mfenced><maligngroup/><mo>=</mo><mn>0</mn></mrow></mtd></mtr></mtable></mrow>",
                        type : "derivDefn"
                ]
        ]
        for(int i; i < expectedVarDefns.size(); i++){
            assertEquals(expectedVarDefns[i].symbId, defnList.structuralModels[0].variables.get(i).symbId)
            assertEquals(expectedVarDefns[i].maths, defnList.structuralModels[0].variables.get(i).maths)
            assertEquals(expectedVarDefns[i].type, defnList.structuralModels[0].variables.get(i).type)
        }

        def expectedPkDefns = [
                [
                        maths : "<mrow><mi>absorption</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>TLAG</mi><mo>=</mo><mi>t0</mi></mrow></mfenced></mrow>",
                        type : "absorption"
                ]
        ]
        for(int i; i < expectedPkDefns.size(); i++){
            assertEquals(expectedPkDefns[i].maths, defnList.structuralModels[0].pkmacros.get(i).maths)
            assertEquals(expectedPkDefns[i].type, defnList.structuralModels[0].pkmacros.get(i).type)
        }
    }

    @Test
    void testMultipleModels(){
        def root = createModelDefinition(
                 createStructuralModel("sm1",
                        createPopulationParam(
                                "W"
                        ),
                ),
                createStructuralModel("sm2",
                        createPopulationParam(
                                "Y"
                        ),
                )
        )

        def defnList = StructuralModelExtractor.extractStructuralModels(root, "t")
        assertEquals(2, defnList.structuralModels.size())
        def expectedDefns = [
                "<mrow><mi>W</mi></mrow>",
                "<mrow><mi>Y</mi></mrow>",
        ]
        for(int i; i < expectedDefns.size(); i++){
            assertEquals(expectedDefns[i], defnList.structuralModels[i].parameters.get(0).maths)
        }
    }
}
