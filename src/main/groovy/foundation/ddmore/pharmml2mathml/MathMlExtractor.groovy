/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import foundation.ddmore.pharmml08.dom.commontypes.*
import foundation.ddmore.pharmml08.dom.dataset.ColumnRefType
import foundation.ddmore.pharmml08.dom.maths.*
import groovy.xml.StreamingMarkupBuilder
import org.slf4j.LoggerFactory

import javax.xml.bind.JAXBElement

/**
 * Created by stumoodie on 03/01/2017.
 */
class MathMlExtractor {
    static final def LOGGER = LoggerFactory.getLogger(MathMlExtractor.class)

    static TIMES_OP = "&#x022C5;"
    static APPLY_FUNCTION = "&ApplyFunction;"
    final StreamingMarkupBuilder builder
    final OperatorComparator opCmp

    MathMlExtractor() {
        this.builder = new StreamingMarkupBuilder()
        this.opCmp = new OperatorComparator()
        builder.setEncoding('UTF-8')
        builder.setUseDoubleQuotes(true)
    }

//    static String extract(JAXBElement<?> node, boolean mathsTag=true) {
//        def walker = new MathMlExtractor()
//        walker.extractMathMl(node, mathsTag)
//    }

    static String extract(def node, boolean mathsTag=true) {
        def walker = new MathMlExtractor()
        walker.extractMathMl(node, mathsTag)
    }

//    String extractMathMl(JAXBElement<?> node, boolean mathsTag) {
//        def mathmlDoc = builder.bind{
//            if(mathsTag) {
//                math(xmlns: "http://www.w3.org/1998/Math/MathML") {
//                    visitClosure(visit(node), delegate)
//                }
//            }
//            else{
//                visitClosure(visit(node), delegate)
//            }
//        }
//
//        mathmlDoc.toString()
//    }

    String extractMathMl(def node, boolean mathsTag) {
        def mathmlDoc = builder.bind{
            if(mathsTag) {
                math(xmlns: "http://www.w3.org/1998/Math/MathML") {
                    visitClosure(visit(node, node), delegate)
                }
            }
            else{
                visitClosure(visit(node, node), delegate)
            }
        }

        mathmlDoc.toString()
    }

    def visitClosure(Closure vc, delegate){
        LOGGER.trace("Appling visit closure:" + vc)
        vc.delegate = delegate
        vc.call()
    }

    def visit(RhsType node, parent) {
        LOGGER.debug("Visiting node:" + node + ", parent=" + parent)
        return{
            visitClosure(visit(node.getExpression(), node), delegate)
        }
    }

    def visit(JAXBElement<?> node, parent){
        LOGGER.debug("Visiting node:" + node + ", parent=" + parent)
        return{
            visitClosure(visit(node.getValue(), parent), delegate)
        }
    }

    def visit(IntValueType node, parent) {
        LOGGER.debug("Visiting node:" + node + ", parent=" + parent)
        return {
            mn(node.value)
        }
    }

    def visit(RealValueType node, parent) {
        LOGGER.debug("Visiting node:" + node + ", parent=" + parent)
        return {
            mn(node.value)
        }
    }

    def visit(StringValueType node, parent) {
        LOGGER.debug("Visiting node:" + node + ", parent=" + parent)
        return {
            mtext{
                mkp.yieldUnescaped node.value
            }
        }
    }


    def visit(TrueBooleanType node, parent) {
        LOGGER.debug("Visiting node:" + node + ", parent=" + parent)
        return {
            mi("T")
        }
    }


    def visit(FalseBooleanType node, parent) {
        LOGGER.debug("Visiting node:" + node + ", parent=" + parent)
        return {
            mi("F")
        }
    }


    def visit(ColumnRefType node, parent){
        LOGGER.debug("Visiting node:" + node + ", parent=" + parent)
        return {
            mi node.columnIdRef
        }
    }

    def visit(SymbolRefType node, parent) {
        LOGGER.debug("Visiting node:" + node + ", parent=" + parent)
        return {
            def buf = new StringBuilder()
            if(node.blkIdRef != null){
                buf.append(node.blkIdRef)
                buf.append(".")
            }
            buf.append(node.symbIdRef)
            mi(buf.toString())
        }
    }


    def visit(BinopType node, parent) {
        LOGGER.debug("Visiting node:" + node + ", parent=" + parent)
        if (node.op == BinaryOperator.POWER) {
            return {
                msup {
                    mrow {
                        visitClosure(visit(node.expression[0], node), delegate)
                    }
                    mrow {
                        visitClosure(visit(node.expression[1], node), delegate)
                    }
                }
            }
        } else if (node.op == BinaryOperator.DIVIDE) {
            return{
                mfrac {
                    mrow {
                        visitClosure(visit(node.expression[0], node), delegate)
                    }
                    mrow {
                        visitClosure(visit(node.expression[1], node), delegate)
                    }
                }
            }
        } else if (node.op == BinaryOperator.ROOT) {
            return {
                mroot() {
                    mrow {
                        visitClosure(visit(node.expression[0], node), delegate)
                    }
                    mrow {
                        visitClosure(visit(node.expression[1], node), delegate)
                    }
                }
            }
        } else {
            return {
                def baseExpreClosure = {
                    visitClosure(visit(node.expression[0], node), delegate)
                    mo {
                        def symb = opSymbol(node.op)
                        mkp.yieldUnescaped symb
                    }
                    visitClosure(visit(node.expression[1], node), delegate)
                }
                // if lower precedence to parent then add paren
                if(isParentPrecedenceHigher(node, parent)){
                    mfenced{
                        mrow {
                            baseExpreClosure.call()
                        }
                    }
                }
                else{
                    baseExpreClosure.call()
                }
            }
        }
    }

    def boolean isParentPrecedenceHigher(node, parent){
        def op1 = null
        def op2 = null
        switch(node.class){
            case BinopType.class:
            case UniopType.class:
            case LogicUniOpType.class:
            case LogicBinOpType.class:
                op1 = node.op
                break;
        }
        switch(parent.class){
            case BinopType.class:
            case UniopType.class:
            case LogicUniOpType.class:
            case LogicBinOpType.class:
                op2 = parent.op
                break;
        }
        if(op1 && op2)
            opCmp.compare(op1, op2) < 0
        else
            false
    }

    def visit(VectorSelectorType node, parent){
        LOGGER.debug("Visiting node:" + node + ", parent=" + parent)
        return{
            mrow{
                visitClosure(visit(node.symbRef, node), delegate)
                mo{
                    mkp.yieldUnescaped APPLY_FUNCTION
                }
                mfenced{
                    mrow{
                        node.getCellOrSegment().each {cs->
                            visitClosure(visit(cs, node), delegate)
                        }
                    }
                }
            }
        }
    }


    def visit(VectorType node, parent){
        LOGGER.debug("Visiting node:" + node + ", parent=" + parent)
        return {
            mrow{
                mfenced{
                    visitClosure(visit(node.getVectorElements(), node), delegate)
                }
            }
        }
    }

    def visit(MatrixVectorIndexType node, parent){
        LOGGER.debug("Visiting node:" + node + ", parent=" + parent)
        return{
            if(node.assign){
                visitClosure(visit(node.assign, node), delegate)
            }
            else if(node.int){
                visitClosure(visit(node.int, node), delegate)
            }
            else if(node.symbRef){
                visitClosure(visit(node.symbRef, node), delegate)
            }
        }
    }

    def visit(VectorElementsType node, parent){
        return {
            node.getExpressionOrAssign().each {vv->
                mrow{
                    visitClosure(visit(vv, node), delegate)
                }
            }
        }
    }


    def opSymbol(BinaryOperator op) {
        def retVal = "Error!"
        switch (op) {
            case BinaryOperator.ATAN_2: retVal = "atan2"; break
            case BinaryOperator.LOGX: retVal = "log"; break
            case BinaryOperator.MAX: retVal = "max"; break
            case BinaryOperator.MIN: retVal = "min"; break
            case BinaryOperator.PLUS: retVal = "+"; break
            case BinaryOperator.MINUS: retVal = "-"; break
            case BinaryOperator.REM: retVal = "%"; break
            case BinaryOperator.TIMES: retVal = TIMES_OP; break
//            case BinaryOperator.TIMES: retVal = '&#x022C5;'; break
            default: retVal = "Error!"
        }
        retVal
    }

    def visit(UniopType node, parent) {
        LOGGER.debug("Visiting node:" + node + ", parent=" + parent)
        switch(node.op){
            case UnaryOperator.SQRT:
            return {
                msqrt {
                    visitClosure(visit(node.expression, node), delegate)
                }
            }
            case UnaryOperator.ABS:
            return {
                mo("|")
                visitClosure(visit(node.expression, node), delegate)
                mo("|")
            }
            case UnaryOperator.FACTORIAL:
            return {
                visitClosure(visit(node.expression, node), delegate)
                mo('!')
            }
            case UnaryOperator.MINUS:
                return {
                    mo(form: "prefix", "-")
                    visitClosure(visit(node.expression, node), delegate)
                }
            case UnaryOperator.LOG:
                return {
                    visitClosure(functionCall("ln", node, [node.expression]), delegate)
                }
                break
            case UnaryOperator.LOG_2:
                def fName = {
                    msub{
                        mi("log")
                        mn(2)
                    }
                }
                return {
                    visitClosure(functionCall(fName, node, [node.expression]), delegate)
                }
                break
            case UnaryOperator.LOG_10:
                def fName = {
                    msub{
                        mi("log")
                        mn(10)
                    }
                }
                return {
                    visitClosure(functionCall(fName, node, [node.expression]), delegate)
                }
                break
            case UnaryOperator.SIN:
            case UnaryOperator.COS:
            case UnaryOperator.TAN:
            return {
                visitClosure(functionCall(node.op.value(), node, [node.expression]), delegate)
            }
            case UnaryOperator.EXP:
                return{
                    msup{
                        mrow{
                            mi{
                                mkp.yieldUnescaped "&ExponentialE;"
                            }
                        }
                        mrow{
                            visitClosure(visit(node.expression, node), delegate)
                        }
                    }
                }
            default:
            return {
                mo(opSymbol(node.op))
                visitClosure(visit(node.expression, node), delegate)
            }
        }
    }

    def visit(FunctionCallType node, parent){
        LOGGER.debug("Visiting node:" + node + ", parent=" + parent)
        return {
            visitClosure(visit(node.symbRef, node), delegate)
            mo{
                mkp.yieldUnescaped APPLY_FUNCTION
            }
            mfenced {
                for(def a : node.functionArgument) {
                    mrow{
                        mi(a.symbId)
                        mo("=")
                        if(a.assign) {
                            visitClosure(visit(a.assign, node), delegate)
                        }
                        else{
                            visitClosure(visit(a.expression, node), delegate)
                        }
                    }
                }
            }
        }
    }


    def visit(PiecewiseType node, parent){
        LOGGER.debug("Visiting node:" + node + ", parent=" + parent)
        return {
            mrow{
                mo('{')
                mtable(columnalign:"left"){
                    for(def pc : node.getPiece()){
                        mtr{
                            mtd{
                                mrow{
                                    visitClosure(visit(pc.expression, node), delegate)
                                }
                            }
                            mtd{
                                if(pc.condition.getExpression().getValue() instanceof OtherwiseType){
                                    mtext("otherwise")
                                }
                                else{
                                    mtext("if")
                                }
                            }
                            mtd{
                                if(!(pc.condition.getExpression().getValue() instanceof OtherwiseType)) {
                                    mrow{
                                        visitClosure(visit(pc.condition.getExpression(), node), delegate)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    def opSymbol(LogicalBinOperator op) {
        String opStr = ""
        switch (op) {
            case LogicalBinOperator.GEQ:
                opStr = "&#x2265;"
                break
            case LogicalBinOperator.AND:
                opStr = "&#x2227;"
                break
            case LogicalBinOperator.OR:
                opStr = "&#x2228;"
                break
            case LogicalBinOperator.XOR:
                opStr = "&#x2295;"
                break
            case LogicalBinOperator.LT:
                opStr = "&lt;"
                break
            case LogicalBinOperator.NEQ:
                opStr = "&#x2260;"
                break
            case LogicalBinOperator.LEQ:
                opStr = "&#x2264;"
                break
            case LogicalBinOperator.EQ:
                opStr = "="
                break
            case LogicalBinOperator.GT:
                opStr = ">"
                break
            default:
                opStr = "Error"
        }
        return {
            mo{
                mkp.yieldUnescaped opStr
            }
        }
    }

    def opSymbol(LogicalUniOperator op) {
        String opStr = ""
        switch (op) {
            case LogicalUniOperator.NOT:
                opStr = "!"
                break;
            default:
                opStr = "Error"
        }
        return {
            mo{
                mkp.yieldUnescaped opStr
            }
        }
    }

    def visit(LogicBinOpType node, parent){
        LOGGER.debug("Visiting node:" + node + ", parent=" + parent)
        return {
            def exprClosure = {
                visitClosure(visit(node.getExpression().get(0), node), delegate)
                visitClosure(opSymbol(node.op), delegate)
                visitClosure(visit(node.getExpression().get(1), node), delegate)
            }
            if(isParentPrecedenceHigher(node, parent)){
                mfenced {
                    mrow {
                        visitClosure(exprClosure, delegate)
                    }
                }
            }
            else{
                visitClosure(exprClosure, delegate)
            }
        }
    }

    def visit(LogicUniOpType node, parent){
        LOGGER.debug("Visiting node:" + node + ", parent=" + parent)
        switch(node.op){
            case LogicalUniOperator.IS_DEFINED:
                return {
                    visitClosure(functionCall(node.op.value(), node, [node.getExpression()]), delegate)
                }
            break;
            default:
                return {
                    def exprClosure = {
                        visitClosure(opSymbol(node.op), delegate)
                        visitClosure(visit(node.getExpression(), node), delegate)
                    }
                    if(isParentPrecedenceHigher(node, parent)){
                        mfenced {
                            mrow {
                                visitClosure(exprClosure, delegate)
                            }
                        }
                    }
                    else{
                        visitClosure(exprClosure, delegate)
                    }
                }
        }
    }


    def visit(MatrixUniOpType node, parent){
        LOGGER.debug("Visiting node:" + node + ", parent=" + parent)
        Closure retVal = {}
        switch(node.op){
            case MatrixUniOperatorType.DETERMINANT:
                retVal = {
                    mrow {
                        mo("det")
                        visitClosure(visit(node.getExpression(), parent), delegate)
                    }
                }
                break;
            case MatrixUniOperatorType.TRACE:
                retVal = {
                    mrow {
                        mo("Tr")
                        visitClosure(visit(node.getExpression(), parent), delegate)
                    }
                }
                break;
            case MatrixUniOperatorType.TRANSPOSE:
                retVal = {
                    msup{
                        mrow{
                            visitClosure(visit(node.getExpression(), parent), delegate)
                        }
                        mrow{
                            mo("T")
                        }
                    }
                }
                break;
            case MatrixUniOperatorType.INVERSE:
                retVal = {
                    msup{
                        mrow{
                            visitClosure(visit(node.getExpression(), parent), delegate)
                        }
                        mrow{
                            mn("-1")
                        }
                    }
                }
                break;
        }

        retVal
    }


    def functionCall(Closure name, List<Closure> args) {
        LOGGER.debug("create function call. name closure=" + name + ", argClosures=" + args)
        return{
            visitClosure(name, delegate)
            mo{
                mkp.yieldUnescaped APPLY_FUNCTION
            }
            mfenced {
                for(def a : args) {
                    mrow{
                        visitClosure(a, delegate)
                    }
                }
            }
        }

    }

    def functionCall(String name, List<Closure> args){
        LOGGER.debug("create function call. name str=" + name + ", argClosures=" + args)
        functionCall({ mi(name) }, args)
    }

    def functionCall(Closure name, node, List<JAXBElement> args){
        LOGGER.debug("create function call. name closure=" + name + ", owningNode=" + node + ", argClosures=" + args)
        List<Closure> closureArgs = []
        args.each {a->
            closureArgs.add({
                visitClosure(visit(a, node), delegate)
            })
        }
        functionCall(name, closureArgs)
    }

    def functionCall(String name, node, List<JAXBElement> args){
        LOGGER.debug("create function call. name=" + name + ", owningNode=" + node + ", argClosures=" + args)
        functionCall({ mi(name) }, node, args)
    }


     def opSymbol(UnaryOperator op) {
        def retVal = "Error!"
        switch (op) {
            default: retVal = op.value()
        }
        retVal
    }



    def visit(ConstantType node, parent){
        LOGGER.debug("Visiting node:" + node + ", parent=" + parent)
        return {
            def op = "Error"
            switch(node.op) {
                case ConstantValueType.PI: op = "&pi;"; break
                case ConstantValueType.EXPONENTIALE: op = "&ExponentialE;"; break
            }
            mi{
                mkp.yieldUnescaped op
            }
        }
    }

    def visit(PlusInfType node, parent){
        LOGGER.debug("Visiting node:" + node + ", parent=" + parent)
        return {
            mi {
                mkp.yieldUnescaped "&infin;"
            }
        }
    }

    def visit(MinusInfType node, parent){
        LOGGER.debug("Visiting node:" + node + ", parent=" + parent)
        return {
            mo(form: "prefix", "-")
            mi {
                mkp.yieldUnescaped "&infin;"
            }
        }
    }

    def visit(NaNType node, parent){
        LOGGER.debug("Visiting node:" + node + ", parent=" + parent)
        return {
            mi("NaN")
        }
    }

    def visit(MatrixType node, parent){
        LOGGER.debug("Visiting node:" + node + ", parent=" + parent)
        return{
            mrow{
                mo('(')
                mtable{
                    node.getMatrixColumnOrMatrixRowOrMatrixCell().each {r->
                        visitClosure(visit(r, node), delegate)
                    }
                }
                mo(')')
            }
        }
    }

    def visit(MatrixRowType node, parent){
        LOGGER.debug("Visiting node:" + node + ", parent=" + parent)
        return{
            mtr{
                node.getExpressionOrAssign().each {c->
                    mtd{
                        visitClosure(visit(c, node), delegate)
                    }
                }
            }
        }
    }

}
