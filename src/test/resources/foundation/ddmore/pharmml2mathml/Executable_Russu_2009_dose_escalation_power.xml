<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
  ~
  ~  Licensed under the Apache License, Version 2.0 (the
  ~  "License"); you may not use this file except in
  ~  compliance with the License.  You may obtain a copy of
  ~  the License at
  ~
  ~       http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~  Unless required by applicable law or agreed to in writing,
  ~  software distributed under the License is distributed on
  ~  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
  ~  KIND, either express or implied. See the License for the
  ~  specific language governing permissions and limitations
  ~  under the License.
  ~
  -->

<PharmML
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
	xmlns="http://www.pharmml.org/pharmml/0.8/PharmML"
	xsi:schemaLocation="http://www.pharmml.org/pharmml/0.8/PharmML http://www.pharmml.org/pharmml/0.8/PharmML"
	xmlns:math="http://www.pharmml.org/pharmml/0.8/Maths"
	xmlns:ct="http://www.pharmml.org/pharmml/0.8/CommonTypes"
	xmlns:ds="http://www.pharmml.org/pharmml/0.8/Dataset"
	xmlns:mdef="http://www.pharmml.org/pharmml/0.8/ModelDefinition"
	xmlns:mstep="http://www.pharmml.org/pharmml/0.8/ModellingSteps"
	xmlns:design="http://www.pharmml.org/pharmml/0.8/TrialDesign"
	writtenVersion="0.8.1">
	<ct:Name>Generated from MDL. MOG ID: russu_power_mog</ct:Name>
	<IndependentVariable symbId="DOSE"/>
	<ct:FunctionDefinition xmlns="http://www.pharmml.org/pharmml/0.8/CommonTypes" 
		symbolType="real"
		symbId="proportionalError">
	    <ct:Description>Proportional or constant CV (CVV)</ct:Description>
	    <ct:FunctionArgument symbolType="real" symbId="proportional"/>
	    <ct:FunctionArgument symbolType="real" symbId="f"/>
	    <ct:Definition>
	        <Assign>
	            <Binop op="times" xmlns="http://www.pharmml.org/pharmml/0.8/Maths">
	                <ct:SymbRef symbIdRef="proportional"/>
	                <ct:SymbRef symbIdRef="f"/>
	            </Binop>
	        </Assign>
	    </ct:Definition>
	</ct:FunctionDefinition>
	<ModelDefinition xmlns="http://www.pharmml.org/pharmml/0.8/ModelDefinition">
		<VariabilityModel blkId="vm_err" type="residualError">
			<Level symbId="DV"/>
		</VariabilityModel>
		<VariabilityModel blkId="vm_mdl" type="parameterVariability">
			<Level symbId="MDL__prior"/>
			<Level symbId="ID">
				<ParentLevel>
					<ct:SymbRef symbIdRef="MDL__prior"/>
				</ParentLevel>
			</Level>
		</VariabilityModel>
		<ParameterModel blkId="pm">
			<PopulationParameter symbId="MU_THETA1">
				<ct:Assign>
					<ct:Int>0</ct:Int>
				</ct:Assign>
			</PopulationParameter>
			<PopulationParameter symbId="MU_THETA2">
				<ct:Assign>
					<ct:Int>1</ct:Int>
				</ct:Assign>
			</PopulationParameter>
			<PopulationParameter symbId="VAR_THETA1">
				<ct:Assign>
					<math:Binop op="power">
						<ct:Int>10</ct:Int>
						<ct:Int>1</ct:Int>
					</math:Binop>
				</ct:Assign>
			</PopulationParameter>
			<PopulationParameter symbId="VAR_THETA2">
				<ct:Assign>
					<math:Binop op="power">
						<ct:Int>10</ct:Int>
						<ct:Int>1</ct:Int>
					</math:Binop>
				</ct:Assign>
			</PopulationParameter>
			<PopulationParameter symbId="a_1">
				<ct:Assign>
					<ct:Real>0.1</ct:Real>
				</ct:Assign>
			</PopulationParameter>
			<PopulationParameter symbId="b_1">
				<ct:Assign>
					<ct:Real>0.1</ct:Real>
				</ct:Assign>
			</PopulationParameter>
			<PopulationParameter symbId="a_2">
				<ct:Assign>
					<ct:Real>0.1</ct:Real>
				</ct:Assign>
			</PopulationParameter>
			<PopulationParameter symbId="b_2">
				<ct:Assign>
					<ct:Real>0.1</ct:Real>
				</ct:Assign>
			</PopulationParameter>
			<PopulationParameter symbId="a_cv">
				<ct:Assign>
					<ct:Int>10</ct:Int>
				</ct:Assign>
			</PopulationParameter>
			<PopulationParameter symbId="b_cv">
				<ct:Assign>
					<ct:Int>1</ct:Int>
				</ct:Assign>
			</PopulationParameter>
			<PopulationParameter symbId="THETA1">
				<ct:VariabilityReference>
					<ct:SymbRef blkIdRef="vm_mdl" symbIdRef="MDL__prior"/>
				</ct:VariabilityReference>
				<Distribution>
					<ProbOnto xmlns="http://www.pharmml.org/probonto/ProbOnto" name="Normal2">
						<Parameter name="mean">
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="MU_THETA1"/>
							</ct:Assign>
						</Parameter>
						<Parameter name="var">
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="VAR_THETA1"/>
							</ct:Assign>
						</Parameter>
					</ProbOnto>
				</Distribution>
			</PopulationParameter>
			<PopulationParameter symbId="THETA2">
				<ct:VariabilityReference>
					<ct:SymbRef blkIdRef="vm_mdl" symbIdRef="MDL__prior"/>
				</ct:VariabilityReference>
				<Distribution>
					<ProbOnto xmlns="http://www.pharmml.org/probonto/ProbOnto" name="Normal2">
						<Parameter name="mean">
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="MU_THETA2"/>
							</ct:Assign>
						</Parameter>
						<Parameter name="var">
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="VAR_THETA2"/>
							</ct:Assign>
						</Parameter>
					</ProbOnto>
				</Distribution>
			</PopulationParameter>
			<PopulationParameter symbId="invOMEGA_1">
				<ct:VariabilityReference>
					<ct:SymbRef blkIdRef="vm_mdl" symbIdRef="MDL__prior"/>
				</ct:VariabilityReference>
				<Distribution>
					<ProbOnto xmlns="http://www.pharmml.org/probonto/ProbOnto" name="Gamma2">
						<Parameter name="shape">
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="a_1"/>
							</ct:Assign>
						</Parameter>
						<Parameter name="rate">
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="b_1"/>
							</ct:Assign>
						</Parameter>
					</ProbOnto>
				</Distribution>
			</PopulationParameter>
			<PopulationParameter symbId="invOMEGA_2">
				<ct:Assign>
					<ct:Int>200</ct:Int>
				</ct:Assign>
			</PopulationParameter>
			<PopulationParameter symbId="invCV2">
				<ct:VariabilityReference>
					<ct:SymbRef blkIdRef="vm_mdl" symbIdRef="MDL__prior"/>
				</ct:VariabilityReference>
				<Distribution>
					<ProbOnto xmlns="http://www.pharmml.org/probonto/ProbOnto" name="Gamma2">
						<Parameter name="shape">
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="a_cv"/>
							</ct:Assign>
						</Parameter>
						<Parameter name="rate">
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="b_cv"/>
							</ct:Assign>
						</Parameter>
					</ProbOnto>
				</Distribution>
			</PopulationParameter>
			<PopulationParameter symbId="OMEGA_1">
				<ct:Assign>
					<math:Binop op="divide">
						<ct:Int>1</ct:Int>
						<ct:SymbRef blkIdRef="pm" symbIdRef="invOMEGA_1"/>
					</math:Binop>
				</ct:Assign>
			</PopulationParameter>
			<PopulationParameter symbId="OMEGA_2">
				<ct:Assign>
					<math:Binop op="divide">
						<ct:Int>1</ct:Int>
						<ct:SymbRef blkIdRef="pm" symbIdRef="invOMEGA_2"/>
					</math:Binop>
				</ct:Assign>
			</PopulationParameter>
			<PopulationParameter symbId="CV">
				<ct:Assign>
					<math:Uniop op="sqrt">
						<math:Binop op="divide">
							<ct:Int>1</ct:Int>
							<ct:SymbRef blkIdRef="pm" symbIdRef="invCV2"/>
						</math:Binop>
					</math:Uniop>	
				</ct:Assign>
			</PopulationParameter>
			<IndividualParameter symbId="ALPHA">
				<StructuredModel>
					<LinearCovariate>
						<PopulationValue>
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="THETA1"/>
							</ct:Assign>
						</PopulationValue>
					</LinearCovariate>
					<RandomEffects>
						<ct:SymbRef blkIdRef="pm" symbIdRef="ETA_1"/>
					</RandomEffects>
				</StructuredModel>
			</IndividualParameter>
			<IndividualParameter symbId="BETA">
				<StructuredModel>
					<LinearCovariate>
						<PopulationValue>
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="THETA2"/>
							</ct:Assign>
						</PopulationValue>
					</LinearCovariate>
					<RandomEffects>
						<ct:SymbRef blkIdRef="pm" symbIdRef="ETA_2"/>
					</RandomEffects>
				</StructuredModel>
			</IndividualParameter>
			<RandomVariable symbId="ETA_1">
				<ct:VariabilityReference>
					<ct:SymbRef blkIdRef="vm_mdl" symbIdRef="ID"/>
				</ct:VariabilityReference>
				<Distribution>
					<ProbOnto xmlns="http://www.pharmml.org/probonto/ProbOnto" name="Normal2">
						<Parameter name="mean">
							<ct:Assign>
								<ct:Int>0</ct:Int>
							</ct:Assign>
						</Parameter>
						<Parameter name="var">
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="OMEGA_1"/>
							</ct:Assign>
						</Parameter>
					</ProbOnto>
				</Distribution>
			</RandomVariable>
			<RandomVariable symbId="ETA_2">
				<ct:VariabilityReference>
					<ct:SymbRef blkIdRef="vm_mdl" symbIdRef="ID"/>
				</ct:VariabilityReference>
				<Distribution>
					<ProbOnto xmlns="http://www.pharmml.org/probonto/ProbOnto" name="Normal2">
						<Parameter name="mean">
							<ct:Assign>
								<ct:Int>0</ct:Int>
							</ct:Assign>
						</Parameter>
						<Parameter name="var">
							<ct:Assign>
								<ct:SymbRef blkIdRef="pm" symbIdRef="OMEGA_2"/>
							</ct:Assign>
						</Parameter>
					</ProbOnto>
				</Distribution>
			</RandomVariable>
			<RandomVariable symbId="EPS">
				<ct:VariabilityReference>
					<ct:SymbRef blkIdRef="vm_err" symbIdRef="DV"/>
				</ct:VariabilityReference>
				<Distribution>
					<ProbOnto xmlns="http://www.pharmml.org/probonto/ProbOnto" name="Normal2">
						<Parameter name="mean">
							<ct:Assign>
								<ct:Int>0</ct:Int>
							</ct:Assign>
						</Parameter>
						<Parameter name="var">
							<ct:Assign>
								<ct:Int>1</ct:Int>
							</ct:Assign>
						</Parameter>
					</ProbOnto>
				</Distribution>
			</RandomVariable>
		</ParameterModel>
		<StructuralModel blkId="sm">
			<ct:Variable symbId="C" symbolType="real">
				<ct:Assign>
					<math:Binop op="times">
						<math:Uniop op="exp">
							<ct:SymbRef blkIdRef="pm" symbIdRef="ALPHA"/>
						</math:Uniop>	
						<math:Binop op="power">
							<ct:SymbRef symbIdRef="DOSE"/>
							<ct:SymbRef blkIdRef="pm" symbIdRef="BETA"/>
						</math:Binop>
					</math:Binop>
				</ct:Assign>
			</ct:Variable>
		</StructuralModel>
		<ObservationModel blkId="om1">
			<ContinuousData>
				<Standard symbId="Y">
					<Output>
						<ct:SymbRef blkIdRef="sm" symbIdRef="C"/>
					</Output>
					<ErrorModel>
						<ct:Assign>
							<math:FunctionCall>
								<ct:SymbRef symbIdRef="proportionalError"/>
								<math:FunctionArgument symbId="proportional">
									<ct:SymbRef blkIdRef="pm" symbIdRef="CV"/>
								</math:FunctionArgument>
								<math:FunctionArgument symbId="f">
									<ct:SymbRef blkIdRef="sm" symbIdRef="C"/>
								</math:FunctionArgument>
							</math:FunctionCall>
						</ct:Assign>
					</ErrorModel>
					<ResidualError>
						<ct:SymbRef blkIdRef="pm" symbIdRef="EPS"/>
					</ResidualError>
				</Standard>
			</ContinuousData>
		</ObservationModel>
	</ModelDefinition>
	<TrialDesign xmlns="http://www.pharmml.org/pharmml/0.8/TrialDesign">
		<ExternalDataSet toolName="NONMEM" oid="nm_ds">
			<ColumnMapping>
				<ColumnRef xmlns="http://www.pharmml.org/pharmml/0.8/Dataset" columnIdRef="ID"/>
				<ct:SymbRef blkIdRef="vm_mdl" symbIdRef="ID"/>
			</ColumnMapping>
			<ColumnMapping>
				<ColumnRef xmlns="http://www.pharmml.org/pharmml/0.8/Dataset" columnIdRef="DOSE"/>
				<ct:SymbRef symbIdRef="DOSE"/>
			</ColumnMapping>
			<ColumnMapping>
			    <ColumnRef xmlns="http://www.pharmml.org/pharmml/0.8/Dataset" columnIdRef="DV"/>
				<ct:SymbRef blkIdRef="om1" symbIdRef="Y"/>
			</ColumnMapping>
			<DataSet xmlns="http://www.pharmml.org/pharmml/0.8/Dataset">
				<Definition>
					<Column columnId="ID" columnType="id" valueType="int" columnNum="1"/>
					<Column columnId="DOSE" columnType="idv" valueType="real" columnNum="2"/>
					<Column columnId="DV" columnType="dv" valueType="real" columnNum="3"/>
				</Definition>
				<ExternalFile oid="id">
					<path>Simulated_russu_2009_power_data.csv</path>
					<format>CSV</format>
					<delimiter>COMMA</delimiter>
				</ExternalFile>
			</DataSet>
		</ExternalDataSet>
	</TrialDesign>
	<ModellingSteps xmlns="http://www.pharmml.org/pharmml/0.8/ModellingSteps">
		<EstimationStep oid="estimStep_1">
			<ExternalDataSetReference>
				<ct:OidRef oidRef="nm_ds"/>
			</ExternalDataSetReference>
			<ParametersToEstimate>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="THETA1"/>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="THETA2"/>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="invOMEGA_1"/>
				</ParameterEstimation>
				<ParameterEstimation>
					<ct:SymbRef blkIdRef="pm" symbIdRef="invCV2"/>
				</ParameterEstimation>
			</ParametersToEstimate>
			<Operation order="1" opType="generic">
				<Property name="algo">
					<ct:Assign>
						<ct:String>mcmc</ct:String>
					</ct:Assign>
				</Property>
			</Operation>
			<Operation order="2" opType="BUGS">
				<Property name="nchains">
					<ct:Assign>
						<ct:Int>1</ct:Int>
					</ct:Assign>
				</Property>
				<Property name="burnin">
					<ct:Assign>
						<ct:Int>1000</ct:Int>
					</ct:Assign>
				</Property>
				<Property name="niter">
					<ct:Assign>
						<ct:Int>5000</ct:Int>
					</ct:Assign>
				</Property>
				<Property name="winbugsgui">
					<ct:Assign>
						<ct:String>true</ct:String>
					</ct:Assign>
				</Property>
			</Operation>
		</EstimationStep>
		<mstep:StepDependencies>
			<mstep:Step>
				<ct:OidRef oidRef="estimStep_1"/>
			</mstep:Step>
		</mstep:StepDependencies>
	</ModellingSteps>		
</PharmML>
