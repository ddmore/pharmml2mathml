/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import foundation.ddmore.pharmml08.IPharmMLResource
import foundation.ddmore.pharmml08.PharmMlFactory
import foundation.ddmore.pharmml08.impl.LibPharmMLImpl
import org.junit.After
import org.junit.Assert
import static org.junit.Assert.*
import org.junit.Before
import org.junit.Test

/**
 * Created by smoodie on 16/12/2016.
 */
class PharmMLModelExtractorPkMacroIntegrationTest {
    IPharmMLResource resource = null


    def createPharmMLResource(InputStream f){
        LibPharmMLImpl api = PharmMlFactory.getInstance().createLibPharmML()
        def stream = null
        IPharmMLResource resource = null
        try {
            stream = new BufferedInputStream(f)
            resource = api.createDomFromResource(stream)
        } finally {
            stream?.close()
            return resource
        }

    }

    @Before
    void setUp(){
        def testFile = this.class.getResourceAsStream("UseCase6_3.xml")

        assert testFile

        resource = createPharmMLResource(testFile)
    }

    @After
    void tearDown(){
        resource = null
    }


    @Test
    void testNameDefined(){
        def result = PharmMlExtractor.extractPharmML(resource.dom)

        Assert.assertNotNull(result)
        Assert.assertEquals("Generated from MDL. MOG ID: warfarin_PK_ODE_mog", result.name)
    }


    @Test
    void testExpectedIdv(){
        def result = PharmMlExtractor.extractPharmML(resource.dom)

        Assert.assertNotNull(result)
        Assert.assertEquals(1, result.indVars.size())
        Assert.assertEquals("T", result.indVars.get(0))
    }

    @Test
    void testCanParseCovModel(){
        def result = PharmMlExtractor.extractPharmML(resource.dom).modelDefinition
        Assert.assertNotNull(result)
        Assert.assertEquals(1, result.covariateModels.size())
        def covModel = result.covariateModels.get(0)
        Assert.assertNotNull(covModel)
        Assert.assertEquals("cm", covModel.blkId)
        Assert.assertEquals(0, covModel.parameters.size())
        Assert.assertEquals(0, covModel.covariates.grep{ n->
            n.type == 'categorical' }.size())
        Assert.assertEquals(1, covModel.covariates.grep{ it.type == 'continuous' }.size())
        Assert.assertEquals("logtWT", covModel.covariates.get(0).symbId)
        def expectedDisn = "<mrow><mi>logtWT</mi></mrow>"
        Assert.assertEquals(expectedDisn, covModel.covariates.get(0).maths)
    }

    @Test
    void testCanParseParamModel(){
        def result = PharmMlExtractor.extractPharmML(resource.dom).modelDefinition
        Assert.assertNotNull(result)
        Assert.assertEquals(1, result.parameterModels.size())
        def paramModel = result.parameterModels.get(0)
        Assert.assertNotNull(paramModel)
        Assert.assertEquals("pm", paramModel.blkId)
        Assert.assertEquals(21, paramModel.parameters.size())
        Assert.assertEquals("POP_CL", paramModel.parameters.get(0).symbId)

        Assert.assertEquals("POP_V", paramModel.parameters.get(1).symbId)
        def expectedPopV = "<mrow><mi>POP_V</mi></mrow>"
        Assert.assertEquals(expectedPopV, paramModel.parameters.get(1).maths)

        Assert.assertEquals("ETA_CL_V_KA", paramModel.parameters.get(11).symbId)
        def expectedEtaV = "<mrow><msub><mi>ETA_CL_V_KA</mi><mrow><mfenced open=\"\" close=\"\"><mrow><mi>vm_mdl.ID</mi></mrow></mfenced></mrow></msub><mo>~</mo><mrow><mi>MultivariateStudentT1</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>mean</mi><mo>=</mo><mrow><mfenced><mrow><mn>0</mn></mrow><mrow><mn>0</mn></mrow><mrow><mn>0</mn></mrow></mfenced></mrow></mrow><mrow><mi>covariancematrix</mi><mo>=</mo><mi>pm.PPV_CL_V_KA</mi></mrow><mrow><mi>degreesoffreedom</mi><mo>=</mo><mi>pm.DF_CL_V_KA</mi></mrow></mfenced></mrow></mrow>"
        Assert.assertEquals(expectedEtaV, paramModel.parameters.get(11).maths)
        Assert.assertEquals("rvParam", paramModel.parameters.get(11).type)

        Assert.assertEquals("MDL__ETA_CL_V_KA_1", paramModel.parameters.get(13).symbId)
        def expectedV = "<mrow><mi>MDL__ETA_CL_V_KA_1</mi><mo>=</mo><mrow><mi>pm.ETA_CL_V_KA</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mn>1</mn></mrow></mfenced></mrow></mrow>"
        Assert.assertEquals(expectedV, paramModel.parameters.get(13).maths)
        Assert.assertEquals("popParam", paramModel.parameters.get(13).type)
    }

    @Test
    void testCanParseStructModel(){
        def result = PharmMlExtractor.extractPharmML(resource.dom).modelDefinition
        Assert.assertNotNull(result)
        Assert.assertEquals(1, result.structuralModels.size())
        def structModel = result.structuralModels.get(0)
        Assert.assertNotNull(structModel)
        Assert.assertEquals("sm", structModel.blkId)
        Assert.assertEquals(0, structModel.parameters.size())
        Assert.assertEquals(2, structModel.variables.size())
        Assert.assertEquals(3, structModel.pkmacros.size())

        Assert.assertEquals("CC", structModel.variables.get(0).symbId)
        def expectedk = "<mrow><mi>CC</mi><mo>=</mo><mfrac><mrow><mi>sm.CENTRAL</mi></mrow><mrow><mi>pm.V</mi></mrow></mfrac></mrow>"
        Assert.assertEquals(expectedk, structModel.variables.get(0).maths)
        Assert.assertEquals("varDefn", structModel.variables.get(0).type)

        Assert.assertEquals("compartment", structModel.pkmacros.get(1).type)
        def expectedAd = "<mrow><mi>compartment</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>AMOUNT</mi><mo>=</mo><mi>sm.CENTRAL</mi></mrow><mrow><mi>CMT</mi><mo>=</mo><mn>2</mn></mrow></mfenced></mrow>"
        Assert.assertEquals(expectedAd.toString(), structModel.pkmacros.get(1).maths)
    }

    @Test
    void testCanParseObsModel(){
        def result = PharmMlExtractor.extractPharmML(resource.dom).modelDefinition
        Assert.assertNotNull(result)
        Assert.assertEquals(1, result.observationModels.size())
        def obsModel = result.observationModels.get(0)
        Assert.assertNotNull(obsModel)
        Assert.assertEquals("om1", obsModel.blkId)
        Assert.assertEquals(0, obsModel.continuousObs.parameters.size())
        Assert.assertEquals(0, obsModel.continuousObs.correlations.size())

        Assert.assertEquals("Y", obsModel.continuousObs.observation.symbId)
        def expectedk = "<mrow><mi>Y</mi><mo>=</mo><mi>sm.CC</mi><mo>+</mo><mi>combinedError1</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>additive</mi><mo>=</mo><mi>pm.RUV_ADD</mi></mrow><mrow><mi>proportional</mi><mo>=</mo><mi>pm.RUV_PROP</mi></mrow><mrow><mi>f</mi><mo>=</mo><mi>sm.CC</mi></mrow></mfenced><mo>+</mo><mi>pm.EPS_Y</mi></mrow>"
        Assert.assertEquals(expectedk, obsModel.continuousObs.observation.maths)
        Assert.assertEquals("structured", obsModel.continuousObs.observation.type)
    }

    @Test
    void testCanParseModellingStep(){
        def result = PharmMlExtractor.extractPharmML(resource.dom).modellingSteps
        Assert.assertNotNull(result)
        Assert.assertEquals(1, result.estimationSteps.size())
        def estStep = result.estimationSteps.get(0)
        Assert.assertNotNull(estStep)
        Assert.assertEquals("estimStep_1", estStep.oid)
        Assert.assertNull(estStep.name)
        Assert.assertNull(estStep.description)
        Assert.assertEquals("estimation", estStep.type)

        def expectedParams = [
                [
                        symbRef : "pm.POP_CL",
                        initial : "<mn>0.1</mn>",
                        fixed : false,
                        lower : "<mn>0.001</mn>",
                        upper : null
                ],
                [
                        symbRef : "pm.POP_V",
                        initial : "<mn>8</mn>",
                        fixed : false,
                        lower : "<mn>0.001</mn>",
                        upper : null
                ],
                [
                        symbRef : "pm.POP_KA",
                        initial : "<mn>0.362</mn>",
                        fixed : false,
                        lower : "<mn>0.001</mn>",
                        upper : null
                ],
                [
                        symbRef : "pm.POP_TLAG",
                        initial : "<mn>1</mn>",
                        fixed : false,
                        lower : "<mn>0.001</mn>",
                        upper : null
                ],
                [
                        symbRef : "pm.BETA_CL_WT",
                        initial : "<mn>0.75</mn>",
                        fixed : true,
                        lower : null,
                        upper : null
                ],
                [
                        symbRef : "pm.BETA_V_WT",
                        initial : "<mn>1</mn>",
                        fixed : true,
                        lower : null,
                        upper : null
                ],
                [
                        symbRef : "pm.PPV_CL_V_KA",
                        initial : "<mrow><mo>(</mo><mtable><mtr><mtd><mn>0.0</mn></mtd></mtr><mtr><mtd><mn>0.01</mn></mtd><mtd><mn>0.0</mn></mtd></mtr><mtr><mtd><mn>0.01</mn></mtd><mtd><mn>0.01</mn></mtd><mtd><mn>0.0</mn></mtd></mtr></mtable><mo>)</mo></mrow>",
                        fixed : false,
                        lower : null,
                        upper : null
                ],
                [
                        symbRef : "pm.DF_CL_V_KA",
                        initial : "<mn>3</mn>",
                        fixed : true
                ],
        ]

        assertEquals(11, estStep.parametersToEstimate.size())

        for(int i = 0; i < expectedParams.size(); i++){
            assertEquals(expectedParams[i].symbRef, estStep.parametersToEstimate[i].symbRef)
            assertEquals(expectedParams[i].initial, estStep.parametersToEstimate[i].initialValue.value)
            assertEquals(expectedParams[i].fixed, estStep.parametersToEstimate[i].initialValue.fixed)
            assertEquals(expectedParams[i].lower, estStep.parametersToEstimate[i].lower)
            assertEquals(expectedParams[i].upper, estStep.parametersToEstimate[i].upper)
        }
    }

    @Test
    void testCanParseTrialDesign(){
        def result = PharmMlExtractor.extractPharmML(resource.dom).trialDesign
        Assert.assertNotNull(result)
        Assert.assertEquals(1, result.extnlDatasets.size())
        def extnlDataset = result.extnlDatasets[0]
        assertEquals("nm_ds", extnlDataset.oid)
        assertEquals("Monolix", extnlDataset.toolname)
        Assert.assertNotNull(result.extnlDatasets[0].dataset)
        Assert.assertEquals(5, result.extnlDatasets[0].columnMappings.size())

        def mapping1 = result.extnlDatasets[0].columnMappings[0]
        Assert.assertEquals("ID", mapping1.colRef)
        Assert.assertEquals("<mi>vm_mdl.ID</mi>", mapping1.modelMapping)


        def mapping3 = result.extnlDatasets[0].columnMappings[2]
        Assert.assertEquals("AMT", mapping3.colRef)
        Assert.assertEquals("<mrow><mtext>blk:</mtext><mspace width=\"1mm\"/><mi>sm</mi><mo>,</mo><mspace width=\"2mm\"/><mtext>adm:</mtext><mspace width=\"1mm\"/><mn>1</mn></mrow>", mapping3.modelMapping)
//        Assert.assertEquals(1, mapping3.targetMappings.size())
//        Assert.assertEquals("sm", mapping3.targetMappings[0].blkIdRef)
//        Assert.assertEquals(1, mapping3.targetMappings[0].mappings[0].size())
//        Assert.assertEquals("1", mapping3.targetMappings[0].mappings.adm)
//        Assert.assertEquals("1", mapping3.targetMappings[0].mappings.dataSymbol)

    }
}
