/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import foundation.ddmore.pharmml08.dom.commontypes.SymbolTypeType
import foundation.ddmore.pharmml08.dom.commontypes.TrueBooleanType
import foundation.ddmore.pharmml08.dom.dataset.ColumnMappingType
import foundation.ddmore.pharmml08.dom.dataset.ColumnTypeType
import foundation.ddmore.pharmml08.dom.trialdesign.MultipleDVMappingType
import org.junit.Test

import static foundation.ddmore.pharmml08.builder.MathsBuilder.*
import static foundation.ddmore.pharmml08.builder.TrialDesignBuilder.*
import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertNotNull
import static org.junit.Assert.assertNull

/**
 * Created by stumoodie on 05/01/2017.
 */
class TrialDesignExtractorTest {

    @Test
    void testExternalDataset(){
        def root = createTrialDesign(
                createExternalDataset(
                        "testOid",
                        "NONMEM",
                        createDataset(
                                createExternalFileType("foo/bar"),
                                createColumnDefinition("c1", 1, ColumnTypeType.DOSE, SymbolTypeType.REAL),
                                createColumnDefinition("c2", 2, ColumnTypeType.DV, SymbolTypeType.REAL),
                                createColumnDefinition("c3", 3, ColumnTypeType.CMT, SymbolTypeType.INT)
                        ),
                        createColumnMapping(
                                createColumnRefType("col1"),
                                createSymbRefType("sm", "foo")
                        ),
                        createMultiDvMapping(
                                createColumnRefType("col1"),
                                createPiecewise(
                                        createPiece(
                                                createCondition(
                                                        createBoolean(true)
                                                ),
                                                createString("A")
                                        )
                                ).value
                        ),
                        createColumnMapping(
                                createColumnRefType("col3"),
                                createTargetMapping(
                                        "sm", [
                                        [ adm : 1, dataSymbol:"1"],
                                        [ adm : 2, dataSymbol: "2"]
                                ]
                                )
                        ),
                        createColumnMapping(
                                createColumnRefType("col4"),
                                createTargetMapping(
                                        "sm", 1
                                )
                        ),
                        createColumnMapping(
                                createColumnRefType("col5"),
                                createSymbRefType("cm", "Sex"),
                                [
                                        [ dataSymb : "1", modelSymb : "MALE" ],
                                        [ dataSymb : "2", modelSymb : "FEMALE"],
                                        [ dataSymb : "n", modelSymb : "NEUTRAL"]
                                ]
                        )
                )
        )


        def actual = TrialDesignExtractor.extractTrialDesign(root)

        assertEquals(1, actual.extnlDatasets.size())

        def xtnlDataset = actual.extnlDatasets.head()
        assertEquals("testOid", xtnlDataset.oid)
        assertEquals("NONMEM", xtnlDataset .toolname)
        assertEquals(5, xtnlDataset.columnMappings.size())

        // extnl file
        def xtnlFile = xtnlDataset.dataset.fileSpec
        assertEquals("foo/bar", xtnlFile.path)
        assertEquals("comma", xtnlFile.delimiter)
        assertEquals("csv", xtnlFile.format)

        def expectedColDefns = [
                [
                        id : "c1",
                        pos : 1,
                        colType : "dose",
                        valType : "real"
                ],
                [
                        id : "c2",
                        pos : 2,
                        colType : "dv",
                        valType : "real"
                ],
                [
                        id : "c3",
                        pos : 3,
                        colType : "cmt",
                        valType : "int"
                ],
        ]

        def colDefns = xtnlDataset.dataset.definitions
        assertEquals(expectedColDefns.size(), colDefns.size())
        for(int i = 0; i < expectedColDefns.size(); i++){
            assertEquals(expectedColDefns[i].id, colDefns[i].colId)
            assertEquals(expectedColDefns[i].pos, colDefns[i].colNum)
            assertEquals(expectedColDefns[i].colType, colDefns[i].colType)
            assertEquals(expectedColDefns[i].valType, colDefns[i].valType)
        }

        def mapping1 = xtnlDataset.columnMappings[0]
        assertEquals("col1", mapping1.colRef)
        assertEquals("<mi>sm.foo</mi>", mapping1.modelMapping)

        def mapping2 = xtnlDataset.columnMappings[1]
        assertEquals("col1", mapping2.colRef)
        assertEquals("<mrow><mo>{</mo><mtable columnalign=\"left\"><mtr><mtd><mrow><mtext>A</mtext></mrow></mtd><mtd><mtext>if</mtext></mtd><mtd><mrow><mi>T</mi></mrow></mtd></mtr></mtable></mrow>", mapping2.modelMapping)
//        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mrow><mo>{</mo><mtable columnalign=\"left\"><mtr><mtd><mrow><mi>sm.GUT</mi><mo>&#x022C5;</mo><mi>pm.KA</mi></mrow></mtd><mtd><mtext>if</mtext></mtd><mtd><mrow><mi>T</mi><mo>&#x2265;</mo><mi>pm.TLAG</mi></mrow></mtd></mtr><mtr><mtd><mrow><mn>0</mn></mrow></mtd><mtd><mtext>otherwise</mtext></mtd><mtd></mtd></mtr></mtable></mrow></math>"

        def mapping3 = xtnlDataset.columnMappings[2]
        assertEquals("col3", mapping3.colRef)
        assertEquals("<mrow><mo>{</mo><mtable columnalign=\"left\"><mtr><mtd><mrow><mtext>blk:</mtext><mspace width=\"1mm\"/><mi>sm</mi><mo>,</mo><mspace width=\"2mm\"/><mtext>adm:</mtext><mspace width=\"1mm\"/><mn>1</mn></mrow></mtd><mtd><mtext>if</mtext></mtd><mtd><mrow><mi>col3</mi><mo>=</mo><mtext>1</mtext></mrow></mtd></mtr><mtr><mtd><mrow><mtext>blk:</mtext><mspace width=\"1mm\"/><mi>sm</mi><mo>,</mo><mspace width=\"2mm\"/><mtext>adm:</mtext><mspace width=\"1mm\"/><mn>2</mn></mrow></mtd><mtd><mtext>if</mtext></mtd><mtd><mrow><mi>col3</mi><mo>=</mo><mtext>2</mtext></mrow></mtd></mtr></mtable></mrow>", mapping3.modelMapping)
//        assertEquals(1, mapping3.targetMappings.size())
//        assertEquals("sm", mapping3.targetMappings[0].blkIdRef)
//        assertEquals("1", mapping3.targetMappings[0].mappings[0].adm)
//        assertEquals("1", mapping3.targetMappings[0].mappings[0].dataSymbol)
//        assertEquals("2", mapping3.targetMappings[0].mappings[1].adm)
//        assertEquals("2", mapping3.targetMappings[0].mappings[1].dataSymbol)

        def mapping4 = xtnlDataset.columnMappings[3]
        assertEquals("col4", mapping4.colRef)
        assertEquals("<mrow><mtext>blk:</mtext><mspace width=\"1mm\"/><mi>sm</mi><mo>,</mo><mspace width=\"2mm\"/><mtext>adm:</mtext><mspace width=\"1mm\"/><mn>1</mn></mrow>", mapping4.modelMapping)

        def mapping5 = xtnlDataset.columnMappings[4]
        assertEquals("col5", mapping5.colRef)
        assertEquals("<mi>cm.Sex</mi>", mapping5.modelMapping)
        assertEquals("<mi>MALE</mi>", mapping5.catMappings[0].modelCat)
        assertEquals("<mn>1</mn>", mapping5.catMappings[0].dataCat)
        assertEquals("<mi>FEMALE</mi>", mapping5.catMappings[1].modelCat)
        assertEquals("<mn>2</mn>", mapping5.catMappings[1].dataCat)
        assertEquals("<mi>NEUTRAL</mi>", mapping5.catMappings[2].modelCat)
        assertEquals("<mi>n</mi>", mapping5.catMappings[2].dataCat)
    }

}
