# README #

This project provides a library to convert PharmML into MathML.

Developed by Stuart Moodie on behalf of the DDMoRe Foundation.

Copyright 2017.

## What is this repository for? ##

### Quick summary ###

In order to help the web presentation of PharmML this library reads through the a PharmML document and converts its maths content to MathML.  The information is stored in a set of data structures that can then be easily retrieved by the client application.

### Version ###
1.0-SNAPSHOT

## How do I get set up? ##

### Summary of set up ###

The project is built with maven. So to install:

man clean install