/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import foundation.ddmore.pharmml08.dom.modeldefn.AbsorptionMacroType
import foundation.ddmore.pharmml08.dom.modeldefn.AbsorptionOralMacroType
import foundation.ddmore.pharmml08.dom.modeldefn.AbstractPkMacroType
import foundation.ddmore.pharmml08.dom.modeldefn.CompartmentMacroType
import foundation.ddmore.pharmml08.dom.modeldefn.DepotMacroType
import foundation.ddmore.pharmml08.dom.modeldefn.EffectMacroType
import foundation.ddmore.pharmml08.dom.modeldefn.EliminationMacroType
import foundation.ddmore.pharmml08.dom.modeldefn.IVMacroType
import foundation.ddmore.pharmml08.dom.modeldefn.OralMacroType
import foundation.ddmore.pharmml08.dom.modeldefn.PeripheralMacroType
import foundation.ddmore.pharmml08.dom.modeldefn.PkMacrosType
import foundation.ddmore.pharmml08.dom.modeldefn.TransferMacroType

/**
 * Created by stumoodie on 05/01/2017.
 */
class PkMacroDefinitionExtractor extends MathMlExtractor {

    PkMacroDefinitionExtractor() {
    }

    static String extractPkMacro(AbstractPkMacroType node) {
        def walker = new PkMacroDefinitionExtractor()
        walker.extract(node)
    }

    String extract(AbstractPkMacroType node) {
        def covDefn = builder.bind {
            visitClosure(visit(node, node), delegate)
        }

        covDefn.toString()
    }

//    def visit(AbstractPkMacroType node, parent) {
//        return {
//            node.getAbstractPkMacro().each { pk ->
//                visitClosure(visit(pk), delegate)
//            }
//        }
//    }

    def visit(AbsorptionMacroType node, parent){
        return {
            visitClosure(visitPkBody('absorption', node), delegate)
        }
    }

    def visit(OralMacroType node, parent){
        return {
            visitClosure(visitPkBody('oral', node), delegate)
        }
    }

    def visit(CompartmentMacroType node, parent){
        return {
            visitClosure(visitPkBody('compartment', node), delegate)
        }
    }

    def visit(TransferMacroType node, parent){
        return {
            visitClosure(visitPkBody('transfer', node), delegate)
        }
    }

    def visit(PeripheralMacroType node, parent){
        return {
            visitClosure(visitPkBody('peripheral', node), delegate)
        }
    }

    def visit(IVMacroType node, parent){
        return {
            visitClosure(visitPkBody('iv', node), delegate)
        }
    }

    def visit(EffectMacroType node, parent){
        return {
            visitClosure(visitPkBody('effect', node), delegate)
        }
    }

    def visit(EliminationMacroType node, parent){
        return {
            visitClosure(visitPkBody('elimination', node), delegate)
        }
    }

    def visit(DepotMacroType node, parent){
        return {
            visitClosure(visitPkBody('depot', node), delegate)
        }
    }

    def visitPkBody(String name, AbstractPkMacroType node){
        return {
            mrow {
                mi(name)
                mo {
                    mkp.yieldUnescaped "&ApplyFunction;"
                }
                mfenced {
                    node.getValue().each { val ->
                        mrow {
                            mi val.argument
                            mo "="
                            if(val.assign) {
                                visitClosure(visit(val.assign, node), delegate)
                            }
                            else{
                                visitClosure(visit(val.expression, node), delegate)
                            }
                        }
                    }
                }
            }
        }
    }

 }
