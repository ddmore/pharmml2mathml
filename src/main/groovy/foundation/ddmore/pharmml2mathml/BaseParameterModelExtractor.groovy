/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import foundation.ddmore.pharmml08.dom.modeldefn.*

import javax.xml.bind.JAXBElement

/**
 * Created by stumoodie on 05/01/2017.
 */
abstract class BaseParameterModelExtractor {

    BaseParameterModelExtractor(){
    }

    def visit(JAXBElement<?> node){
        visit(node.getValue())
    }


    def visit(SimpleParameterType node){
        def retVal = [:]
        retVal.symbId = node.symbId
        retVal.maths = ParameterDefinitionExtractor.extractParameterDefn(node)
        retVal.type = 'simpleParam'
        retVal
    }

    def visit(PopulationParameterType node){
        def retVal = [:]
        retVal.symbId = node.symbId
        retVal.maths = ParameterDefinitionExtractor.extractParameterDefn(node)
        retVal.type = 'popParam'
        retVal
    }

    def visit(ParameterRandomVariableType node){
        def retVal = [:]
        retVal.symbId = node.symbId
        retVal.maths = ParameterDefinitionExtractor.extractParameterDefn(node)
        retVal.type = 'rvParam'
        retVal
    }

    def visit(IndividualParameterType node){
        def retVal = [:]
        retVal.symbId = node.symbId
        retVal.maths = ParameterDefinitionExtractor.extractParameterDefn(node)
        retVal.type = 'indivParam'
        retVal
    }

    def visit(Category node){
        node.name.value
    }


    def visit(CorrelationType node) {
        def retVal = [:]
        if (node.pairwise) {
            retVal.maths = ParameterDefinitionExtractor.extractParameterDefn(node)
            if (node.pairwise.covariance){
                retVal.type = 'cov'
            } else {
                retVal.type = 'corr'
            }
        }
        retVal
    }

}

