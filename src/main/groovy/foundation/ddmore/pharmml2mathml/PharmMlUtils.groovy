/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

/**
 * Created by stumoodie on 10/01/2017.
 */
class PharmMlUtils {

    static getContinuousCovariates(Map covModel){
        covModel.covariates.grep{ it.type == 'continuous'}
    }

    static getCategoricalCovariates(Map covModel){
        covModel.covariates.grep{ it.type == 'categorical'}
    }

    static getSimpleParameters(Map covModel){
        covModel.parameters.grep{ it.type == 'simpleParam'}
    }

    static getPopulationParameters(Map covModel){
        covModel.parameters.grep{ it.type == 'popParam'}
    }

}
