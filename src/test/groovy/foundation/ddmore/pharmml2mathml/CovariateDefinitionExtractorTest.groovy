/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import foundation.ddmore.pharmml08.dom.maths.UnaryOperator
import foundation.ddmore.pharmml08.dom.probonto.DistroNameType
import foundation.ddmore.pharmml08.dom.probonto.DistroType
import foundation.ddmore.pharmml08.dom.probonto.ParameterNameType
import org.junit.Test

import static foundation.ddmore.pharmml08.builder.CovariateModelBuilder.*
import static foundation.ddmore.pharmml08.builder.MathsBuilder.*
import static foundation.ddmore.pharmml08.builder.ProbontoBuilder.createDistnParameter
import static foundation.ddmore.pharmml08.builder.ProbontoBuilder.createProbonto
import static foundation.ddmore.pharmml08.builder.UncertMlBuilder.*
import static org.junit.Assert.assertEquals

/**
 * Created by stumoodie on 05/01/2017.
 */
class CovariateDefinitionExtractorTest {

    @Test
    void testContinuousNoRhs(){
        def root = createCovariateDefinition(
                "W",
                createContinuousCovariate()
        )

        def actual = CovariateDefinitionExtractor.extractCovarDefn(root)
        def expected = "<mrow><mi>W</mi></mrow>"
        assertEquals(expected, actual)
    }

    @Test
    void testContinuousDistnRhs(){
        def root = createCovariateDefinition(
                "W",
                createContinuousCovariate(
                        createDistribution(
                                createProbonto(DistroNameType.NORMAL_1, DistroType.UNIVARIATE,
                                        createDistnParameter(ParameterNameType.MEAN, createRhs(
                                                createReal(0)
                                        )),
                                        createDistnParameter(ParameterNameType.STDEV, createRhs(
                                                createReal(1)
                                        ))
                                )
                        )
                )
        )

        def actual = CovariateDefinitionExtractor.extractCovarDefn(root)
        def expected = "<mrow><mi>W</mi><mo>~</mo><mrow><mi>Normal1</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>mean</mi><mo>=</mo><mn>0.0</mn></mrow><mrow><mi>stdev</mi><mo>=</mo><mn>1.0</mn></mrow></mfenced></mrow></mrow>"
        assertEquals(expected, actual)
    }

    @Test
    void testContinuousUncermMLDistnRhs(){
        def root = createCovariateDefinition(
                "W",
                createContinuousCovariate(
                        createDistribution(
                                createUncertml(
                                        createNormalDistnSd(
                                                createContinuousValue(0.0),
                                                createPositiveRealValue(1.0)
                                        )
                                )
                        ),
                        [
                                createTransformedCovariate("logWt",
                                        createRhs(
                                                createUniop(
                                                        UnaryOperator.LOG,
                                                        createSymbRef("W")
                                                )
                                        )
                                )
                        ]
                )
        )

        def actual = CovariateDefinitionExtractor.extractCovarDefn(root)
        def expected = "<mrow><mi>W</mi><mo>~</mo><mrow><mi>Normal</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>mean</mi><mo>=</mo><mn>0.0</mn></mrow><mrow><mi>sd</mi><mo>=</mo><mn>1.0</mn></mrow></mfenced></mrow></mrow>"
        assertEquals(expected, actual)
    }

    @Test
    void testContinuousUncermMLNormVarDistnRhs(){
        def root = createCovariateDefinition(
                "W",
                createContinuousCovariate(
                        createDistribution(
                                createUncertml(
                                        createNormalDistnVar(
                                                createContinuousValue(0.0),
                                                createPositiveRealValue(1.0)
                                        )
                                )
                        ),
                        [
                                createTransformedCovariate("logWt",
                                        createRhs(
                                                createUniop(
                                                        UnaryOperator.LOG,
                                                        createSymbRef("W")
                                                )
                                        )
                                )
                        ]
                )
        )

        def actual = CovariateDefinitionExtractor.extractCovarDefn(root)
        def expected = "<mrow><mi>W</mi><mo>~</mo><mrow><mi>Normal</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>mean</mi><mo>=</mo><mn>0.0</mn></mrow><mrow><mi>var</mi><mo>=</mo><mn>1.0</mn></mrow></mfenced></mrow></mrow>"
        assertEquals(expected, actual)
    }

    @Test
    void testContinuousAssignRhs(){
        def root = createCovariateDefinition(
                "W",
                createContinuousCovariate(
                   createRhs(
                        createReal(0)
                    )
                )
        )

        def actual = CovariateDefinitionExtractor.extractCovarDefn(root)
        def expected = "<mrow><mi>W</mi><mo>=</mo><mn>0.0</mn></mrow>"
        assertEquals(expected, actual)
    }

}
