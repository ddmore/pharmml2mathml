/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import foundation.ddmore.pharmml08.dom.commontypes.DerivativeVariableType
import foundation.ddmore.pharmml08.dom.commontypes.VariableDefinitionType
import foundation.ddmore.pharmml08.dom.modeldefn.AbsorptionMacroType
import foundation.ddmore.pharmml08.dom.modeldefn.AbstractPkMacroType
import foundation.ddmore.pharmml08.dom.modeldefn.CompartmentMacroType
import foundation.ddmore.pharmml08.dom.modeldefn.DepotMacroType
import foundation.ddmore.pharmml08.dom.modeldefn.EffectMacroType
import foundation.ddmore.pharmml08.dom.modeldefn.IVMacroType
import foundation.ddmore.pharmml08.dom.modeldefn.ModelDefinitionType
import foundation.ddmore.pharmml08.dom.modeldefn.OralMacroType
import foundation.ddmore.pharmml08.dom.modeldefn.PeripheralMacroType
import foundation.ddmore.pharmml08.dom.modeldefn.PkMacrosType
import foundation.ddmore.pharmml08.dom.modeldefn.StructuralModelType
import foundation.ddmore.pharmml08.dom.modeldefn.TransferMacroType

/**
 * Created by stumoodie on 05/01/2017.
 */
class StructuralModelExtractor extends BaseParameterModelExtractor {

    final String defaultIdv

    StructuralModelExtractor(String defaultIdv){
        this.defaultIdv = defaultIdv
    }

    static extractStructuralModels(ModelDefinitionType node, String defaultIdv){
        def walker = new StructuralModelExtractor(defaultIdv)
        walker.extractAllDefns(node)
    }

    def extractAllDefns(ModelDefinitionType node){
        visit(node)
    }

    def visit(ModelDefinitionType node){
        def retVal = [ structuralModels : [] ]
        for(def cm : node.getStructuralModel()){
            retVal.structuralModels.add(visit(cm))
        }
        retVal
    }


    def visit(StructuralModelType node){
        def retVal = [
                variables : [],
                parameters : [],
                pkmacros : []
        ]
        def results = []
        node.getPopulationParameterAndIndividualParameterAndParameter().each {p->
            results.add(visit(p))
        }
        for(def res : results.flatten()) {
            if (res.type == 'popParam' || res.type == 'indivParam' || res.type == "simpleParam" || res.type == "simpleParam") {
                retVal.parameters.add(res)
            }
            else if(res.type == "varDefn" || res.type == "derivDefn"){
                retVal.variables.add(res)
            }
            else{
                retVal.pkmacros.add(res)
            }
        }
        retVal.blkId = node.blkId
        retVal
    }

    def visit(VariableDefinitionType node){
        def retVal = [:]
        retVal.symbId = node.symbId
        retVal.maths = CommonVariableDefinitionExtractor.extractCommonVarDefn(node, defaultIdv)
        retVal.type = 'varDefn'
        retVal
    }

    def visit(DerivativeVariableType node){
        def retVal = [:]
        retVal.symbId = node.symbId
        retVal.maths = CommonVariableDefinitionExtractor.extractCommonVarDefn(node, defaultIdv)
        retVal.type = 'derivDefn'
        retVal
    }


    def visit(PkMacrosType node){
        def retVal = []
        node.getAbstractPkMacro().each {pk->
            retVal.add(visit(pk))
        }
        retVal
    }


    def visit(AbstractPkMacroType node){
        def retVal = [:]
        retVal.maths = PkMacroDefinitionExtractor.extractPkMacro(node)
        retVal.type = getPkMacroType(node)
        retVal
    }


    def getPkMacroType(AbstractPkMacroType node){
        String retVal = null
        switch(node.getClass()){
            case AbsorptionMacroType.class:
                retVal = "absorption"
                break;
            case OralMacroType.class:
                retVal = "oral"
                break;
            case CompartmentMacroType.class:
                retVal = "compartment"
                break;
            case DepotMacroType.class:
                retVal = "depot"
                break;
            case EffectMacroType.class:
                retVal = "effect"
                break;
            case TransferMacroType.class:
                retVal = "transfer"
                break;
            case PeripheralMacroType.class:
                retVal = "peripheral"
                break;
            case IVMacroType.class:
                retVal = "iv"
                break;
        }
        retVal
    }
}
