/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import foundation.ddmore.pharmml08.dom.commontypes.LevelReferenceType
import foundation.ddmore.pharmml08.dom.modeldefn.*

import javax.xml.bind.JAXBElement

/**
 * Created by stumoodie on 05/01/2017.
 */
class ParameterDefinitionExtractor extends DistributionExtractor {

    ParameterDefinitionExtractor() {
        super()
    }

    static String extractParameterDefn(CommonParameterType node) {
        def walker = new ParameterDefinitionExtractor()
        walker.extract(node)
    }

    static String extractParameterDefn(CorrelationType node) {
        def walker = new ParameterDefinitionExtractor()
        walker.extract(node)
    }

    static String extractParameterDefn(JAXBElement<?> node) {
        def walker = new ParameterDefinitionExtractor()
        walker.extract(node.value)
    }

    String extract(CommonParameterType node) {
        def covDefn = builder.bind {
            visitClosure(visit(node, node), delegate)
        }

        covDefn.toString()
    }

    String extract(CorrelationType node) {
        def covDefn = builder.bind {
            if(node.pairwise){
                visitClosure(visit(node.pairwise, node), delegate)
            }
        }

        covDefn.toString()
    }

    def visit(PopulationParameterType node, parent) {
        return {
            mrow {
                if(node.getVariabilityReference()){
                    visitClosure(varWithVariability(node.symbId, node, node.getVariabilityReference()), delegate)
//                    def lvlRef = node.getListOfVariabilityReference().head()
//                    msub{
//                        mi(node.symbId)
//                        mrow{
//                            visitClosure(visit(lvlRef), delegate)
//                        }
//                    }
                }
                else{
                    mi(node.symbId)
                }
                if (node.distribution) {
                    mo("~")
                    visitClosure(visit(node.distribution, node), delegate)
                }
                else if (node.assign) {
                    mo("=")
                    visitClosure(visit(node.assign, node), delegate)
                }
            }
        }
    }

    def visit(SimpleParameterType node, parent) {
        return {
            mrow {
                mi(node.symbId)
                if (node.assign) {
                    mo("=")
                    visitClosure(visit(node.assign, node), delegate)
                }
            }
        }
    }


    Closure varWithVariability(String symbId, node, List<LevelReferenceType> varLevels){
        return {
            if (varLevels.isEmpty()) {
                mi(symbId)
            } else {
                msub {
                    mi(symbId)
                    mrow {
                        mfenced(open: "", close: "") {
                            varLevels.each { vl ->
                                mrow {
                                    visitClosure(visit(vl, node), delegate)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    def visit(LevelReferenceType node, parent){
        return {
            visitClosure(visit(node.symbRef, node), delegate)
        }
    }


    def visit(ParameterRandomVariableType node, parent){
        return {
            def nameClosure = varWithVariability(node.symbId, node, node.getVariabilityReference())
            mrow{
                if(node.LHSTransformation){
                    visitClosure(visitTransform(node.LHSTransformation, nameClosure), delegate)
                }
                else{
                    visitClosure(nameClosure, delegate)
                }
                mo("~")
                visitClosure(visit(node.distribution, node), delegate)
            }
        }
    }


    def visitTransform(node, Closure parameterClosure){
        return {
            String opName = ""
            switch (node.type) {
                case TransformationType.BOX_COX:
                    opName = "BoxCox"
                    break
                case TransformationType.LOG:
                    opName = "ln"
                    break
                case TransformationType.LOGIT:
                    opName = "logit"
                    break
                case TransformationType.PROBIT:
                    opName = "probit"
                    break
            }
            if (opName) {
                visitClosure(functionCall(opName, [parameterClosure]), delegate)
            }
            else {
                visitClosure(parameterClosure, delegate)
            }
        }
    }

    def visit(IndividualParameterType node, parent){
        return {
            def nameClosure = varWithVariability(node.symbId, node, node.getVariabilityReference())
            mrow {
                if (node.structuredModel?.transformation) {
                    visitClosure(visitTransform(node.structuredModel.transformation, nameClosure), delegate)
                } else {
                    visitClosure(nameClosure, delegate)
                }
                if(node.structuredModel){
                    mo("=")
                    visitClosure(visit(node.structuredModel, node), delegate)
                }
                else if (node.assign) {
                    mo("=")
                    visitClosure(visit(node.assign, node), delegate)
                }
                else if (node.distribution) {
                    mo("~")
                    visitClosure(visit(node.distribution, node), delegate)
                }
            }
        }
    }

    def visit(StructuredModelType node, parent){
        return {
            if(node.generalCovariate){
                visitClosure(visit(node.generalCovariate, node), delegate)
            }
            else if(node.linearCovariate){
//                visitClosure(visitLinear(node.linearCovariate), delegate)
                visitClosure(visitLinear(node.linearCovariate, node.transformation), delegate)
            }
            if(!node.getRandomEffects().isEmpty()){
                mo("+")
            }
            for(int i = 0; i < node.getRandomEffects().size(); i++){
                visitClosure(visit(node.getRandomEffects().get(i), node), delegate)
                if(i < node.getRandomEffects().size()-1){
                    mo("+")
                }
            }
        }
    }

    def visitLinear(StructuredModelType.LinearCovariate node, transform){
        return {
            if(transform){
                visitClosure(visitTransform(transform,
                        {
                            visitClosure(visit(node.populationValue, node), delegate)
                        }
                         ), delegate)

            }
            else{
                visitClosure(visit(node.populationValue, node), delegate)
            }
            if(!node.getCovariate().isEmpty()) {
                mo("+")
                node.getCovariate().each { c ->
                    visitClosure(visit(c, node), delegate)
                    if (c != node.getCovariate().last()) {
                        mo("+")
                    }
                }
            }
        }
    }

    def visit(StructuredModelType.GeneralCovariate node, parent){
        return {
            visitClosure(visit(node.getAssign(), node), delegate)
        }
    }

    def visit(StructuredModelType.LinearCovariate.PopulationValue node, parent){
        return {
            visitClosure(visit(node.getAssign(), node), delegate)
        }
    }

    def visit(ParameterRandomEffectType node, parent){
        return {
            visitClosure(visit(node.getSymbRef(), node), delegate)
        }
    }

    def visit(CorrelatedRandomVarType node, parent){
        return {
            visitClosure(visit(node.getSymbRef(), node), delegate)
        }
    }

    def visit(CovariateRelationType node, parent){
        return{
            if(!node.getFixedEffect().head().category) {
                visitClosure(visit(node.symbRef, node), delegate)
                mo {
                    mkp.yieldUnescaped(TIMES_OP)
                }
            }
            visitClosure(visit(node.getFixedEffect().head(), node), delegate)
        }
    }

    def visit(FixedEffectRelationType node, CovariateRelationType parent){
        return {
            if(node.getExpression()){
                visitClosure(visit(node.getExpression(), node), delegate)
            }

            if(node.category){
                mo {
                    mkp.yieldUnescaped(TIMES_OP)
                }
                msub{
                    mn(1)
                    mrow{
                        visitClosure(visit(parent.symbRef, node), delegate)
                        mo("=")
                        mi(node.category.catId)
                    }
                }
            }
        }
    }

    def visit(PairwiseType node, parent){
        return{
            def rhsVal = node.covariance?.assign
            mrow {
                if (node.covariance)
                    mi("cov")
                else {
                    mi("corr")
                    rhsVal = node.correlationCoefficient.assign
                }
                mo {
                    mkp.yieldUnescaped APPLY_FUNCTION
                }
                mfenced {
                    mrow {
                        visitClosure(visit(node.randomVariable1, node), delegate)
                    }
                    mrow {
                        visitClosure(visit(node.randomVariable2, node), delegate)
                    }
                }
                mo("=")
                visitClosure(visit(rhsVal, node), delegate)
            }
        }
    }

}
