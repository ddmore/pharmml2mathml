/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import foundation.ddmore.pharmml08.IPharmMLResource
import foundation.ddmore.pharmml08.PharmMlFactory
import foundation.ddmore.pharmml08.impl.LibPharmMLImpl
import groovy.xml.MarkupBuilder

/**
 * Created by stumoodie on 30/01/2017.
 */

class GeneratePharmMLTestHtml {

    def createPharmMLResource(InputStream f) {
        LibPharmMLImpl api = PharmMlFactory.getInstance().createLibPharmML()
        def stream = null
        IPharmMLResource resource = null
        try {
            stream = new BufferedInputStream(f)
            resource = api.createDomFromResource(stream)
        }
        catch (Exception e) {
            e.printStackTrace()
        } finally {
            stream?.close()
            return resource
        }

    }


    def writeTestHtml(String fileName, String outFileName){
        def cl = this.class
        def testFile = GeneratePharmMLTestHtml.getResourceAsStream(fileName)

        assert testFile

        def pr = createPharmMLResource(testFile)
        def writer = new FileWriter(outFileName)
        def builder = new MarkupBuilder(writer)
        builder.setDoubleQuotes(true)

        def pdef = PharmMlExtractor.extractPharmML(pr.dom)

        builder.html {
            head {
//                <meta charset="utf-8">
//                <title>PharmML Output Test File</title>
//    <script type="text/javascript" async src="https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=MML_CHTML"></script>
                meta(charset: "utf-8")
                title "PharmML Output Test File"
                script('', type: "text/javascript", async: null, src: "https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=MML_CHTML")
            }
            body {
                h1 "Name"

                p pdef.name

                h1 "Description"

                p pdef.descn

                h1 "Independent Variables"
                pdef.indVars.each { idv ->
                    math {
                        mi idv
                    }
                }

                h1 "Function Definitions"

                for (def fd : pdef.funcDefns) {
                    table(border: "1px", cellpadding: "5px") {
                        tr {
                            td {
                                math {
                                    mkp.yieldUnescaped fd.maths
                                }
                            }
                        }
                    }
                }

                h1 "Model Definition"

                def covModel = CovariateModelExtractor.extractCovariateModels(pr.getDom().modelDefinition)
                for (def cm : covModel.covariateModels) {
                    h2 "Covariate Model: ${cm.blkId}"
                    h3 "Parameters"
                    table(border: "1px", cellpadding: "5px") {
                        for (def p : cm.parameters) {
                            tr {
                                td {
                                    math {
                                        mkp.yieldUnescaped p.maths
                                    }
                                }
                            }
                        }
                    }
                    h3 "Covariates"
                    table(border: "1px", cellpadding: "5px") {
                        for (def c : cm.covariates) {
                            tr {
                                td {
                                    math {
                                        mkp.yieldUnescaped c.maths
                                    }
                                }
                            }
                        }
                    }
                }
                def parModel = ParameterModelExtractor.extractParametersModels(pr.getDom().modelDefinition)
                for (def pm : parModel.parameterModels) {
                    h2 "Parameter Model: ${pm.blkId}"
                    h3 "Parameters"
                    table(border: "1px", cellpadding: "5px") {
                        for (def p : pm.parameters) {
                            tr {
                                td {
                                    math {
                                        mkp.yieldUnescaped p.maths
                                    }
                                }
                            }
                        }
                    }
                    h3 "Correlations"
                    table(border: "1px", cellpadding: "5px") {
                        for (def p : pm.correlations) {
                            tr {
                                td {
                                    math {
                                        mkp.yieldUnescaped p.maths
                                    }
                                }
                            }
                        }
                    }
                }
                def stModel = StructuralModelExtractor.extractStructuralModels(pr.getDom().modelDefinition, pdef.indVars.head())
                for (def sm : stModel.structuralModels) {
                    h2 "Structural Model: ${sm.blkId}"
                    h3 "Parameters"
                    for (def p : sm.parameters) {
                        tr {
                            td {
                                math {
                                    mkp.yieldUnescaped p.maths
                                }
                            }
                        }
                    }
                    h3 "Variables"
                    table(border: "1px", cellpadding: "5px") {
                        for (def p : sm.variables) {
                            tr {
                                td {
                                    math {
                                        mkp.yieldUnescaped p.maths
                                    }
                                }
                            }
                        }
                    }
                    h3 "PK Macros"
                    table(border: "1px", cellpadding: "5px") {
                        for (def p : sm.pkmacros) {
                            tr {
                                td {
                                    math {
                                        mkp.yieldUnescaped p.maths
                                    }
                                }
                            }
                        }
                    }
                }
                def obsModel = ObservationModelExtractor.extractObservationModels(pr.getDom().modelDefinition)
                for (def pm : obsModel.observationModels) {
                    h2 "Observation Model: ${pm.blkId}"
                    if (pm.continuousObs) {
                        h3 "Continuous Observation"
                        h4 "Parameters"
                        table(border: "1px", cellpadding: "5px") {
                            for (def p : pm.continuousObs.parameters) {
                                tr {
                                    td {
                                        math {
                                            mkp.yieldUnescaped p.maths
                                        }
                                    }
                                }
                            }
                        }
                        h4 "Correlations"
                        table(border: "1px", cellpadding: "5px") {
                            for (def p : pm.continuousObs.correlations) {
                                tr {
                                    td {
                                        math {
                                            mkp.yieldUnescaped p.maths
                                        }
                                    }
                                }
                            }
                        }
                        h4 "Observation Definition"
                        table(border: "1px", cellpadding: "5px") {
                            tr {
                                td {
                                    math {
                                        mkp.yieldUnescaped pm.continuousObs.observation.maths
                                    }
                                }
                            }
                        }
                    } else if (pm.discreteObs) {
                        h4 "Parameters"
                        table(border: "1px", cellpadding: "5px") {
                            for (def p : pm.discreteObs.parameters) {
                                tr {
                                    td {
                                        math {
                                            mkp.yieldUnescaped p.maths
                                        }
                                    }
                                }
                            }
                        }
                        h4 "Correlations"
                        table(border: "1px", cellpadding: "5px") {
                            for (def p : pm.discreteObs.correlations) {
                                tr {
                                    td {
                                        math {
                                            mkp.yieldUnescaped p.maths
                                        }
                                    }
                                }
                            }
                        }

                        h4 "Observation Definition"
                        def dObj = pm.discreteObs.observation
                        table(border: "1px", cellpadding: "5px") {
                            switch (dObj.type) {
                                case ("count"):
                                    tr {
                                        td {
                                            math {
                                                mkp.yieldUnescaped dObj.maths
                                            }
                                        }
                                    }
                                    break;
                                case ("categorical"):
                                    tr {
                                        td {
                                            math {
                                                mkp.yieldUnescaped dObj.maths
                                            }
                                        }
                                    }
                                    break;
                                case ("tte"):
                                    tr {
                                        td {
                                            p("Event variable")
                                        }
                                        td {
                                            math {
                                                mi {
                                                    mkp.yieldUnescaped dObj.symbId
                                                }
                                            }
                                        }
                                    }
                                    tr {
                                        td {
                                            p("Hazard")
                                        }
                                        td {
                                            math {
                                                mkp.yieldUnescaped dObj.maths
                                            }
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                }

                h1 "Trial Design"

                for(def ed : pdef.trialDesign.extnlDatasets){
                    h2 "External Dataset"

                    table(border: "1px", cellpadding: "5px") {
                        tr {
                            td {
                                p "OID"
                            }
                            td {
                                math {
                                    mi {
                                        mkp.yieldUnescaped ed.oid
                                    }
                                }
                            }
                        }
                        tr {
                            td {
                                p "Tool Format"
                            }
                            td {
                                p ed.toolname
                            }
                        }
                        if (ed.description) {
                            tr {
                                td {
                                    p "Description"
                                }
                                td {
                                    p ed.description
                                }
                            }
                        }
                    }

                    h3 "File Specification"

                    if(ed.dataset.fileSpec){
                        table(border: "1px", cellpadding: "5px") {
                            tr {
                                td {
                                    p "Format"
                                }
                                td {
                                    if(ed.dataset.fileSpec.format != null) {
                                        math {
                                            mi {
                                                mkp.yieldUnescaped ed.dataset.fileSpec.format
                                            }
                                        }
                                    }
                                }
                            }
                            tr {
                                td {
                                    p "Delimiter"
                                }
                                td {
                                    if(ed.dataset.fileSpec.delimiter != null) {
                                        math {
                                            mo {
                                                mkp.yieldUnescaped ed.dataset.fileSpec.delimiter
                                            }
                                        }
                                    }
                                }
                            }
                            tr {
                                td {
                                    p "File Location"
                                }
                                td {
                                    p ed.dataset.fileSpec.path
                                }
                            }
                        }
                    }

                    h3 "Column Definitions"

                    table(border: "1px", cellpadding: "5px") {
                        tr {
                            th "Column ID"
                            th "Position"
                            th "Column Type"
                            th "Value Type"
                        }
                        for (def pte : ed.dataset.definitions) {
                            tr {
                                td {
                                    math {
                                        mi {
                                            mkp.yieldUnescaped pte.colId
                                        }
                                    }
                                }
                                td {
                                    math {
                                        mn{
                                            mkp.yieldUnescaped pte.colNum
                                        }
                                    }
                                }
                                td {
                                    math {
                                        mi{
                                            mkp.yieldUnescaped pte.colType
                                        }
                                    }
                                }
                                td {
                                    math {
                                        mi {
                                            mkp.yieldUnescaped pte.valType
                                        }
                                    }
                                }
                            }
                        }
                    }



                    h3 "Data to Model Mappings"

                    table(border: "1px", cellpadding: "5px") {
                        tr {
                            th "Column Ref"
                            th "Model Mapping"
//                            th {
//                                div "Adm Target: "
//                                math{
//                                    mi "blkRef"
//                                    mfenced{
//                                        mrow{
//                                            mi "adm"
//                                        }
//                                    }
//                                }
//                            }
                        }
                        for (def cm : ed.columnMappings) {
                            tr {
                                td {
                                    math {
                                        mi {
                                            mkp.yieldUnescaped cm.colRef
                                        }
                                    }
                                }
                                td {
                                    math {
                                        if(cm.modelMapping)
                                            mkp.yieldUnescaped cm.modelMapping
                                    }
                                }
//                                td {
//                                    math {
//                                        for(def tm : cm.targetMappings){
//                                            mi {
//                                                mkp.yieldUnescaped tm.blkIdRef
//                                            }
//                                            mfenced {
//                                                for(def a : tm.adms) {
//                                                    mrow {
//                                                        mn a
//                                                    }
//                                                }
//                                            }
//                                        }
//                                    }
//                                }
                            }
                        }
                    }
                }

                h1 "Modelling Steps"
                if (pdef.modellingSteps.estimationSteps) {
                    for (def es : pdef.modellingSteps.estimationSteps) {
                        h2 "Estimation Step"
                        table(border: "1px", cellpadding: "5px") {
                            tr {
                                td {
                                    p "OID"
                                }
                                td {
                                    math {
                                        mi {
                                            mkp.yieldUnescaped es.oid
                                        }
                                    }
                                }
                            }
                            if (es.name) {
                                tr {
                                    td {
                                        p "Name"
                                    }
                                    td {
                                        p es.name
                                    }
                                }

                            }
                            if (es.description) {
                                tr {
                                    td {
                                        p "Description"
                                    }
                                    td {
                                        p es.description
                                    }
                                }
                            }
                            tr {
                                td {
                                    p "Dataset OidRef"
                                }
                                td {
                                    math {
                                        mi {
                                            mkp.yieldUnescaped es.extDatasetRef
                                        }
                                    }
                                }
                            }
                        }

                        h3 "Parameters To Estimation"

                        table(border: "1px", cellpadding: "5px") {
                            tr {
                                th "Parameter"
                                th "Initial value"
                                th "Fixed?"
                                th "Limits"
                            }
                            for (def pte : es.parametersToEstimate) {
                                tr {
                                    td {
                                        math {
                                            mi {
                                                mkp.yieldUnescaped pte.symbRef
                                            }
                                        }
                                    }
                                    td {
                                        if(pte.initialValue.value){
                                            math {
                                                mkp.yieldUnescaped pte.initialValue.value
                                            }
                                        }
                                    }
                                    td {
                                        math {
                                            mi {
                                                mkp.yieldUnescaped pte.initialValue.fixed
                                            }
                                        }
                                    }
                                    td {
                                        math {
                                            mfenced {
                                                mrow {
                                                    if (pte.lower) {
                                                        mkp.yieldUnescaped pte.lower
                                                    }
                                                }
                                                mrow {
                                                    if (pte.upper)
                                                        mkp.yieldUnescaped pte.upper
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        h3 "Operations"

                        for (def op : es.operations) {
                            table(border: "1px", cellpadding: "5px") {
                                tr {
                                    tr {
                                        td {
                                            p "Order"
                                        }
                                        td {
                                            math {
                                                mn {
                                                    mkp.yieldUnescaped op.order
                                                }
                                            }
                                        }
                                    }
                                    if (es.name) {
                                        tr {
                                            td {
                                                p "Name"
                                            }
                                            td {
                                                p op.name
                                            }
                                        }

                                    }
                                    if (es.description) {
                                        tr {
                                            td {
                                                p "Description"
                                            }
                                            td {
                                                p op.description
                                            }
                                        }
                                    }
                                    tr {
                                        td {
                                            p "Op Type"
                                        }
                                        td {
                                            math {
                                                mi {
                                                    mkp.yieldUnescaped op.opType
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            h4 "Properties"

                            table(border: "1px", cellpadding: "5px") {
                                tr {
                                    th "Name"
                                    th "Value"
                                }
                                for (def prop : op.properties) {
                                    tr {
                                        td {
                                            p prop.name
                                        }
                                        td {
                                            math {
                                                mkp.yieldUnescaped prop.value
                                            }
                                        }
                                    }
                                }
                            }

                            h4 "Algorithm"

                            def alg = op.algorithm
                            if(alg) {
                                table(border: "1px", cellpadding: "5px") {
                                    tr {
                                        td {
                                            p "Name"
                                        }
                                        td {
                                            p alg.name
                                        }
                                    }
                                    tr {
                                        td {
                                            p "Definition"
                                        }
                                        td {
                                            p alg.defn
                                        }
                                    }
                                }

                                h5 "Algorithm Properties"

                                table(border: "1px", cellpadding: "5px") {
                                    tr {
                                        th "Name"
                                        th "Value"
                                    }
                                    for (def prop : alg.properties) {
                                        tr {
                                            td {
                                                p prop.name
                                            }
                                            td {
                                                math {
                                                    mkp.yieldUnescaped prop.value
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else{
                                p "No algorithm defined"
                            }

                        }
                    }
                }

                if (pdef.modellingSteps.simulationSteps) {
                    for (def ss : pdef.modellingSteps.simulationSteps) {
                        h2 "Simulation Step"
                        table(border: "1px", cellpadding: "5px") {
                            tr {
                                td {
                                    p "OID"
                                }
                                td {
                                    math {
                                        mi {
                                            mkp.yieldUnescaped ss.oid
                                        }
                                    }
                                }
                            }
                            if (ss.name) {
                                tr {
                                    td {
                                        p "Name"
                                    }
                                    td {
                                        p ss.name
                                    }
                                }

                            }
                            if (ss.description) {
                                tr {
                                    td {
                                        p "Description"
                                    }
                                    td {
                                        p ss.description
                                    }
                                }
                            }
                            tr {
                                td {
                                    p "Dataset OidRef"
                                }
                                td {
                                    math {
                                        mi {
                                            if(ss.extDatasetRef)
                                                mkp.yieldUnescaped ss.extDatasetRef
                                        }
                                    }
                                }
                            }
                        }

                        h3 "Variable Assignments"

                        table(border: "1px", cellpadding: "5px") {
                            tr {
                                th "Variable"
                                th "Value"
                            }
                            for (def pte : ss.variableAssignments) {
                                tr {
                                    td {
                                        math {
                                            mi {
                                                mkp.yieldUnescaped pte.symbRef
                                            }
                                        }
                                    }
                                    td {
                                        math {
                                            mkp.yieldUnescaped pte.maths
                                        }
                                    }
                                }
                            }
                        }

                        h3 "Operations"

                        for (def op : ss.operations) {
                            table(border: "1px", cellpadding: "5px") {
                                tr {
                                    tr {
                                        td {
                                            p "Order"
                                        }
                                        td {
                                            math {
                                                mn {
                                                    mkp.yieldUnescaped op.order
                                                }
                                            }
                                        }
                                    }
                                    if (ss.name) {
                                        tr {
                                            td {
                                                p "Name"
                                            }
                                            td {
                                                p op.name
                                            }
                                        }

                                    }
                                    if (ss.description) {
                                        tr {
                                            td {
                                                p "Description"
                                            }
                                            td {
                                                p op.description
                                            }
                                        }
                                    }
                                    tr {
                                        td {
                                            p "Op Type"
                                        }
                                        td {
                                            math {
                                                mi {
                                                    mkp.yieldUnescaped op.opType
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            h4 "Properties"

                            table(border: "1px", cellpadding: "5px") {
                                tr {
                                    th "Name"
                                    th "Value"
                                }
                                for (def prop : op.properties) {
                                    tr {
                                        td {
                                            p prop.name
                                        }
                                        td {
                                            math {
                                                mkp.yieldUnescaped prop.value
                                            }
                                        }
                                    }
                                }
                            }

                            h4 "Algorithm"

                            def alg = op.algorithm
                            if(alg) {
                                table(border: "1px", cellpadding: "5px") {
                                    tr {
                                        td {
                                            p "Name"
                                        }
                                        td {
                                            p alg.name
                                        }
                                    }
                                    tr {
                                        td {
                                            p "Definition"
                                        }
                                        td {
                                            p alg.defn
                                        }
                                    }
                                }

                                h5 "Algorithm Properties"

                                table(border: "1px", cellpadding: "5px") {
                                    tr {
                                        th "Name"
                                        th "Value"
                                    }
                                    for (def prop : alg.properties) {
                                        tr {
                                            td {
                                                p prop.name
                                            }
                                            td {
                                                math {
                                                    mkp.yieldUnescaped prop.value
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else{
                                p "No algorithm defined"
                            }

                        }
                    }
                }

                h2 "Step Dependencies"

                table(border: "1px", cellpadding: "5px") {
                    tr {
                        th "Steps"
                        th "Dependencies"
                    }
                    for (def pte : pdef.modellingSteps.stepDependencies) {
                        tr {
                            td {
                                p{
                                    "foo"
                                }
                                math {
                                    mi {
                                        mkp.yieldUnescaped pte.oidRef
                                    }
                                }
                            }
                            td {
                                math {
                                    for(def depOid : pte.depOids) {
                                            mi{
                                                mkp.yieldUnescaped depOid
                                            }
                                    }
                                }
                            }
                        }
                    }
                }

            }
        }

        writer.close()

    }

    static void main(String[] args) {
        new GeneratePharmMLTestHtml().writeTestHtml("example1.xml", 'testPharmmlOut.html')
        new GeneratePharmMLTestHtml().writeTestHtml("UseCase6_3.xml", 'testPharmmlOutUC63.html')
        new GeneratePharmMLTestHtml().writeTestHtml("UseCase6_3Multitarget.xml", 'testPharmmlOutUC63Multitarget.html')
        new GeneratePharmMLTestHtml().writeTestHtml("UseCase1.xml", 'testPharmmlOutUC1.html')
        new GeneratePharmMLTestHtml().writeTestHtml("UseCase11.xml", 'testPharmmlOutUC11.html')
        new GeneratePharmMLTestHtml().writeTestHtml("UseCase12.xml", 'testPharmmlOutUC12.html')
        new GeneratePharmMLTestHtml().writeTestHtml("UseCase13.xml", 'testPharmmlOutUC13.html')
        new GeneratePharmMLTestHtml().writeTestHtml("UseCase14.xml", 'testPharmmlOutUC14.html')
        new GeneratePharmMLTestHtml().writeTestHtml("Executable_Russu_2009_dose_escalation_power.xml", 'Executable_Russu_2009_dose_escalation_power.html')
        new GeneratePharmMLTestHtml().writeTestHtml("Magni_2000_diabetes_C-peptide.xml", 'Magni_2000_diabetes_C-peptide.html')
        new GeneratePharmMLTestHtml().writeTestHtml("Executable_Magni_2004_diabetes_IVGTT.xml", 'Executable_Magni_2004_diabetes_IVGTT.html')
        new GeneratePharmMLTestHtml().writeTestHtml("Executable_Magni_2000_diabetes_C-peptide.xml", 'Executable_Magni_2000_diabetes_C-peptide.html')
    }

}
