/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import foundation.ddmore.pharmml08.dom.probonto.DistroNameType
import foundation.ddmore.pharmml08.dom.probonto.DistroType
import foundation.ddmore.pharmml08.dom.probonto.ParameterNameType
import org.junit.Test

import static foundation.ddmore.pharmml08.builder.MathsBuilder.*
import static foundation.ddmore.pharmml08.builder.ParameterModelBuilder.*
import static foundation.ddmore.pharmml08.builder.PharmMlBuilder.createModelDefinition
import static foundation.ddmore.pharmml08.builder.ProbontoBuilder.createDistnParameter
import static foundation.ddmore.pharmml08.builder.ProbontoBuilder.createProbonto
import static org.junit.Assert.assertEquals

/**
 * Created by stumoodie on 05/01/2017.
 */
class ParameterModelExtractorTest {


    @Test
    void testSingleSimpleDefinition(){
        def root = createModelDefinition(
                createParameterModel("pm",
                    createPopulationParam(
                            "W"
                    )
            )
        )

        def actualResult = ParameterModelExtractor.extractParametersModels(root)
        assertEquals(1, actualResult.parameterModels.size())
        assertEquals("pm", actualResult.parameterModels.get(0).blkId)
        def defnList = actualResult.parameterModels.get(0)
        def expectedDefns = [
                [
                        symbId : "W",
                        maths : "<mrow><mi>W</mi></mrow>",
                        type : "popParam",
                ]
        ]
        assertEquals(expectedDefns.size(), defnList.parameters.size())
        assertEquals(1, defnList.parameters.size())
        for(int i; i < expectedDefns.size(); i++){
            assert expectedDefns[i] == defnList.parameters.get(i)
        }
    }

    @Test
    void testMultipleDefinitions(){
        def root = createModelDefinition(createParameterModel("pm",
                createPopulationParam(
                        "pop",
                        createDistribution(
                                createProbonto(
                                        DistroNameType.WISHART_1,
                                        DistroType.UNIVARIATE,
                                        createDistnParameter(
                                                ParameterNameType.DEGREES_OF_FREEDOM,
                                                createRhs(
                                                        createInt(3)
                                                )
                                        )
                                )
                        ),
                        createVariabilityRef("vm", "prior")
                ),
                createRandomVariable(
                        "rv",
                        createDistribution(
                                createProbonto(
                                        DistroNameType.WISHART_1,
                                        DistroType.UNIVARIATE,
                                        createDistnParameter(
                                                ParameterNameType.DEGREES_OF_FREEDOM,
                                                createRhs(
                                                        createInt(3)
                                                )
                                        )
                                )
                        ),
                        [ createVariabilityRef("vm", "prior") ]
                ),
                createStructuredIndividualParameter(
                        "idv",
                        createStructuredModel(
                                createLinearCovariate(
                                        createRhs(
                                                createSymbRef("pop")
                                        ),
                                        [createCovariateRelation(
                                                createSymbRefType("cm", "C1"),
                                                createFixedEffectRelationParamRef(
                                                        createSymbRefType("beta")
                                                )
                                        )]
                                ),
                                [ createSymbRefType("rv1")]
                        )
                )
        )
        )

        def defnList = ParameterModelExtractor.extractParametersModels(root)
        assertEquals(1, defnList.parameterModels.size())
        assertEquals(3, defnList.parameterModels[0].parameters.size())

        def expectedDefns = [
                [
                        symbId : "pop",
                        maths : "<mrow><msub><mi>pop</mi><mrow><mfenced open=\"\" close=\"\"><mrow><mi>vm.prior</mi></mrow></mfenced></mrow></msub><mo>~</mo><mrow><mi>Wishart1</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>degreesoffreedom</mi><mo>=</mo><mn>3</mn></mrow></mfenced></mrow></mrow>",
                        type : "popParam"
                ],
                [
                        symbId : "rv",
                        maths : "<mrow><msub><mi>rv</mi><mrow><mfenced open=\"\" close=\"\"><mrow><mi>vm.prior</mi></mrow></mfenced></mrow></msub><mo>~</mo><mrow><mi>Wishart1</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>degreesoffreedom</mi><mo>=</mo><mn>3</mn></mrow></mfenced></mrow></mrow>",
                        type : "rvParam"
                ],
                [
                        symbId : "idv",
                        maths : "<mrow><mi>idv</mi><mo>=</mo><mi>pop</mi><mo>+</mo><mi>cm.C1</mi><mo>&#x022C5;</mo><mi>beta</mi><mo>+</mo><mi>rv1</mi></mrow>",
                        type : "indivParam"
                ]

//                "<mrow><msub><mi>rv</mi><mrow><mfenced open=\"\" close=\"\"><mrow><mi>vm.prior</mi></mrow></mfenced></mrow></msub><mo>~</mo><mrow><mi>Wishart1</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>degreesoffreedom</mi><mo>=</mo><mn>3</mn></mrow></mfenced></mrow></mrow>",
//                "<mrow><mi>ln</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>idv</mi></mrow></mfenced><mo>=</mo><mi>ln</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>pop</mi></mrow></mfenced><mo>+</mo><mi>cm.C1</mi><mo>&#x022C5;</mo><mi>beta</mi><mo>+</mo><mi>rv1</mi></mrow>"
        ]

        for(int i; i < expectedDefns.size(); i++){
            assertEquals(expectedDefns[i].symbId, defnList.parameterModels[0].parameters.get(i).symbId)
            assertEquals(expectedDefns[i].maths, defnList.parameterModels[0].parameters.get(i).maths)
            assertEquals(expectedDefns[i].type, defnList.parameterModels[0].parameters.get(i).type)
        }
    }

    @Test
    void testMultipleModels(){
        def root = createModelDefinition(
                createParameterModel("pm1",
                        createPopulationParam(
                                "W"
                        ),
                ),
                createParameterModel("pm2",
                        createPopulationParam(
                                "Y"
                        ),
                )
        )

        def defnList = ParameterModelExtractor.extractParametersModels(root)
        assertEquals(2, defnList.parameterModels.size())
        def expectedDefns = [
                "<mrow><mi>W</mi></mrow>",
                "<mrow><mi>Y</mi></mrow>",
        ]
        for(int i; i < expectedDefns.size(); i++){
            assertEquals(expectedDefns[i], defnList.parameterModels[i].parameters.get(0).maths)
        }
    }

    @Test
    void testMultipleDefinitionsWithCorrelation(){
        def root = createModelDefinition(createParameterModel("pm",
                createPopulationParam(
                        "pop",
                        createDistribution(
                                createProbonto(
                                        DistroNameType.WISHART_1,
                                        DistroType.UNIVARIATE,
                                        createDistnParameter(
                                                ParameterNameType.DEGREES_OF_FREEDOM,
                                                createRhs(
                                                        createInt(3)
                                                )
                                        )
                                )
                        ),
                        createVariabilityRef("vm", "prior")
                ),
                createRandomVariable(
                        "rv",
                        createDistribution(
                                createProbonto(
                                        DistroNameType.WISHART_1,
                                        DistroType.UNIVARIATE,
                                        createDistnParameter(
                                                ParameterNameType.DEGREES_OF_FREEDOM,
                                                createRhs(
                                                        createInt(3)
                                                )
                                        )
                                )
                        ),
                        [ createVariabilityRef("vm", "prior") ]
                ),
                createStructuredIndividualParameter(
                        "idv",
                        createStructuredModel(
                                createLinearCovariate(
                                        createRhs(
                                                createSymbRef("pop")
                                        ),
                                        [createCovariateRelation(
                                                createSymbRefType("cm", "C1"),
                                                createFixedEffectRelationParamRef(
                                                        createSymbRefType("beta")
                                                )
                                        )]
                                ),
                                [ createSymbRefType("rv1")]
                        )
                ),
                createRvCorrelationCorr(createRhs(createSymbRef("p34")),
                                        createSymbRefType("rv1"),
                                        createSymbRefType("rv2")
                                        ),
                createRvCorrelationCov(createRhs(createSymbRef("coVar")),
                                        createSymbRefType("rv3"),
                                        createSymbRefType("rv4")
                                        )
                )
        )

        def defnList = ParameterModelExtractor.extractParametersModels(root)
        assertEquals(1, defnList.parameterModels.size())
        assertEquals(3, defnList.parameterModels[0].parameters.size())
        assertEquals(2, defnList.parameterModels[0].correlations.size())

        def expectedDefns = [
                [
                        symbId : "pop",
                        maths : "<mrow><msub><mi>pop</mi><mrow><mfenced open=\"\" close=\"\"><mrow><mi>vm.prior</mi></mrow></mfenced></mrow></msub><mo>~</mo><mrow><mi>Wishart1</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>degreesoffreedom</mi><mo>=</mo><mn>3</mn></mrow></mfenced></mrow></mrow>",
                        type : "popParam"
                ],
                [
                        symbId : "rv",
                        maths : "<mrow><msub><mi>rv</mi><mrow><mfenced open=\"\" close=\"\"><mrow><mi>vm.prior</mi></mrow></mfenced></mrow></msub><mo>~</mo><mrow><mi>Wishart1</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>degreesoffreedom</mi><mo>=</mo><mn>3</mn></mrow></mfenced></mrow></mrow>",
                        type : "rvParam"
                ],
                [
                        symbId : "idv",
                        maths : "<mrow><mi>idv</mi><mo>=</mo><mi>pop</mi><mo>+</mo><mi>cm.C1</mi><mo>&#x022C5;</mo><mi>beta</mi><mo>+</mo><mi>rv1</mi></mrow>",
                        type : "indivParam"
                ]
        ]

        for(int i; i < expectedDefns.size(); i++){
            assertEquals(expectedDefns[i].symbId, defnList.parameterModels[0].parameters.get(i).symbId)
            assertEquals(expectedDefns[i].maths, defnList.parameterModels[0].parameters.get(i).maths)
            assertEquals(expectedDefns[i].type, defnList.parameterModels[0].parameters.get(i).type)
        }

        def expectedDefnsCorr = [
                [
                        maths : "<mrow><mi>corr</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>rv1</mi></mrow><mrow><mi>rv2</mi></mrow></mfenced><mo>=</mo><mi>p34</mi></mrow>",
                        type : "corr"
                ],
                [
                        maths : "<mrow><mi>cov</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>rv3</mi></mrow><mrow><mi>rv4</mi></mrow></mfenced><mo>=</mo><mi>coVar</mi></mrow>",
                        type : "cov"
                ]
        ]

        for(int i; i < expectedDefnsCorr.size(); i++){
            assertEquals(expectedDefnsCorr[i].maths, defnList.parameterModels[0].correlations.get(i).maths)
            assertEquals(expectedDefnsCorr[i].type, defnList.parameterModels[0].correlations.get(i).type)
        }
    }

}
