/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import foundation.ddmore.pharmml08.IPharmMLResource
import foundation.ddmore.pharmml08.PharmMlFactory
import foundation.ddmore.pharmml08.impl.LibPharmMLImpl
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Ignore
import org.junit.Test

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertEquals

/**
 * Created by smoodie on 16/12/2016.
 */
class PharmMLModelExtractorIntegrationTest {
    IPharmMLResource resource = null


    def createPharmMLResource(InputStream f){
        LibPharmMLImpl api = PharmMlFactory.getInstance().createLibPharmML()
        def stream = null
        IPharmMLResource resource = null
        try {
            stream = new BufferedInputStream(f)
            resource = api.createDomFromResource(stream)
        } finally {
            stream?.close()
            return resource
        }

    }

    @Before
    void setUp(){
        def testFile = this.class.getResourceAsStream("example1.xml")

        assert testFile

        resource = createPharmMLResource(testFile)
    }

    @After
    void tearDown(){
        resource = null
    }


    @Test
    void testNameDefined(){
        def result = PharmMlExtractor.extractPharmML(resource.dom)

        Assert.assertNotNull(result)
        Assert.assertEquals("Example 1 - simulation continuous PK/PD", result.name)
    }


    @Test
    void testExpectedIdv(){
        def result = PharmMlExtractor.extractPharmML(resource.dom)

        Assert.assertNotNull(result)
        Assert.assertEquals(1, result.indVars.size())
        Assert.assertEquals("t", result.indVars.get(0))
    }

    @Test
    void testCanParseCovModel(){
        def result = PharmMlExtractor.extractPharmML(resource.dom).modelDefinition
        Assert.assertNotNull(result)
        Assert.assertEquals(1, result.covariateModels.size())
        def covModel = result.covariateModels.get(0)
        Assert.assertNotNull(covModel)
        Assert.assertEquals("c1", covModel.blkId)
        Assert.assertEquals(2, covModel.parameters.size())
        Assert.assertEquals(covModel.parameters.get(0).symbId, "pop_W")
        Assert.assertEquals(covModel.parameters.get(1).symbId, "omega_W")
        Assert.assertEquals(0, covModel.covariates.grep{ n->
            n.type == 'categorical' }.size())
        Assert.assertEquals(2, covModel.covariates.grep{ it.type == 'continuous' }.size())
        Assert.assertEquals("W", covModel.covariates.get(0).symbId)
        def expectedDisn = "<mrow><mi>W</mi><mo>~</mo><mrow><mi>Normal</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>mean</mi><mo>=</mo><mi>pop_W</mi></mrow><mrow><mi>var</mi><mo>=</mo><mi>omega_W</mi></mrow></mfenced></mrow></mrow>"
        Assert.assertEquals(expectedDisn, covModel.covariates.get(0).maths)
        Assert.assertEquals("logtW", covModel.covariates.get(1).symbId)
        def expectedLogW = "<mrow><mi>logtW</mi><mo>=</mo><mi>ln</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mfrac><mrow><mi>W</mi></mrow><mrow><mn>70.0</mn></mrow></mfrac></mrow></mfenced></mrow>"
        Assert.assertEquals(expectedLogW, covModel.covariates.get(1).maths)
    }

    @Test
    void testCanParseParamModel(){
        def result = PharmMlExtractor.extractPharmML(resource.dom).modelDefinition
        Assert.assertNotNull(result)
        Assert.assertEquals(1, result.parameterModels.size())
        def paramModel = result.parameterModels.get(0)
        Assert.assertNotNull(paramModel)
        Assert.assertEquals("p1", paramModel.blkId)
        Assert.assertEquals(1, paramModel.correlations.size())
        Assert.assertEquals(31, paramModel.parameters.size())
        Assert.assertEquals(paramModel.parameters.get(0).symbId, "beta_V")
        Assert.assertEquals(paramModel.parameters.get(1).symbId, "pop_V")
        def expectedPopV = "<mrow><mi>pop_V</mi></mrow>"
        Assert.assertEquals(expectedPopV, paramModel.parameters.get(1).maths)
        Assert.assertEquals("eta_V", paramModel.parameters.get(3).symbId)
        def expectedEtaV = "<mrow><msub><mi>eta_V</mi><mrow><mfenced open=\"\" close=\"\"><mrow><mi>modelVar.indiv</mi></mrow></mfenced></mrow></msub><mo>~</mo><mrow><mi>Normal</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>mean</mi><mo>=</mo><mn>0.0</mn></mrow><mrow><mi>sd</mi><mo>=</mo><mi>omega_V</mi></mrow></mfenced></mrow></mrow>"
        Assert.assertEquals(expectedEtaV, paramModel.parameters.get(3).maths)
        Assert.assertEquals("V", paramModel.parameters.get(4).symbId)
        def expectedV = "<mrow><mi>ln</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>V</mi></mrow></mfenced><mo>=</mo><mi>ln</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>pop_V</mi></mrow></mfenced><mo>+</mo><mi>c1.W</mi><mo>&#x022C5;</mo><mi>beta_V</mi><mo>+</mo><mi>eta_V</mi></mrow>"
        Assert.assertEquals(expectedV, paramModel.parameters.get(4).maths)
        Assert.assertEquals("indivParam", paramModel.parameters.get(4).type)
    }

    @Test
    void testCanParseStructModel(){
        def result = PharmMlExtractor.extractPharmML(resource.dom).modelDefinition
        Assert.assertNotNull(result)
        Assert.assertEquals(1, result.structuralModels.size())
        def structModel = result.structuralModels.get(0)
        Assert.assertNotNull(structModel)
        Assert.assertEquals("sm1", structModel.blkId)
        Assert.assertEquals(0, structModel.parameters.size())
        Assert.assertEquals(5, structModel.variables.size())
        Assert.assertEquals(0, structModel.pkmacros.size())

        Assert.assertEquals(structModel.variables.get(0).symbId, "k")
        def expectedk = "<mrow><mi>k</mi><mo>=</mo><mfrac><mrow><mi>p1.Cl</mi></mrow><mrow><mi>p1.V</mi></mrow></mfrac></mrow>"
        Assert.assertEquals(expectedk, structModel.variables.get(0).maths)
        Assert.assertEquals("varDefn", structModel.variables.get(0).type)

        Assert.assertEquals("Ad", structModel.variables.get(1).symbId)
        def expectedAd = "<mrow><mtable groupalign=\"{left}\"><mtr><mtd><mrow><mfrac><mrow><mo>&DifferentialD;</mo></mrow><mrow><mo>&DifferentialD;</mo><mi>t</mi></mrow></mfrac><mi>Ad</mi><mrow><maligngroup/><mo>=</mo><mo form=\"prefix\">-</mo><mi>p1.ka</mi><mo>${MathMlExtractor.TIMES_OP}</mo><mi>Ad</mi></mrow></mrow></mtd></mtr><mtr><mtd><mrow><mi>Ad</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>t</mi><mo>=</mo><mn>0</mn></mrow></mfenced><maligngroup/><mo>=</mo><mn>0.0</mn></mrow></mtd></mtr></mtable></mrow>"
        Assert.assertEquals(expectedAd.toString(), structModel.variables.get(1).maths)
        Assert.assertEquals("derivDefn", structModel.variables.get(1).type)
    }

    @Ignore
    // need to do when extractor handles simulation test
    void testCanParseModellingStep(){
        def result = PharmMlExtractor.extractPharmML(resource.dom).modellingSteps
        Assert.assertNotNull(result)
        Assert.assertEquals(1, result.estimationSteps.size())
        def estStep = result.estimationSteps.get(0)
        Assert.assertNotNull(estStep)
        Assert.assertEquals("estimStep_1", estStep.oid)
        Assert.assertNull(estStep.name)
        Assert.assertNull(estStep.description)
        Assert.assertEquals("estimation", estStep.type)

        def expectedParams = [
                [
                        symbRef : "pm.POP_CL",
                        initial : "<mn>0.1</mn>",
                        fixed : false,
                        lower : "<mn>0.001</mn>",
                        upper : null
                ],
                [
                        symbRef : "pm.POP_V",
                        initial : "<mn>8</mn>",
                        fixed : false,
                        lower : "<mn>0.001</mn>",
                        upper : null
                ],
                [
                        symbRef : "pm.POP_KA",
                        initial : "<mn>0.362</mn>",
                        fixed : false,
                        lower : "<mn>0.001</mn>",
                        upper : null
                ],
                [
                        symbRef : "pm.POP_TLAG",
                        initial : "<mn>1</mn>",
                        fixed : false,
                        lower : "<mn>0.001</mn>",
                        upper : null
                ],
                [
                        symbRef : "pm.BETA_CL_WT",
                        initial : "<mn>0.75</mn>",
                        fixed : true,
                        lower : null,
                        upper : null
                ],
                [
                        symbRef : "pm.BETA_V_WT",
                        initial : "<mn>1</mn>",
                        fixed : true,
                        lower : null,
                        upper : null
                ],
                [
                        symbRef : "pm.PPV_CL_V_KA",
                        initial : "<mrow><mo>(</mo><mtable><mtr><mtd><mn>0.0</mn></mtd></mtr><mtr><mtd><mn>0.01</mn></mtd><mtd><mn>0.0</mn></mtd></mtr><mtr><mtd><mn>0.01</mn></mtd><mtd><mn>0.01</mn></mtd><mtd><mn>0.0</mn></mtd></mtr></mtable><mo>)</mo></mrow>",
                        fixed : false,
                        lower : null,
                        upper : null
                ],
                [
                        symbRef : "pm.DF_CL_V_KA",
                        initial : "<mn>3</mn>",
                        fixed : true
                ],
        ]

        assertEquals(11, estStep.parametersToEstimate.size())

        for(int i = 0; i < expectedParams.size(); i++){
            assertEquals(expectedParams[i].symbRef, estStep.parametersToEstimate[i].symbRef)
            assertEquals(expectedParams[i].initial, estStep.parametersToEstimate[i].initialValue.value)
            assertEquals(expectedParams[i].fixed, estStep.parametersToEstimate[i].initialValue.fixed)
            assertEquals(expectedParams[i].lower, estStep.parametersToEstimate[i].lower)
            assertEquals(expectedParams[i].upper, estStep.parametersToEstimate[i].upper)
        }
    }

    @Test
    void testCanParseTrialDesign(){
        def result = PharmMlExtractor.extractPharmML(resource.dom).trialDesign
        Assert.assertNotNull(result)
        Assert.assertEquals(0, result.extnlDatasets.size())
    }

}
