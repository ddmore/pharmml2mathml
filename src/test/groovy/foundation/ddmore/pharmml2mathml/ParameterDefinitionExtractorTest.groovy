/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import foundation.ddmore.pharmml08.dom.maths.BinaryOperator
import foundation.ddmore.pharmml08.dom.maths.UnaryOperator
import foundation.ddmore.pharmml08.dom.modeldefn.TransformationType
import foundation.ddmore.pharmml08.dom.probonto.DistroNameType
import foundation.ddmore.pharmml08.dom.probonto.DistroType
import foundation.ddmore.pharmml08.dom.probonto.ParameterNameType
import org.junit.Test

import static foundation.ddmore.pharmml08.builder.MathsBuilder.*
import static foundation.ddmore.pharmml08.builder.ParameterModelBuilder.*
import static foundation.ddmore.pharmml08.builder.ProbontoBuilder.createDistnParameter
import static foundation.ddmore.pharmml08.builder.ProbontoBuilder.createProbonto
import static org.junit.Assert.assertEquals

/**
 * Created by stumoodie on 05/01/2017.
 */
class ParameterDefinitionExtractorTest {

    @Test
    void testSimpleParameterNoRhs(){
        def root = createSimpleParam("pop")

        def actual = ParameterDefinitionExtractor.extractParameterDefn(root)
        def expected = "<mrow><mi>pop</mi></mrow>"
        assertEquals(expected, actual)
    }

    @Test
    void testSimpleParameterRhs(){
        def root = createSimpleParam(
                "pop",
                createRhs(
                        createReal(0.0)
                )
        )

        def actual = ParameterDefinitionExtractor.extractParameterDefn(root)
        def expected = "<mrow><mi>pop</mi><mo>=</mo><mn>0.0</mn></mrow>"
        assertEquals(expected, actual)
    }

    @Test
    void testPopulationParameterNoRhs(){
        def root = createPopulationParam("pop")

        def actual = ParameterDefinitionExtractor.extractParameterDefn(root)
        def expected = "<mrow><mi>pop</mi></mrow>"
        assertEquals(expected, actual)
    }

    @Test
    void testPopulationParameterRhs(){
        def root = createPopulationParam(
                "pop",
                createRhs(
                        createReal(0.0)
                )
        )

        def actual = ParameterDefinitionExtractor.extractParameterDefn(root)
        def expected = "<mrow><mi>pop</mi><mo>=</mo><mn>0.0</mn></mrow>"
        assertEquals(expected, actual)
    }

    @Test
    void testPopulationParameterComplexRhs(){
        def root = createPopulationParam(
                "Rin",
                createRhs(
                        createBinop(
                                BinaryOperator.TIMES,
                                createSymbRef("pop_Rin"),
                                createUniop(
                                        UnaryOperator.EXP,
                                        createSymbRef("eta_Rin")
                                )
                        )
                )
        )

        def actual = ParameterDefinitionExtractor.extractParameterDefn(root)
        def expected = "<mrow><mi>Rin</mi><mo>=</mo><mi>pop_Rin</mi><mo>&#x022C5;</mo><msup><mrow><mi>&ExponentialE;</mi></mrow><mrow><mi>eta_Rin</mi></mrow></msup></mrow>"
        assertEquals(expected, actual)
    }

    @Test
    void testPopulationParameterDistribution(){
        def root = createPopulationParam(
                "pop",
                createDistribution(
                        createProbonto(
                                DistroNameType.WISHART_1,
                                DistroType.UNIVARIATE,
                                createDistnParameter(
                                        ParameterNameType.DEGREES_OF_FREEDOM,
                                        createRhs(
                                                createInt(3)
                                        )
                                )
                        )
                ),
                createVariabilityRef("vm", "prior")
        )

        def actual = ParameterDefinitionExtractor.extractParameterDefn(root)
        def expected = "<mrow><msub><mi>pop</mi><mrow><mfenced open=\"\" close=\"\"><mrow><mi>vm.prior</mi></mrow></mfenced></mrow></msub><mo>~</mo><mrow><mi>Wishart1</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>degreesoffreedom</mi><mo>=</mo><mn>3</mn></mrow></mfenced></mrow></mrow>"
        assertEquals(expected, actual)
    }

    @Test
    void testRandomVariable(){
        def root = createRandomVariable(
                "rv",
                createDistribution(
                        createProbonto(
                                DistroNameType.WISHART_1,
                                DistroType.UNIVARIATE,
                                createDistnParameter(
                                        ParameterNameType.DEGREES_OF_FREEDOM,
                                        createRhs(
                                                createInt(3)
                                        )
                                )
                        )
                ),
                [ createVariabilityRef("vm", "prior") ]
        )

        def actual = ParameterDefinitionExtractor.extractParameterDefn(root)
        def expected = "<mrow><msub><mi>rv</mi><mrow><mfenced open=\"\" close=\"\"><mrow><mi>vm.prior</mi></mrow></mfenced></mrow></msub><mo>~</mo><mrow><mi>Wishart1</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>degreesoffreedom</mi><mo>=</mo><mn>3</mn></mrow></mfenced></mrow></mrow>"
        assertEquals(expected, actual)
    }

    @Test
    void testRandomVariableWithTransform(){
        def root = createRandomVariable(
                "rv",
                createDistribution(
                        createProbonto(
                                DistroNameType.WISHART_1,
                                DistroType.UNIVARIATE,
                                createDistnParameter(
                                        ParameterNameType.DEGREES_OF_FREEDOM,
                                        createRhs(
                                                createInt(3)
                                        )
                                )
                        )
                ),
                [ createVariabilityRef("vm", "prior") ],
                createRVTransform(TransformationType.LOG)
        )

        def actual = ParameterDefinitionExtractor.extractParameterDefn(root)
        def expected = "<mrow><mi>ln</mi><mo>&ApplyFunction;</mo><mfenced><mrow><msub><mi>rv</mi><mrow><mfenced open=\"\" close=\"\"><mrow><mi>vm.prior</mi></mrow></mfenced></mrow></msub></mrow></mfenced><mo>~</mo><mrow><mi>Wishart1</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>degreesoffreedom</mi><mo>=</mo><mn>3</mn></mrow></mfenced></mrow></mrow>"
        assertEquals(expected, actual)
    }

    @Test
    void testRandomVariableMultiVarLevels(){
        def root = createRandomVariable(
                "rv",
                createDistribution(
                        createProbonto(
                                DistroNameType.NORMAL_1,
                                DistroType.UNIVARIATE,
                                createDistnParameter(
                                        ParameterNameType.MEAN,
                                        createRhs(
                                                createSymbRef("pop")
                                        )
                                )
                        )
                ),
                [createVariabilityRef("vm", "p"),
                createVariabilityRef("vm", "q")]
        )

        def actual = ParameterDefinitionExtractor.extractParameterDefn(root)
        def expected = "<mrow><msub><mi>rv</mi><mrow><mfenced open=\"\" close=\"\"><mrow><mi>vm.p</mi></mrow><mrow><mi>vm.q</mi></mrow></mfenced></mrow></msub><mo>~</mo><mrow><mi>Normal1</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>mean</mi><mo>=</mo><mi>pop</mi></mrow></mfenced></mrow></mrow>"
        assertEquals(expected, actual)
    }

    @Test
    void testIndividualParameterExplicity(){
        def root = createIndividualParameter(
                "idv",
                createRhs(createReal(2.2))
        )

        def actual = ParameterDefinitionExtractor.extractParameterDefn(root)
        def expected = "<mrow><mi>idv</mi><mo>=</mo><mn>2.2</mn></mrow>"
        assertEquals(expected, actual)
    }

    @Test
    void testIndividualParameterStandardLinearNoCovariate(){
        def root = createStructuredIndividualParameter(
                "idv",
                createStructuredModel(
                        createLinearCovariate(
                                createRhs(
                                createSymbRef("pop")
                                ),
                                Collections.emptyList()
                        ),
                        [ createSymbRefType("rv1")]
                )
        )

        def actual = ParameterDefinitionExtractor.extractParameterDefn(root)
        def expected = "<mrow><mi>idv</mi><mo>=</mo><mi>pop</mi><mo>+</mo><mi>rv1</mi></mrow>"
        assertEquals(expected, actual)
    }

    @Test
    void testIndividualParameterStandardLinear(){
        def root = createStructuredIndividualParameter(
                "idv",
                createStructuredModel(
                        createLinearCovariate(
                                createRhs(
                                        createSymbRef("pop")
                                ),
                                [createCovariateRelation(
                                        createSymbRefType("cm", "C1"),
                                        createFixedEffectRelationParamRef(
                                                createSymbRefType("beta")
                                        )
                                )]
                        ),
                        [ createSymbRefType("rv1")]
                )
        )

        def actual = ParameterDefinitionExtractor.extractParameterDefn(root)
        def expected = "<mrow><mi>idv</mi><mo>=</mo><mi>pop</mi><mo>+</mo><mi>cm.C1</mi><mo>&#x022C5;</mo><mi>beta</mi><mo>+</mo><mi>rv1</mi></mrow>"
        assertEquals(expected, actual)
    }

    @Test
    void testIndividualParameterStandardLinearWithTransform(){
        def root = createStructuredIndividualParameter(
                "idv",
                createStructuredModel(
                        createLinearCovariate(
                                createRhs(
                                        createSymbRef("pop")
                                ),
                                [createCovariateRelation(
                                        createSymbRefType("cm", "C1"),
                                        createFixedEffectRelationParamRef(
                                                createSymbRefType("beta")
                                        )
                                )]
                        ),
                        [ createSymbRefType("rv1")],
                        createParameterTransform(TransformationType.LOG)
                )
        )

        def actual = ParameterDefinitionExtractor.extractParameterDefn(root)
        def expected = "<mrow><mi>ln</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>idv</mi></mrow></mfenced><mo>=</mo><mi>ln</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>pop</mi></mrow></mfenced><mo>+</mo><mi>cm.C1</mi><mo>&#x022C5;</mo><mi>beta</mi><mo>+</mo><mi>rv1</mi></mrow>"
        assertEquals(expected, actual)
    }

    @Test
    void testIndividualParameterStandardLinear2Rvs(){
        def root = createStructuredIndividualParameter(
                "idv",
                createStructuredModel(
                        createLinearCovariate(
                                createRhs(
                                        createSymbRef("pop")
                                ),
                                [
                                        createCovariateRelation(
                                            createSymbRefType("cm", "C1"),
                                            createFixedEffectRelationParamRef(
                                                    createSymbRefType("beta")
                                            )
                                        ),
                                        createCovariateRelation(
                                                createSymbRefType("cm", "C2"),
                                                createFixedEffectRelationScalar(
                                                        createReal(2.9)
                                                )
                                        )
                                ]
                        ),
                        [
                                createSymbRefType("rv1"),
                                createSymbRefType("rv2")
                        ]
                )
        )

        def actual = ParameterDefinitionExtractor.extractParameterDefn(root)
        def expected = "<mrow><mi>idv</mi><mo>=</mo><mi>pop</mi><mo>+</mo><mi>cm.C1</mi><mo>&#x022C5;</mo><mi>beta</mi><mo>+</mo><mi>cm.C2</mi><mo>&#x022C5;</mo><mn>2.9</mn><mo>+</mo><mi>rv1</mi><mo>+</mo><mi>rv2</mi></mrow>"
        assertEquals(expected, actual)
    }

    @Test
    void testIndividualParameterStandardLinearCategoricalCovs(){
        def root = createStructuredIndividualParameter(
                "idv",
                createStructuredModel(
                        createLinearCovariate(
                                createRhs(
                                        createSymbRef("pop")
                                ),
                                [
                                        createCovariateRelation(
                                                createSymbRefType("cm", "C1"),
                                                createFixedEffectRelationCategorical(
                                                        createSymbRef("beta"),
                                                        "male")
                                        )
                                ]
                        ),
                        [
                                createSymbRefType("rv1")
                        ]
                )
        )

        def actual = ParameterDefinitionExtractor.extractParameterDefn(root)
        // idv = pop + beta . 1_cm.C1=male + rv1
        def expected = "<mrow><mi>idv</mi><mo>=</mo><mi>pop</mi><mo>+</mo><mi>beta</mi><mo>&#x022C5;</mo><msub><mn>1</mn><mrow><mi>cm.C1</mi><mo>=</mo><mi>male</mi></mrow></msub><mo>+</mo><mi>rv1</mi></mrow>"
        assertEquals(expected, actual)
    }

    @Test
    void testIndividualParameterStandardGeneral(){
        def root = createStructuredIndividualParameter(
                "idv",
                createStructuredModel(
                        createGeneralCovariate(
                                createRhs(
                                        createBinop(
                                                BinaryOperator.DIVIDE,
                                                createSymbRef("cm", "C1"),
                                                createSymbRef("pop")
                                        )
                                )
                        ),
                        [ createSymbRefType("rv1")]
                )
        )

        def actual = ParameterDefinitionExtractor.extractParameterDefn(root)
        def expected = "<mrow><mi>idv</mi><mo>=</mo><mfrac><mrow><mi>cm.C1</mi></mrow><mrow><mi>pop</mi></mrow></mfrac><mo>+</mo><mi>rv1</mi></mrow>"
        assertEquals(expected, actual)
    }

    @Test
    void testIndividualParameterStandardGeneralWithTransform(){
        def root = createStructuredIndividualParameter(
                "idv",
                createStructuredModel(
                        createGeneralCovariate(
                                createRhs(
                                        createBinop(
                                                BinaryOperator.DIVIDE,
                                                createSymbRef("cm", "C1"),
                                                createSymbRef("pop")
                                        )
                                )
                        ),
                        [ createSymbRefType("rv1")],
                        createParameterTransform(TransformationType.LOGIT)
                )
        )

        def actual = ParameterDefinitionExtractor.extractParameterDefn(root)
        def expected = "<mrow><mi>logit</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>idv</mi></mrow></mfenced><mo>=</mo><mfrac><mrow><mi>cm.C1</mi></mrow><mrow><mi>pop</mi></mrow></mfrac><mo>+</mo><mi>rv1</mi></mrow>"
        assertEquals(expected, actual)
    }

    @Test
    void testIndividualParameterStandardGeneral2Rvs(){
        def root = createStructuredIndividualParameter(
                "idv",
                createStructuredModel(
                        createGeneralCovariate(
                                createRhs(
                                        createBinop(
                                                BinaryOperator.DIVIDE,
                                                createSymbRef("pop"),
                                                createReal(2.9)
                                        )
                                )
                        ),
                        [
                                createSymbRefType("rv1"),
                                createSymbRefType("rv2")
                        ]
                )
        )

        def actual = ParameterDefinitionExtractor.extractParameterDefn(root)
        def expected = "<mrow><mi>idv</mi><mo>=</mo><mfrac><mrow><mi>pop</mi></mrow><mrow><mn>2.9</mn></mrow></mfrac><mo>+</mo><mi>rv1</mi><mo>+</mo><mi>rv2</mi></mrow>"
        assertEquals(expected, actual)
    }

    @Test
    void testRvCorrelation(){

        def root = createRvCorrelationCorr(
                createRhs(createSymbRef("p1", "corrVar")),
                createSymbRefType("rv1"),
                createSymbRefType("rv2")
        )
        def actual = ParameterDefinitionExtractor.extractParameterDefn(root)
        def expected = "<mrow><mi>corr</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>rv1</mi></mrow><mrow><mi>rv2</mi></mrow></mfenced><mo>=</mo><mi>p1.corrVar</mi></mrow>"
        assertEquals(expected, actual)
    }

    @Test
    void testRvCovariance(){

        def root = createRvCorrelationCov(
                createRhs(createSymbRef("p1", "covVar")),
                createSymbRefType("rv1"),
                createSymbRefType("rv2")
        )
        def actual = ParameterDefinitionExtractor.extractParameterDefn(root)
        def expected = "<mrow><mi>cov</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>rv1</mi></mrow><mrow><mi>rv2</mi></mrow></mfenced><mo>=</mo><mi>p1.covVar</mi></mrow>"
        assertEquals(expected, actual)
    }

}
