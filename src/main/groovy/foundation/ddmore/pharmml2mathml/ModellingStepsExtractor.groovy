/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import foundation.ddmore.pharmml08.dom.IndependentVariableType
import foundation.ddmore.pharmml08.dom.PharmML
import foundation.ddmore.pharmml08.dom.commontypes.FunctionDefinitionType
import foundation.ddmore.pharmml08.dom.commontypes.OidRefType
import foundation.ddmore.pharmml08.dom.commontypes.SymbolRefType
import foundation.ddmore.pharmml08.dom.commontypes.VariableAssignmentType
import foundation.ddmore.pharmml08.dom.modellingsteps.*

/**
 * Created by stumoodie on 05/01/2017.
 */
class ModellingStepsExtractor {

    ModellingStepsExtractor(){
    }

    static extractPharmML(ModellingStepsType node){
        def walker = new ModellingStepsExtractor()
        walker.extract(node)
    }

    def extract(ModellingStepsType node){
        visit(node)
    }

    def visit(ModellingStepsType node){
        def retVal = [
            simulationSteps : [],
            estimationSteps : [],
            optimalDesignSteps : [],
            stepDependencies : []
        ]

        node.commonModellingStep.each {cms->
            def step = visit(cms.value)
            switch(step.type){
                case "estimation":
                    retVal.estimationSteps.add(step)
                    break;
                case "simulation":
                    retVal.simulationSteps.add(step)
                    break;
                case "optimalDesign":
                    retVal.optimalDesignSteps.add(step)
                    break;
            }
        }

        retVal.stepDependencies = visit(node.stepDependencies)

        retVal
    }


    def visit(EstimationStepType node){
        def retVal = [
                parametersToEstimate : [],
                operations : [],
                datasetRef : null,
                type : "estimation"
        ]

        retVal.oid = node.getOid()
        retVal.description = node.getDescription()?.value
        retVal.extDatasetRef = node.externalDataSetReference?.oidRef?.oidRef

        node.getParametersToEstimate().each {te->
            te.getParameterEstimation().each {pe->
                retVal.parametersToEstimate.add(visit(pe))
            }
        }

        node.getOperation().each {op->
            retVal.operations.add(visit(op))
        }

        retVal
    }


    def visit(EstimationOperationType node){
        def retVal = [ properties : [] ]

        retVal.name = node.name?.value
        retVal.order = node.order
        retVal.opType = node.opType
        if(node.algorithm)
            retVal.algorithm  = visit(node.algorithm)

        node.getProperty().each {p->
            retVal.properties.add(visit(p))
        }

        retVal
    }


    def visit(AlgorithmType node){
        def retVal = [ properties : [] ]

        retVal.name = node.name?.value
        retVal.defn = node.definition
        node.getProperty().each {p->
            retVal.properties.add(visit(p))
        }

        retVal
    }

    def visit(OperationPropertyType node){
        def retVal = [:]

        retVal.name = node.name
        retVal.value = MathMlExtractor.extract(node.assign, false)

        retVal
    }


    def visit(ParameterEstimateType node){
        def retVal = [
                initialValue : [ value : null, fixed : false ],
                lower : null,
                upper :null
        ]

        retVal.symbRef = visit(node.symbRef)
        if(node.initialEstimate)
            retVal.initialValue = visit(node.initialEstimate)

        if(node.lowerBound)
            retVal.lower = MathMlExtractor.extract(node.lowerBound, false)

        if(node.upperBound)
            retVal.upper = MathMlExtractor.extract(node.upperBound, false)

        retVal
    }


    def visit(SymbolRefType node){
        node.blkIdRef + "." + node.symbIdRef
    }

    def visit(InitialEstimateType node){
        def retVal = [
                fixed : false,
                value : null
        ]

        retVal.value = MathMlExtractor.extract(node.expression, false)
        if(node.fixed)
            retVal.fixed = node.fixed.booleanValue()

        retVal
    }

    def visit(SimulationStepType node){
        def retVal = [
                type : "simulation",
                variableAssignments : [],
                operations : [],
                datasetRef : null,
        ]

        retVal.oid = node.getOid()
        retVal.description = node.getDescription()?.value
        retVal.extDatasetRef = node.externalDataSetReference?.oidRef?.oidRef

        node.getVariableAssignment().each {va->
            retVal.variableAssignments.add(visit(va))
        }

        node.operation.each {op->
            retVal.operations.add(visit(op))
        }

        retVal
    }

    def visit(OptimalDesignStepType node){
        [ type : "optimalDesign" ]
    }

    def visit(OidRefType node){
        node.oidRef
    }

    def visit(SimulationOperationType node){
        def retVal = [ properties : [] ]

        retVal.name = node.name?.value
        retVal.order = node.order
        retVal.opType = node.opType
        if(node.algorithm)
            retVal.algorithm  = visit(node.algorithm)

        node.getProperty().each {p->
            retVal.properties.add(visit(p))
        }

        retVal
    }


    def visit(StepDependencyType node){
        def retVal = [ ]

        node.getStep().each {st->
            def depStep = [ depOids : [] ]
            depStep.oidRef = visit(st.oidRef)
            st.dependents.each {dep->
                depStep.depOids.addAll(visit(dep))
            }
            retVal.add(depStep)
        }

        retVal
    }

    def visit(DependentsType node){
        def retVal = []
        node.oidRef.each {ref->
            retVal.add(visit(ref))
        }

        retVal
    }

    def visit(VariableAssignmentType node){
        def retVal = [:]

        retVal.symbRef = visit(node.symbRef)
        retVal.maths = MathMlExtractor.extract(node.assign, false)

        retVal
    }



 }
