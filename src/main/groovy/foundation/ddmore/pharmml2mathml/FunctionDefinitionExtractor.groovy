/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import foundation.ddmore.pharmml08.dom.commontypes.*

/**
 * Created by stumoodie on 05/01/2017.
 */
class FunctionDefinitionExtractor extends MathMlExtractor {


    FunctionDefinitionExtractor() {
        super()
    }

    static String extractFuncDefn(FunctionDefinitionType node) {
        def walker = new FunctionDefinitionExtractor()
        walker.extract(node)
    }

    String extract(FunctionDefinitionType node) {
        def covDefn = builder.bind {
            visitClosure(visit(node, node), delegate)
        }

        covDefn.toString()
    }

    def visit(FunctionDefinitionType node, parent) {
        return {
            mrow{
                mrow {
                    mi node.symbId
                    mo(':')
                    mi(node.symbolType.value)
                }
                mo{
                    mkp.yieldUnescaped APPLY_FUNCTION
                }
                mfenced{
                    node.getFunctionArgument().each {fa->
                        mrow {
                            mi fa.symbId
                            mo(':')
                            mi(fa.symbolType.value)
                        }
                    }
                }
                mo('=')
                visitClosure(visit(node.definition, node), delegate)
            }
        }
    }

    def visit(StandardAssignType node, parent){
        return {
            visitClosure(visit(node.assign, node), delegate)
        }
    }

}
