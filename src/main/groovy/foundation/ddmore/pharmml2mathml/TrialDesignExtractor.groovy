/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import foundation.ddmore.pharmml08.dom.dataset.CategoryMappingType
import foundation.ddmore.pharmml08.dom.dataset.ColumnDefnType
import foundation.ddmore.pharmml08.dom.dataset.ColumnMappingType
import foundation.ddmore.pharmml08.dom.dataset.ColumnRefType
import foundation.ddmore.pharmml08.dom.dataset.ColumnTypeType
import foundation.ddmore.pharmml08.dom.dataset.DataSetType
import foundation.ddmore.pharmml08.dom.dataset.ExternalFileType
import foundation.ddmore.pharmml08.dom.dataset.HeaderColumnsDefinitionType
import foundation.ddmore.pharmml08.dom.dataset.MapType
import foundation.ddmore.pharmml08.dom.dataset.TargetMappingType
import foundation.ddmore.pharmml08.dom.trialdesign.ExternalDataSetType
import foundation.ddmore.pharmml08.dom.trialdesign.MultipleDVMappingType
import foundation.ddmore.pharmml08.dom.trialdesign.TrialDesignType

/**
 * Created by stumoodie on 12/04/2017.
 */
class TrialDesignExtractor {

    def static extractTrialDesign(TrialDesignType node){
        def walker = new TrialDesignExtractor()
        walker.visit(node)
    }


    def visit(TrialDesignType node){
        def retVal = [
            extnlDatasets : []
        ]

        node.getExternalDataSet().each {eds->
            retVal.extnlDatasets.add(visit(eds))
        }

        retVal
    }


    def visit(ExternalDataSetType node){
        def retVal = [
                columnMappings : []
        ]

        retVal.oid = node.oid
        retVal.toolname = node.toolName

        retVal.dataset = visit(node.dataSet)
        node.columnMappingOrColumnTransformationOrMultipleDVMapping.each {cm->
            retVal.columnMappings.add(visit(cm))
        }

        retVal
    }


    def visit(ColumnMappingType node){
        def retVal = [
                targetMappings : [],
                catMappings : []
        ]

        retVal.colRef = visit(node.columnRef)
        if(node.symbRef)
            retVal.modelMapping = MathMlExtractor.extract(node.symbRef, false)
        else if(node.piecewise)
            retVal.modelMapping = MathMlExtractor.extract(node.piecewise, false)
        else if(node.assign)
            retVal.modelMapping = MathMlExtractor.extract(node.assign, false)
        else if(node.targetMapping){
            retVal.modelMapping = mathMLForTargetMappings(node.columnRef, node.targetMapping)
        }

//        node.targetMapping.each {tm->
//            retVal.targetMappings.add(visit(tm))
//        }


        node.categoryMapping.each {cm->
            retVal.catMappings.addAll(visit(cm))
        }

        retVal
    }

    def mathMLForTargetMappings(ColumnRefType colRef, List<TargetMappingType> node){
        def retVal = ""
        node.each {m->
            retVal += mathMLForTargetMapping(colRef.columnIdRef, m)
        }

        if(node.size() > 0){
            if(node.head().map.size() > 1){
                retVal = "<mrow><mo>{</mo><mtable columnalign=\"left\">" + retVal
                retVal += "</mtable></mrow>"
            }
        }

        retVal
    }

    def mathMLForTargetMapping(String colRef, TargetMappingType node){
        def retVal = ""
        for(def mapping : node.map){
            if(mapping.dataSymbol){
                retVal += getPieceForTarget(colRef, node, mapping)
            }
            else{
                retVal += getAdmMapping(node, mapping)
            }
        }

        retVal
    }

    def getAdmMapping(TargetMappingType tmt, MapType mt){
        "<mrow><mtext>blk:</mtext><mspace width=\"1mm\"/><mi>${tmt.blkIdRef}</mi><mo>,</mo><mspace width=\"2mm\"/><mtext>adm:</mtext><mspace width=\"1mm\"/><mn>${mt.admNumber}</mn></mrow>"
    }

    def getPieceForTarget(String colRef, TargetMappingType tmt, MapType mt){
        "<mtr><mtd>${getAdmMapping(tmt, mt)}</mtd><mtd><mtext>if</mtext></mtd><mtd><mrow><mi>${colRef}</mi><mo>=</mo><mtext>${mt.dataSymbol}</mtext></mrow></mtd></mtr>"
    }

//    def visit(TargetMappingType node){
//        def retVal = [ mappings : [] ]
//
//        retVal.blkIdRef = node.blkIdRef
//        node.map.each {m->
//            def mapping = [:]
//            mapping.dataSymbol = m.dataSymbol
//            mapping.adm = m.admNumber.toString()
//            retVal.mappings.add(mapping)
//        }
//
//        retVal
//    }


    def visit(CategoryMappingType node){
        def retVal = [ ]

        node.map.each {m->
            def cat = [:]
            if(m.dataSymbol.isNumber()){
                cat.dataCat = "<mn>${m.dataSymbol}</mn>".toString()
            }
            else{
                cat.dataCat = "<mi>${m.dataSymbol}</mi>".toString()
            }
            cat.modelCat = "<mi>${m.modelSymbol}</mi>".toString()
            retVal.add(cat)
        }

        retVal
    }


    def visit(MultipleDVMappingType node){
        def retVal = [:]

        retVal.colRef = visit(node.columnRef)
        retVal.modelMapping =  MathMlExtractor.extract(node.piecewise, false)

        retVal
    }


    def visit(ColumnRefType colRef){
        colRef.columnIdRef
    }


    def visit(DataSetType node){
        def retVal = [:]

        retVal.fileSpec = visit(node.externalFile)
        retVal.definitions = visit(node.definition)

        retVal
    }

    def visit(ExternalFileType node){
        def retVal = [:]

        retVal.delimiter = node.delimiter?.toLowerCase()
        retVal.format = node.format?.toLowerCase()
        retVal.path = node.path

        retVal
    }

    def visit(HeaderColumnsDefinitionType node){
        def retVal = []

        node.column.each {col->
            retVal.add(visit(col))
        }

        retVal
    }


    def visit(ColumnDefnType node) {
        def retVal = [:]

        retVal.colId = node.columnId
        retVal.colNum = node.columnNum
        if (node.columnType != null && node.columnType.size() > 0){
            retVal.colType = node.columnType.head().value().toLowerCase() ?: ColumnTypeType.UNDEFINED.value().toLowerCase()
        }
        else {
            retVal.colType = ColumnTypeType.UNDEFINED.value().toLowerCase()
        }
        retVal.valType = node.valueType?.value()?.toLowerCase()

        retVal
    }

}
