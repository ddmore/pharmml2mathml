/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import foundation.ddmore.pharmml08.dom.modeldefn.*

/**
 * Created by stumoodie on 05/01/2017.
 */
class CovariateModelExtractor {

    CovariateModelExtractor(){
    }

    static extractCovariateModels(ModelDefinitionType node){
        def walker = new CovariateModelExtractor()
        walker.extractAllDefns(node)
    }

    def extractAllDefns(ModelDefinitionType node){
        visit(node)
    }

    def visit(ModelDefinitionType node){
        def retVal = [ covariateModels : [] ]
        for(def cm : node.getCovariateModel()){
            retVal.covariateModels.add(visit(cm))
        }
        retVal
    }


//    def visitChildren(TreeNode node) {
//        def retVal = []
//        for (child in node.children()) {
//            def nodeRes = visit(child)
//            if(nodeRes){
//                retVal.add(nodeRes)
//            }
//        }
//        retVal
//    }
//
//    def visit(Object node) {
//        visitChildren(node)
//    }


    def visit(CovariateModelType node){
        def retVal = [
                covariates : [],
                parameters : []
        ]
//        def results = visitChildren(node).flatten()
        def results = []
        node.getParameter().each {p->
            results.add(visit(p))
        }
        node.getPopulationParameter().each {p->
            results.add(visit(p))
        }
        node.getCovariate().each {c->
            results.add(visit(c))
        }
        for(def res : results.flatten()){
            if(res.type == 'continuous'){
                retVal.covariates.add(res)
            }
            else if(res.type == 'categorical'){
                retVal.covariates.add(res)
            }
            else if(res.type == 'popParam'){
                retVal.parameters.add(res)
            }
            else if(res.type == 'simpleParam'){
                retVal.parameters.add(res)
            }
        }
        retVal.blkId = node.blkId
        retVal
    }


    def visit(SimpleParameterType node){
        def retVal = [:]
        retVal.symbId = node.symbId
        retVal.maths = ParameterDefinitionExtractor.extractParameterDefn(node)
        retVal.type = 'simpleParam'
        retVal
    }

    def visit(PopulationParameterType node){
        def retVal = [:]
        retVal.symbId = node.symbId
        retVal.maths = ParameterDefinitionExtractor.extractParameterDefn(node)
        retVal.type = 'popParam'
        retVal
    }

    def visit(CovariateDefinitionType node){
        def retVal = [  ]
        if(node.categorical){
            def catMap = [:]
            catMap.symbId = node.symbId
            catMap.varType = node.type
            catMap.type = 'categorical'
            catMap.categories = visit(node.categorical)
            retVal.add(catMap)
        }
        else if(node.continuous){
            retVal.add([
                    symbId : node.symbId,
                    type : 'continuous',
                    varType : node.type,
                    maths : CovariateDefinitionExtractor.extractCovarDefn(node)
            ])
            node.continuous.getTransformation().each { t ->
                retVal.add([
                        symbId : t.transformedCovariate.symbId,
                        type : 'continuous',
                        varType : node.type,
                        maths : CovariateTransformationExtractor.extractCovariateTrans(node)
                ])
            }
        }
        retVal
    }

    def visit(CategoricalCovariateType node){
        def retVal = []
        node.getCategory().each {c->
            retVal.add(visit(c))
        }
        retVal
    }

    def visit(CategoryType node){
        node.name.value
    }
}
