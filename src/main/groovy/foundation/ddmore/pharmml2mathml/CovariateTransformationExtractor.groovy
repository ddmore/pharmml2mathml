/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import foundation.ddmore.pharmml08.dom.modeldefn.ContinuousCovariateType
import foundation.ddmore.pharmml08.dom.modeldefn.CovariateDefinitionType
import foundation.ddmore.pharmml08.dom.modeldefn.CovariateTransformationType

/**
 * Created by stumoodie on 05/01/2017.
 */
class CovariateTransformationExtractor extends MathMlExtractor {
    CovariateTransformationExtractor(){
        super()
    }

    static String extractCovariateTrans(CovariateDefinitionType node){
        def walker = new CovariateTransformationExtractor()
        def retVal = walker.extractList(node)
        retVal
    }

    String extractList(CovariateDefinitionType node){
        def covDefn = builder.bind {
            visitClosure(visit(node, node), delegate)
        }
        covDefn.toString()
    }


    def visit(CovariateDefinitionType node, parent){
        return {
            if(node.continuous){
                visitClosure(visit(node.continuous, node), delegate)
            }
        }
    }


    def visit(ContinuousCovariateType node, parent){
        return {
            node.getTransformation().each { t->
                visitClosure(visit(t, node), delegate)
            }
        }
    }

    def visit(CovariateTransformationType node, parent){
        return {
            mrow {
                mi(node.transformedCovariate.symbId)
                if (node.assign) {
                    mo("=")
                    visitClosure(visit(node.assign, node), delegate)
                }
            }
        }
    }

}
