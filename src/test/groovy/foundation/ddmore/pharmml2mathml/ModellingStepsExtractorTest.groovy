/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import foundation.ddmore.pharmml08.dom.maths.BinaryOperator
import foundation.ddmore.pharmml08.dom.modeldefn.AbsorptionOralMacroArgumentType
import foundation.ddmore.pharmml08.dom.modellingsteps.EstimationStepType
import org.junit.Test

import static foundation.ddmore.pharmml08.builder.MathsBuilder.*
import static foundation.ddmore.pharmml08.builder.ModellingStepsBuilder.*
import static foundation.ddmore.pharmml08.builder.PharmMlBuilder.createModelDefinition
import static foundation.ddmore.pharmml08.builder.StructuralModelBuilder.*
import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertNull

/**
 * Created by stumoodie on 05/01/2017.
 */
class ModellingStepsExtractorTest {

    @Test
    void testPopulatedEstimationStep(){
        def root = createModellingSteps(
                createStepDependencies(),
                createEstimationStep(
                        "testoid",
                        "extDataSetOid",
                        [
                                createParameterEstimate(
                                        createSymbRefType("p1", "abc"),
                                        createReal(22.0),
                                        false,
                                        createRhs(createReal(0.0)),
                                        createRhs(createReal(30.5))
                                )
                        ],
                        [
                                createOperation(
                                        "op",
                                        1,
                                        "Aname",
                                        createAlgorithm(
                                                "AnAlgo",
                                                [
                                                        createProperty("algProp", createRhs(createString("algValue")))
                                                ]
                                        ),
                                        [
                                                createProperty("opProp", createRhs(createString("opValue")))
                                        ]
                                )
                        ]
                )
        )

        def actual = ModellingStepsExtractor.extractPharmML(root)

        assertEquals(1, actual.estimationSteps.size())
        assertEquals(0, actual.simulationSteps.size())
        assertEquals(0, actual.optimalDesignSteps.size())
        def estStep = actual.estimationSteps[0]
        assertEquals("testoid", estStep.oid)
        assertEquals("estimation", estStep.type)
        assertEquals("extDataSetOid", estStep.extDatasetRef)
        assertEquals(1, estStep.parametersToEstimate.size())

        def paramToEst = estStep.parametersToEstimate[0]
        assertEquals("p1.abc", paramToEst.symbRef)
        assertEquals(false, paramToEst.initialValue.fixed)
        assertEquals("<mn>22.0</mn>", paramToEst.initialValue.value)
        assertEquals("<mn>0.0</mn>", paramToEst.lower)
        assertEquals("<mn>30.5</mn>", paramToEst.upper)

        assertEquals(1, estStep.operations.size())
        def operation = estStep.operations[0]
        assertEquals("AnAlgo", operation.algorithm.name)
        assertEquals("Aname", operation.name)
        assertEquals(1, operation.order)
        assertNull(operation.description)
        assertEquals(1, operation.algorithm.properties.size())
        assertEquals("algProp", operation.algorithm.properties[0].name)
        assertEquals("<mtext>algValue</mtext>", operation.algorithm.properties[0].value)

        assertEquals(1, operation.properties.size())
        assertEquals("opProp", operation.properties[0].name)
        assertEquals("<mtext>opValue</mtext>", operation.properties[0].value)
    }

    @Test
    void testMinimalEstimationStep(){
        def root = createModellingSteps(
                createStepDependencies(),
                createEstimationStep(
                        "testoid",
                        "extDataSetOid",
                        [
                                createParameterEstimate(
                                        createSymbRefType("p1", "abc"),
                                        createReal(22.0)
                                )
                        ],
                        [
                                createOperation(
                                        "op",
                                        1,
                                        "Aname",
                                        []
                                )
                        ]
                )
        )

        def actual = ModellingStepsExtractor.extractPharmML(root)

        assertEquals(1, actual.estimationSteps.size())
        assertEquals(0, actual.simulationSteps.size())
        assertEquals(0, actual.optimalDesignSteps.size())
        def estStep = actual.estimationSteps[0]
        assertEquals("testoid", estStep.oid)
        assertNull(estStep.name)
        assertNull(estStep.description)
        assertEquals("estimation", estStep.type)
        assertEquals("extDataSetOid", estStep.extDatasetRef)
        assertEquals(1, estStep.parametersToEstimate.size())

        def paramToEst = estStep.parametersToEstimate[0]
        assertEquals("p1.abc", paramToEst.symbRef)
        assertEquals(false, paramToEst.initialValue.fixed)
        assertEquals("<mn>22.0</mn>", paramToEst.initialValue.value)
        assertNull(paramToEst.lower)
        assertNull(paramToEst.upper)

        assertEquals(1, estStep.operations.size())
        def operation = estStep.operations[0]
        assertEquals("Aname", operation.name)
        assertNull(operation.description)
        assertNull(operation.algorithm)
        assertEquals(0, operation.properties.size())
    }

    @Test
    void testStepDependencies(){
        def root = createModellingSteps(
                createStepDependencies(
                        createStep("testOid1", "testOid2", "testOid3")
                ),
                createEstimationStep(
                        "testoid1",
                        "extDataSetOid",
                        [
                                createParameterEstimate(
                                        createSymbRefType("p1", "abc"),
                                        createReal(22.0),
                                        false,
                                        createRhs(createReal(0.0)),
                                        createRhs(createReal(30.5))
                                )
                        ],
                        [
                                createEstOperation(
                                        "op",
                                        1,
                                        "Aname",
                                        createAlgorithm(
                                                "AnAlgo",
                                                [
                                                        createProperty("algProp", createRhs(createString("algValue")))
                                                ]
                                        ),
                                        [
                                                createProperty("opProp", createRhs(createString("opValue")))
                                        ]
                                )
                        ]
                )
        )

        def actual = ModellingStepsExtractor.extractPharmML(root)

        assertEquals(1, actual.stepDependencies.size())

        def firstDep = actual.stepDependencies.head()
        assertEquals("testOid1", firstDep.oidRef)
        def expectedDeps = [ "testOid2", "testOid3" ]
        for(int i; i < expectedDeps.size(); i++){
            assertEquals(expectedDeps[i], actual.stepDependencies[0].depOids[i])
        }
    }

    @Test
    void testMultiStepDependencies(){
        def root = createModellingSteps(
                createStepDependencies(
                        createStep("testOid1", "testOid2", "testOid3"),
                        createStep("testOid4", "testOid2")
                ),
                createEstimationStep(
                        "testoid1",
                        "extDataSetOid",
                        [
                                createParameterEstimate(
                                        createSymbRefType("p1", "abc"),
                                        createReal(22.0),
                                        false,
                                        createRhs(createReal(0.0)),
                                        createRhs(createReal(30.5))
                                )
                        ],
                        [
                                createEstOperation(
                                        "op",
                                        1,
                                        "Aname",
                                        createAlgorithm(
                                                "AnAlgo",
                                                [
                                                        createProperty("algProp", createRhs(createString("algValue")))
                                                ]
                                        ),
                                        [
                                                createProperty("opProp", createRhs(createString("opValue")))
                                        ]
                                )
                        ]
                )
        )

        def actual = ModellingStepsExtractor.extractPharmML(root)

        assertEquals(2, actual.stepDependencies.size())

        def expectedDeps = [
                                [
                                    step: "testOid1",
                                    deps : ["testOid2", "testOid3" ]
                                ],
                                [
                                        step: "testOid4",
                                        deps : ["testOid2" ]
                                ]
                ]
        for(int i; i < expectedDeps.size(); i++){
            assertEquals(expectedDeps[i].step, actual.stepDependencies[i].oidRef)
            for(int j; j < expectedDeps[i].deps.size(); j++){
                assertEquals(expectedDeps[i].deps[j], actual.stepDependencies[i].depOids[j])
            }
        }
    }

    @Test
    void testMinimalSimulationStep(){
        def root = createModellingSteps(
                createStepDependencies(),
                createSimulationStep(
                        "testoid",
                        "extDataSetOid",
                        [
                                createVariableAssignment(
                                        createSymbRefType("p1", "abc"),
                                        createRhs(createReal(22.0))
                                )
                        ],
                        [
                                createSimOperation(
                                        "op",
                                        1,
                                        "Aname",
                                        []
                                )
                        ]
                )
        )

        def actual = ModellingStepsExtractor.extractPharmML(root)

        assertEquals(0, actual.estimationSteps.size())
        assertEquals(1, actual.simulationSteps.size())
        assertEquals(0, actual.optimalDesignSteps.size())
        def simStep = actual.simulationSteps[0]
        assertEquals("testoid", simStep.oid)
        assertNull(simStep.name)
        assertNull(simStep.description)
        assertEquals("simulation", simStep.type)
        assertEquals("extDataSetOid", simStep.extDatasetRef)
        assertEquals(1, simStep.variableAssignments.size())

        def varAssign = simStep.variableAssignments[0]
        assertEquals("p1.abc", varAssign.symbRef)
        assertEquals("<mn>22.0</mn>", varAssign.maths)

        assertEquals(1, simStep.operations.size())
        def operation = simStep.operations[0]
        assertEquals("Aname", operation.name)
        assertNull(operation.description)
        assertNull(operation.algorithm)
        assertEquals(0, operation.properties.size())
    }

}
