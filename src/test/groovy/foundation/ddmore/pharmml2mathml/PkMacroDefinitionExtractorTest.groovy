/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import foundation.ddmore.pharmml08.dom.modeldefn.AbsorptionOralMacroArgumentType
import foundation.ddmore.pharmml08.dom.modeldefn.EliminationMacroArgumentType
import org.junit.Test

import static foundation.ddmore.pharmml08.builder.MathsBuilder.*
import static foundation.ddmore.pharmml08.builder.StructuralModelBuilder.createAbsorptionMacro
import static foundation.ddmore.pharmml08.builder.StructuralModelBuilder.createAbsorptionOralMacro
import static foundation.ddmore.pharmml08.builder.StructuralModelBuilder.createEliminationMacro
import static foundation.ddmore.pharmml08.builder.StructuralModelBuilder.createPkMacro
import static org.junit.Assert.assertEquals

/**
 * Created by stumoodie on 05/01/2017.
 */
class PkMacroDefinitionExtractorTest {

   @Test
    void testPKMacro(){
        def root = createAbsorptionMacro([AbsorptionOralMacroArgumentType.TLAG, AbsorptionOralMacroArgumentType.ADM],
                                                [createRhs(createSymbRef("t0")), createRhs(createSymbRef("c0"))]
                )

        def expected = "<mrow><mi>absorption</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>TLAG</mi><mo>=</mo><mi>t0</mi></mrow><mrow><mi>ADM</mi><mo>=</mo><mi>c0</mi></mrow></mfenced></mrow>"
        def actual = PkMacroDefinitionExtractor.extractPkMacro(root)
        assertEquals(expected, actual)
    }

    @Test
    void testElimPKMacro(){
        def root = createEliminationMacro([EliminationMacroArgumentType.CMT, EliminationMacroArgumentType.V, EliminationMacroArgumentType.CL],
                [createRhs(createInt(1)), createRhs(createSymbRef("pm", "V")), createRhs(createSymbRef("pm", "CL"))]
        )

        def expected = "<mrow><mi>elimination</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>CMT</mi><mo>=</mo><mn>1</mn></mrow><mrow><mi>V</mi><mo>=</mo><mi>pm.V</mi></mrow><mrow><mi>CL</mi><mo>=</mo><mi>pm.CL</mi></mrow></mfenced></mrow>"
        def actual = PkMacroDefinitionExtractor.extractPkMacro(root)
        assertEquals(expected, actual)
    }

}
