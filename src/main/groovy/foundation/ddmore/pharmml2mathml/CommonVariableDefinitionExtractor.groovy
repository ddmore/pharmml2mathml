/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import foundation.ddmore.pharmml08.dom.IndependentVariableType
import foundation.ddmore.pharmml08.dom.commontypes.*
import foundation.ddmore.pharmml08.dom.modeldefn.PkMacrosType

/**
 * Created by stumoodie on 05/01/2017.
 */
class CommonVariableDefinitionExtractor extends MathMlExtractor {

    String defaultIdv

    CommonVariableDefinitionExtractor() {
        super()
        defaultIdv = "t"
    }

    static String extractCommonVarDefn(CommonVariableDefinitionType node, String idv = "t") {
        def walker = new CommonVariableDefinitionExtractor()
        walker.extract(node, idv)
    }

    String extract(CommonVariableDefinitionType node, String idv) {
        this.defaultIdv = idv
        def covDefn = builder.bind {
            visitClosure(visit(node, node), delegate)
        }

        covDefn.toString()
    }

    def visit(VariableDefinitionType node, parent) {
        return {
            mrow{
                mi node.symbId
                if (node.assign) {
                    mo("=")
                    visitClosure(visit(node.assign, node), delegate)
                }
            }
        }
    }

    def visit(DerivativeVariableType node, parent) {
        return {
            mrow {
                mtable(groupalign: "{left}") {
                    mtr {
                        mtd {
                            mrow {
                                mfrac {
                                    mrow {
                                        mo {
                                            mkp.yieldUnescaped "&DifferentialD;"
                                        }
                                    }
                                    mrow {
                                        mo {
                                            mkp.yieldUnescaped "&DifferentialD;"
                                        }
                                        if (node.independentVariable) {
                                            visitClosure(visit(node.getIndependentVariable(), node), delegate)
                                        } else {
                                            mi this.defaultIdv
                                        }
                                    }
                                }
                                mi node.symbId
                                mrow {
                                    maligngroup()
                                    mo("=")
                                    visitClosure(visit(node.assign, node), delegate)
                                }
                            }
                        }
                    }
                    mtr {
                        mtd {
                            mrow {
                                mi node.symbId
                                mo {
                                    mkp.yieldUnescaped "&ApplyFunction;"
                                }
                                mfenced {
                                    mrow {
                                        if (node.independentVariable) {
                                            visitClosure(visit(node.independentVariable, node), delegate)
                                        } else {
                                            mi this.defaultIdv
                                        }
                                        mo "="
                                        if (node.initialCondition?.initialTime) {
                                            visitClosure(visit(node.initialCondition.initialTime, node), delegate)
                                        } else {
                                            mn(0)
                                        }
                                    }
                                }
                                maligngroup()
                                mo "="
                                if (node.initialCondition?.initialValue) {
                                    visitClosure(visit(node.initialCondition.initialValue, node), delegate)
                                } else {
                                    mn(0)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    def visit(PkMacrosType node, parent){
        return {
            mi node.name
            mo {
                mkp.yieldUnescaped "&ApplyFunction;"
            }
            mfenced {
                node.listOfValue.each {val->
                    mrow{
                        mi val.argument
                        mo "="
                        visitClosure(node(val.assign), delegate)
                    }
                }
            }
        }
    }


    def visit(IndependentVariableReferenceType node, parent){
        return {
            visitClosure(visit(node.symbRef, node), delegate)
        }
    }

    def visit(StandardAssignType node, parent){
        return {
            visitClosure(visit(node.assign, node), delegate)
        }
    }

}
