/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml;

import foundation.ddmore.pharmml08.dom.maths.BinaryOperator
import foundation.ddmore.pharmml08.dom.maths.LogicalBinOperator
import foundation.ddmore.pharmml08.dom.maths.LogicalUniOperator
import foundation.ddmore.pharmml08.dom.maths.UnaryOperator
import foundation.ddmore.pharmml08.dom.maths.UniopType;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by stumoodie on 13/05/2017.
 */
class OperatorComparatorTest {
    def OperatorComparator testInstance;

    @Before
    public void setUp() throws Exception {
        testInstance = new OperatorComparator()

    }

    @After
    public void tearDown() throws Exception {
        testInstance = null
    }

    @Test
    public void compareSame() throws Exception {
        assertEquals(0, testInstance.compare(BinaryOperator.TIMES, BinaryOperator.TIMES));
    }

    @Test
    public void compareLower() throws Exception {
        assertEquals(-1, testInstance.compare(BinaryOperator.PLUS, BinaryOperator.TIMES));
    }

    @Test
    public void compareHigher() throws Exception {
        assertEquals(1, testInstance.compare(BinaryOperator.POWER, BinaryOperator.MINUS));
    }

    @Test
    public void compareUnaryLower() throws Exception {
        assertEquals(-1, testInstance.compare(BinaryOperator.POWER, UnaryOperator.MINUS));
    }

    @Test
    public void compareMinusFactorial() throws Exception {
        assertEquals(-1, testInstance.compare(BinaryOperator.MINUS, UnaryOperator.FACTORIAL));
    }

    @Test
    public void compareFactorialMinus() throws Exception {
        assertEquals(1, testInstance.compare(UnaryOperator.FACTORIAL, BinaryOperator.MINUS));
    }

    @Test
    public void compareAndNot() throws Exception {
        assertEquals(-1, testInstance.compare(LogicalBinOperator.AND, LogicalUniOperator.NOT));
    }

    @Test
    public void compareNotNot() throws Exception {
        assertEquals(0, testInstance.compare(LogicalUniOperator.NOT, LogicalUniOperator.NOT));
    }

    @Test
    public void compareNotIsDefined() throws Exception {
        assertEquals(1, testInstance.compare(LogicalUniOperator.NOT, LogicalUniOperator.IS_DEFINED));
    }


    @Test
    public void compareAndAnd() throws Exception {
        assertEquals(0, testInstance.compare(LogicalBinOperator.AND, LogicalBinOperator.AND));
    }

    @Test
    public void compareAndOR() throws Exception {
        assertEquals(1, testInstance.compare(LogicalBinOperator.AND, LogicalBinOperator.OR));
    }


    @Test
    public void compareAndEquals() throws Exception {
        assertEquals(-1, testInstance.compare(LogicalBinOperator.AND, LogicalBinOperator.EQ));
    }


    @Test
    public void compareEqualsNE() throws Exception {
        assertEquals(0, testInstance.compare(LogicalBinOperator.EQ, LogicalBinOperator.NEQ));
    }

    @Test
    public void compareGtNE() throws Exception {
        assertEquals(1, testInstance.compare(LogicalBinOperator.GT, LogicalBinOperator.NEQ));
    }

    @Test
    public void compareGtLeq() throws Exception {
        assertEquals(0, testInstance.compare(LogicalBinOperator.GT, LogicalBinOperator.LEQ));
    }

    @Test
    public void compareGtMinus() throws Exception {
        assertEquals(-1, testInstance.compare(LogicalBinOperator.GT, BinaryOperator.MINUS));
    }


    @Test
    public void compareGtMin() throws Exception {
        assertEquals(1, testInstance.compare(LogicalBinOperator.GT, BinaryOperator.MIN));
    }

    @Test
    public void compareOrAbs() throws Exception {
        assertEquals(1, testInstance.compare(LogicalBinOperator.OR, UnaryOperator.ABS));
    }

}