/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import foundation.ddmore.pharmml08.dom.modeldefn.DistributionType
import foundation.ddmore.pharmml08.dom.probonto.DistroType
import foundation.ddmore.pharmml08.dom.modeldefn.UncertMLType
import foundation.ddmore.pharmml08.dom.probonto.ProbOntoType
import foundation.ddmore.pharmml08.dom.uncertml.ContinuousValueType
import foundation.ddmore.pharmml08.dom.uncertml.NormalDistribution
import foundation.ddmore.pharmml08.dom.uncertml.PositiveRealValueType
import foundation.ddmore.pharmml08.dom.uncertml.VarRefType
import org.slf4j.LoggerFactory

import javax.swing.tree.TreeNode

/**
 * Created by stumoodie on 05/01/2017.
 */
class DistributionExtractor extends MathMlExtractor {
    static final LOGGER = LoggerFactory.getLogger(DistributionExtractor.class)

    DistributionExtractor(){
        super()
    }

    static String extract(DistributionType node){
        def walker = new DistributionExtractor()
        walker.extractDistribution(node)
    }

    String extractDistribution(DistributionType node){
        def distnDoc = builder.bind{
            visitClosure(visit(node, node), delegate)
        }

        distnDoc.toString()
    }

    def visit(DistributionType node, parent){
        LOGGER.debug("Distribution node=" + node + ", parent=" + parent)
        return {
            if(node.probOnto){
                visitClosure(visit(node.probOnto, node), delegate)
            }
            if(node.uncertML){
                visitClosure(visit(node.uncertML, node), delegate)
            }
        }
    }

    def visit(UncertMLType node, parent){
        LOGGER.debug("Uncertml node=" + node + ", parent=" + parent)
        return{
            if(node.abstractContinuousUnivariateDistribution){
                visitClosure(visit(node.abstractContinuousUnivariateDistribution.value, node), delegate)
            }
        }
    }


    def visit(NormalDistribution node, parent){
        LOGGER.debug("NormDistn node=" + node + ", parent=" + parent)
        return {
            mrow{
                mi("Normal")
                mo{
                    mkp.yieldUnescaped "&ApplyFunction;"
                }
                mfenced{
                    mrow{
                        mi("mean")
                        mo("=")
                        if(node.mean){
                            visitClosure(visit(node.mean, node), delegate)
                        }
                    }
                    if(node.stddev){
                        mrow{
                            mi("sd")
                            mo("=")
                            visitClosure(visit(node.stddev, node), delegate)
                        }
                    }
                    else{
                        mrow{
                            mi("var")
                            mo("=")
                            visitClosure(visit(node.variance, node), delegate)
                        }
                    }
                }
            }
        }
//        op("&ApplyFunction;")
//        buf.append("<mrow>")
//        op("(")
//        id("mean")
//        op("=")
//        if(node.mean.RVal)
//            num(node.mean.RVal)
//        else
//            id(node.mean.var.varId)
//        op(",")
//        if(node.stddev){
//            id("sd")
//            op("=")
//            if(node.stddev.prVal){
//                num(node.stddev.prVal)
//            }
//            else{
//                id(node.stddev.var.varId)
//            }
//        }
//        else{
//            id("var")
//            op("=")
//            if(node.variance.prVal)
//                num(node.variance.prVal)
//            else
//                id(node.variance.var.varId)
//        }
//        op(")")
//        buf.append("</mrow>")
//        buf.append("</mrow>")
    }

    def visit(ContinuousValueType node, parent){
        LOGGER.debug("ContinuousValueType node=" + node + ", parent=" + parent)
        return {
            if(node.RVal != null){
                mn(node.RVal)
            }
            else if(node.var){
                visitClosure(visit(node.var, node), delegate)
            }
        }
    }

    def visit(PositiveRealValueType node, parent){
        LOGGER.debug("PositiveRealValueType node=" + node + ", parent=" + parent)
        return {
            if(node.prVal != null){
                mn(node.prVal)
            }
            else if(node.var){
                visitClosure(visit(node.var, node), delegate)
            }
        }
    }

    def visit(Double node, parent){
        LOGGER.debug("Double node=" + node + ", parent=" + parent)
        return {
            mn(node)
        }
    }

    def visit(VarRefType node, parent){
        LOGGER.debug("VarRefType node=" + node + ", parent=" + parent)
        return {
            mi(node.varId)
        }
    }

    def visit(ProbOntoType node, parent){
        LOGGER.debug("Probonto node=" + node + ", parent=" + parent)
        return{
            mrow {
                mi(node.name.value())
                mo {
                    mkp.yieldUnescaped "&ApplyFunction;"
                }
                mfenced {
                    node.getParameter().each {param->
                        mrow{
                            mi(param.name.value().toLowerCase())
                            mo("=")
                            visitClosure(visit(param.assign, node), delegate)
                        }
                    }
//                for(def param : node.listOfParameter){
////                    nrow{
//                        mi(param.name.toLowerCase())
//                        mo("=")
//                        visitClosure(visit(param.assign), delegate)
////                    }
//                }

                }
            }
        }
    }

}
