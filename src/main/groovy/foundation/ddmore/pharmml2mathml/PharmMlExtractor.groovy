/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import foundation.ddmore.pharmml08.dom.IndependentVariableType
import foundation.ddmore.pharmml08.dom.PharmML
import foundation.ddmore.pharmml08.dom.commontypes.FunctionDefinitionType
import foundation.ddmore.pharmml08.dom.modellingsteps.EstimationStepType
import foundation.ddmore.pharmml08.dom.modellingsteps.ModellingStepsType
import foundation.ddmore.pharmml08.dom.modellingsteps.OptimalDesignStepType
import foundation.ddmore.pharmml08.dom.modellingsteps.ParameterEstimateType
import foundation.ddmore.pharmml08.dom.modellingsteps.SimulationStepType
import foundation.ddmore.pharmml08.dom.modellingsteps.ToEstimateType

/**
 * Created by stumoodie on 05/01/2017.
 */
class PharmMlExtractor {

    PharmMlExtractor(){
    }

    static extractPharmML(PharmML node){
        def walker = new PharmMlExtractor()
        walker.extract(node)
    }

    def extract(PharmML node){
        visit(node)
    }

    def visit(PharmML node){
        def retVal = [
                name : "",
                descn : "",
                indVars : [],
                funcDefns : [],
                modelDefinition : [
                    covariateModels : [],
                    parameterModels : [],
                    structuralModels : [],
                    observationModels : []
                ],
                trialDesign : null,
                modellingSteps : [
                        simulationSteps : [],
                        estimationSteps : [],
                        optimisationSteps : [],
                        stepDependencies : []
                ]
        ]

        retVal.name = node.getName().value
        if(node.description)
            retVal.descn = node.description.value

        node.getIndependentVariable().each { idv->
            retVal.indVars.add(visit(idv))
        }

        node.getFunctionDefinition().each { df ->
            retVal.funcDefns.add(visit(df))
        }

        def cms = CovariateModelExtractor.extractCovariateModels(node.modelDefinition)
        retVal.modelDefinition.covariateModels = cms.covariateModels

        def pms = ParameterModelExtractor.extractParametersModels(node.modelDefinition)
        retVal.modelDefinition.parameterModels = pms.parameterModels

        def idv = retVal.indVars.isEmpty() ? "t" : retVal.indVars[0]
        def sms = StructuralModelExtractor.extractStructuralModels(node.modelDefinition, idv)
        retVal.modelDefinition.structuralModels = sms.structuralModels

        def obs = ObservationModelExtractor.extractObservationModels(node.modelDefinition)
        retVal.modelDefinition.observationModels = obs.observationModels


        if(node.trialDesign){
            retVal.trialDesign = TrialDesignExtractor.extractTrialDesign(node.trialDesign)
        }

        if(node.modellingSteps)
            retVal.modellingSteps = ModellingStepsExtractor.extractPharmML(node.modellingSteps)

        retVal
    }

    def visit(IndependentVariableType node){
        node.symbId
    }


    def visit(FunctionDefinitionType node){
        def retVal = [:]

        retVal.symbId = node.symbId
        retVal.maths = FunctionDefinitionExtractor.extractFuncDefn(node)

        retVal
    }


 }
