/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import foundation.ddmore.pharmml08.builder.MathsBuilder
import foundation.ddmore.pharmml08.builder.StructuralModelBuilder
import org.junit.Test

import static foundation.ddmore.pharmml08.builder.MathsBuilder.createReal
import static foundation.ddmore.pharmml08.builder.MathsBuilder.createRhs
import static foundation.ddmore.pharmml08.builder.StructuralModelBuilder.createDerivative
import static foundation.ddmore.pharmml08.builder.StructuralModelBuilder.createVariable
import static org.junit.Assert.assertEquals

/**
 * Created by stumoodie on 05/01/2017.
 */
class CommonVariableDefinitionExtractorTest {

    @Test
    void testSimpleVariableRhs(){
        def root = StructuralModelBuilder.createVariable(
                "v",
                MathsBuilder.createRhs(
                        MathsBuilder.createReal(0.0)
                )
        )

        def actual = CommonVariableDefinitionExtractor.extractCommonVarDefn(root)
        def expected = "<mrow><mi>v</mi><mo>=</mo><mn>0.0</mn></mrow>"
        assertEquals(expected, actual)
    }


    @Test
    void testDerivativeNoInitTime(){
        def root = StructuralModelBuilder.createDerivative(
                "v",
                MathsBuilder.createRhs(
                        MathsBuilder.createReal(10.0)
                ),
                MathsBuilder.createRhs(
                        MathsBuilder.createReal(22.3)
                ),
                null,
                "T"
        )

        def expected = "<mrow><mtable groupalign=\"{left}\"><mtr><mtd><mrow><mfrac><mrow><mo>&DifferentialD;</mo></mrow><mrow><mo>&DifferentialD;</mo><mi>T</mi></mrow></mfrac><mi>v</mi><mrow><maligngroup/><mo>=</mo><mn>10.0</mn></mrow></mrow></mtd></mtr><mtr><mtd><mrow><mi>v</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>T</mi><mo>=</mo><mn>0</mn></mrow></mfenced><maligngroup/><mo>=</mo><mn>22.3</mn></mrow></mtd></mtr></mtable></mrow>"
        def actual = CommonVariableDefinitionExtractor.extractCommonVarDefn(root)
        assertEquals(expected, actual)
    }

    @Test
    void testDerivativeInitTimeAndVal(){
        def root = StructuralModelBuilder.createDerivative(
                "v",
                MathsBuilder.createRhs(
                        MathsBuilder.createReal(10.0)
                ),
                MathsBuilder.createRhs(
                        MathsBuilder.createReal(22.3)
                ),
                MathsBuilder.createRhs(
                        MathsBuilder.createReal(-2.3)
                ),
                "T"
        )

        def expected = "<mrow><mtable groupalign=\"{left}\"><mtr><mtd><mrow><mfrac><mrow><mo>&DifferentialD;</mo></mrow><mrow><mo>&DifferentialD;</mo><mi>T</mi></mrow></mfrac><mi>v</mi><mrow><maligngroup/><mo>=</mo><mn>10.0</mn></mrow></mrow></mtd></mtr><mtr><mtd><mrow><mi>v</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>T</mi><mo>=</mo><mn>-2.3</mn></mrow></mfenced><maligngroup/><mo>=</mo><mn>22.3</mn></mrow></mtd></mtr></mtable></mrow>"
        def actual = CommonVariableDefinitionExtractor.extractCommonVarDefn(root)
        assertEquals(expected, actual)
    }

    @Test
    void testDerivativeNoInitVal(){
        def root = StructuralModelBuilder.createDerivative(
                "v",
                MathsBuilder.createRhs(
                        MathsBuilder.createReal(10.0)
                ),
                null,
                MathsBuilder.createRhs(
                        MathsBuilder.createReal(-2.3)
                ),
                "T"
        )

        def expected = "<mrow><mtable groupalign=\"{left}\"><mtr><mtd><mrow><mfrac><mrow><mo>&DifferentialD;</mo></mrow><mrow><mo>&DifferentialD;</mo><mi>T</mi></mrow></mfrac><mi>v</mi><mrow><maligngroup/><mo>=</mo><mn>10.0</mn></mrow></mrow></mtd></mtr><mtr><mtd><mrow><mi>v</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>T</mi><mo>=</mo><mn>-2.3</mn></mrow></mfenced><maligngroup/><mo>=</mo><mn>0</mn></mrow></mtd></mtr></mtable></mrow>"
        def actual = CommonVariableDefinitionExtractor.extractCommonVarDefn(root)
        assertEquals(expected, actual)
    }

    @Test
    void testDerivativeNoInitValNoIdv(){
        def root = StructuralModelBuilder.createDerivative(
                "v",
                MathsBuilder.createRhs(
                        MathsBuilder.createReal(10.0)
                ),
                null,
                MathsBuilder.createRhs(
                        MathsBuilder.createReal(-2.3)
                )
        )

        def expected = "<mrow><mtable groupalign=\"{left}\"><mtr><mtd><mrow><mfrac><mrow><mo>&DifferentialD;</mo></mrow><mrow><mo>&DifferentialD;</mo><mi>t</mi></mrow></mfrac><mi>v</mi><mrow><maligngroup/><mo>=</mo><mn>10.0</mn></mrow></mrow></mtd></mtr><mtr><mtd><mrow><mi>v</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>t</mi><mo>=</mo><mn>-2.3</mn></mrow></mfenced><maligngroup/><mo>=</mo><mn>0</mn></mrow></mtd></mtr></mtable></mrow>"
        def actual = CommonVariableDefinitionExtractor.extractCommonVarDefn(root, "t")
        assertEquals(expected, actual)
    }

}
