/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import foundation.ddmore.pharmml08.builder.MathsBuilder
import foundation.ddmore.pharmml08.builder.StructuralModelBuilder
import foundation.ddmore.pharmml08.dom.commontypes.SymbolTypeType
import org.junit.Test

import static foundation.ddmore.pharmml08.builder.PharmMlBuilder.createFunctionDefinition
import static foundation.ddmore.pharmml08.builder.PharmMlBuilder.createFunctionParameter
import static foundation.ddmore.pharmml08.builder.PharmMlBuilder.createFunctionParameter
import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertNotNull

/**
 * Created by stumoodie on 05/01/2017.
 */
class FunctionDefinitionExtractorTest {

    @Test
    void testFunctionDefinition(){
        def root = createFunctionDefinition(
                "testFunc",
                SymbolTypeType.REAL,
                MathsBuilder.createRhs(
                        MathsBuilder.createReal(0.0)
                ),
                createFunctionParameter("p1", SymbolTypeType.REAL),
                createFunctionParameter("p2", SymbolTypeType.INT),
        )


        def expected = "<mrow><mrow><mi>testFunc</mi><mo>:</mo><mi>real</mi></mrow><mo>&ApplyFunction;</mo><mfenced><mrow><mi>p1</mi><mo>:</mo><mi>real</mi></mrow><mrow><mi>p2</mi><mo>:</mo><mi>int</mi></mrow></mfenced><mo>=</mo><mn>0.0</mn></mrow>"
        def actual = FunctionDefinitionExtractor.extractFuncDefn(root)

        assertEquals(expected, actual)
    }

}
