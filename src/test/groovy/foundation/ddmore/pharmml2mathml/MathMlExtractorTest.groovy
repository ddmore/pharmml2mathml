/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import com.sun.org.apache.xalan.internal.xsltc.compiler.util.RealType
import foundation.ddmore.pharmml08.dom.commontypes.MatrixType
import foundation.ddmore.pharmml08.dom.commontypes.MatrixTypeType
import foundation.ddmore.pharmml08.dom.commontypes.MissingValueType
import foundation.ddmore.pharmml08.dom.maths.BinaryOperator
import foundation.ddmore.pharmml08.dom.maths.BinopType
import foundation.ddmore.pharmml08.dom.maths.ConstantValueType
import foundation.ddmore.pharmml08.dom.maths.LogicalBinOperator
import foundation.ddmore.pharmml08.dom.maths.LogicalUniOperator
import foundation.ddmore.pharmml08.dom.maths.MatrixUniOperatorType
import foundation.ddmore.pharmml08.dom.maths.UnaryOperator
import org.junit.Test

import static foundation.ddmore.pharmml08.builder.MathsBuilder.*
import static org.junit.Assert.assertEquals

/**
 * Created by stumoodie on 04/01/2017.
 */
class MathMlExtractorTest {

    @Test
    void testScalarInt(){
        def root = createRhs(
                createInt(22)
        )

        def actual = MathMlExtractor.extract(root)
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mn>22</mn></math>"
        assertEquals(expected, actual)
    }

    @Test
    void testScalarReal(){
        def root = createRhs(
                createReal(23.55)
        )

        def actual = MathMlExtractor.extract(root)
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mn>23.55</mn></math>"
        assertEquals(expected, actual)
    }

    @Test
    void testScalarVerySmallReal(){
        def root = createRhs(
                createReal(2.355e-10)
        )

        def actual = MathMlExtractor.extract(root)
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mn>2.355E-10</mn></math>"
        assertEquals(expected, actual)
    }

    @Test
    void testScalarBooleanTrue(){
        def root = createRhs(
                createBoolean(true)
        )

        def actual = MathMlExtractor.extract(root)
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mi>T</mi></math>"
        assertEquals(expected, actual)
    }

    @Test
    void testScalarBooleanFalse(){
        def root = createRhs(
                createBoolean(false)
        )

        def actual = MathMlExtractor.extract(root)
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mi>F</mi></math>"
        assertEquals(expected, actual)
    }

    @Test
    void testScalarInfinity(){
        def root = createRhs(
            createPlusInf()
        )

        def actual = MathMlExtractor.extract(root)
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mi>&infin;</mi></math>"
        assertEquals(expected, actual)
    }

    @Test
    void testScalarNegInfinity(){
        def root = createRhs(
               createMinusInf()
        )

        def actual = MathMlExtractor.extract(root)
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo form=\"prefix\">-</mo><mi>&infin;</mi></math>"
        assertEquals(expected, actual)
    }

    @Test
    void testConstantPi(){
        def root = createRhs(
                createConstant(ConstantValueType.PI)
        )

        def actual = MathMlExtractor.extract(root)
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mi>&pi;</mi></math>"
        assertEquals(expected, actual)
    }

    @Test
    void testConstantExp(){
        def root = createRhs(
                createConstant(ConstantValueType.EXPONENTIALE)
        )

        def actual = MathMlExtractor.extract(root)
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mi>&ExponentialE;</mi></math>"
        assertEquals(expected, actual)
    }

    @Test
    void testSimpleBinOp(){
        def root = createAssign(
                createBinop(
                        BinaryOperator.PLUS,
                        createReal(22.0),
                        createInt(33)
                )
        )

        def actual = MathMlExtractor.extract(root)
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mn>22.0</mn><mo>+</mo><mn>33</mn></math>"
        assertEquals(expected, actual)
    }

    @Test
    void testSimpleBinOpNoMaths(){
        def root = createRhs(
                createBinop(
                        BinaryOperator.PLUS,
                        createReal(22.0),
                        createInt(33)
                )
        )

        def actual = MathMlExtractor.extract(root, false)
        def expected = "<mn>22.0</mn><mo>+</mo><mn>33</mn>"
        assertEquals(expected, actual)
    }

    @Test
    void testRightNestedBinOp(){
        def root = createRhs(
                createBinop(
                        BinaryOperator.PLUS,
                        createReal(22.0),
                        createBinop(
                                BinaryOperator.TIMES,
                                createSymbRef("x"),
                                createInt(33)
                        )
                )
        )

        def actual = MathMlExtractor.extract(root)
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mn>22.0</mn><mo>+</mo><mi>x</mi><mo>&#x022C5;</mo><mn>33</mn></math>"
        assertEquals(expected, actual)
    }

    @Test
    void testLeftNestedBinOp(){
        def root = createRhs(
                createBinop(
                        BinaryOperator.TIMES,
                        createBinop(
                                BinaryOperator.PLUS,
                                createReal(22.0),
                                createSymbRef("x")
                        ),
                        createInt(33)

                )
        )
//        def root = new Rhs()
//        def binop = new Binop()
//        root.binop = binop
//        binop.operator = BinaryOperator.TIMES
//        binop.operand2 = new IntValue(33)
//        def binop2 = new Binop()
//        binop.operand1 = binop2
//        binop2.operator = BinaryOperator.PLUS
//        binop2.operand1 = new RealValue(22.0)
//        binop2.operand2 = new SymbolRef("x")

        def actual = MathMlExtractor.extract(root)
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mfenced><mrow><mn>22.0</mn><mo>+</mo><mi>x</mi></mrow></mfenced><mo>&#x022C5;</mo><mn>33</mn></math>"
        assertEquals(expected, actual)
    }

    @Test
    void testSimpleUniOp(){
        def root = createRhs(
                createUniop(
                        UnaryOperator.MINUS,
                        createReal(22.0),
                )
        )

        def actual = MathMlExtractor.extract(root)
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo form=\"prefix\">-</mo><mn>22.0</mn></math>"
        assertEquals(expected, actual)
    }

    @Test
    void testExponentialUniOp(){
        def root = createRhs(
                createUniop(
                        UnaryOperator.EXP,
                        createBinop(
                                BinaryOperator.DIVIDE,
                                createSymbRef("x"),
                                createSymbRef("y")
                        )
                )
        )

        def actual = MathMlExtractor.extract(root)
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><msup><mrow><mi>&ExponentialE;</mi></mrow><mrow><mfrac><mrow><mi>x</mi></mrow><mrow><mi>y</mi></mrow></mfrac></mrow></msup></math>"
        assertEquals(expected, actual)
    }

    @Test
    void testSimpleSqrtUniOp(){
        def root = createRhs(
                createUniop(
                        UnaryOperator.SQRT,
                        createReal(22.0),
                )
        )

        def actual = MathMlExtractor.extract(root)
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><msqrt><mn>22.0</mn></msqrt></math>"
        assertEquals(expected, actual)
    }

    @Test
    void testMinusWithExpressionArgsUniOp(){
        def root = createRhs(
                createUniop(
                        UnaryOperator.MINUS,
                        createBinop(
                                BinaryOperator.POWER,
                                createSymbRef("x"),
                                createInt(33)
                        )
                )
        )
//        def root = new Rhs()
//        def uniop = new Uniop()
//        def binop2 = new Binop()
//        binop2.operator = BinaryOperator.POWER
//        binop2.operand1 = new SymbolRef("x")
//        binop2.operand2 = new IntValue(33)
//
//        uniop.operator = UnaryOperator.MINUS
//        uniop.value = binop2
//        root.uniop = uniop

        def actual = MathMlExtractor.extract(root)
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo form=\"prefix\">-</mo><msup><mrow><mi>x</mi></mrow><mrow><mn>33</mn></mrow></msup></math>"
        assertEquals(expected, actual)
    }

    @Test
    void testAbsWithExpressionsArgs(){
        def root = createRhs(
                createUniop(
                        UnaryOperator.ABS,
                        createBinop(
                                BinaryOperator.POWER,
                                createSymbRef("x"),
                                createInt(33)
                        )
                )
        )
//        def root = new Rhs()
//        def uniop = new Uniop()
//        def binop2 = new Binop()
//        binop2.operator = BinaryOperator.POWER
//        binop2.operand1 = new SymbolRef("x")
//        binop2.operand2 = new IntValue(33)
//
//        uniop.operator = UnaryOperator.ABS
//        uniop.value = binop2
//        root.uniop = uniop

        def actual = MathMlExtractor.extract(root)
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>|</mo><msup><mrow><mi>x</mi></mrow><mrow><mn>33</mn></mrow></msup><mo>|</mo></math>"
        assertEquals(expected, actual)
    }

    @Test
    void testFactorialWithSumArgs(){
        def root = createRhs(
                createUniop(
                        UnaryOperator.FACTORIAL,
                        createBinop(
                                BinaryOperator.MINUS,
                                createSymbRef("x"),
                                createInt(33)
                        )
                )
        )
//        def root = new Rhs()
//        def uniop = new Uniop()
//        def binop2 = new Binop()
//        binop2.operator = BinaryOperator.MINUS
//        binop2.operand1 = new SymbolRef("x")
//        binop2.operand2 = new IntValue(33)

//        uniop.operator = UnaryOperator.FACTORIAL
//        uniop.value = binop2
//        root.uniop = uniop

        def actual = MathMlExtractor.extract(root)
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mfenced><mrow><mi>x</mi><mo>-</mo><mn>33</mn></mrow></mfenced><mo>!</mo></math>"
        assertEquals(expected, actual)
    }

    @Test
    void testSinWithSumExpr(){
        def root = createRhs(
                createUniop(
                        UnaryOperator.SIN,
                        createBinop(
                                BinaryOperator.MINUS,
                                createSymbRef("x"),
                                createInt(33)
                        )
                )
        )
//        def root = new Rhs()
//        def uniop = new Uniop()
//        def binop2 = new Binop()
//        binop2.operator = BinaryOperator.MINUS
//        binop2.operand1 = new SymbolRef("x")
//        binop2.operand2 = new IntValue(33)
//
//        uniop.operator = UnaryOperator.SIN
//        uniop.value = binop2
//        root.uniop = uniop

        def actual = MathMlExtractor.extract(root)
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mi>sin</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>x</mi><mo>-</mo><mn>33</mn></mrow></mfenced></math>"
        assertEquals(expected, actual)
    }

    @Test
    void testLn(){
        def root = createRhs(
                createUniop(
                        UnaryOperator.LOG,
                        createReal(22.5)
                )
        )

        def actual = MathMlExtractor.extract(root)
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mi>ln</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mn>22.5</mn></mrow></mfenced></math>"
        assertEquals(expected, actual)
    }

    @Test
    void testLn2(){
        def root = createRhs(
                createUniop(
                        UnaryOperator.LOG_2,
                        createReal(22.5)
                )
        )

        def actual = MathMlExtractor.extract(root)
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><msub><mi>log</mi><mn>2</mn></msub><mo>&ApplyFunction;</mo><mfenced><mrow><mn>22.5</mn></mrow></mfenced></math>"
        assertEquals(expected, actual)
    }

    @Test
    void testLn10(){
        def root = createRhs(
                createUniop(
                        UnaryOperator.LOG_10,
                        createReal(44.4)
                )
        )

        def actual = MathMlExtractor.extract(root)
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><msub><mi>log</mi><mn>10</mn></msub><mo>&ApplyFunction;</mo><mfenced><mrow><mn>44.4</mn></mrow></mfenced></math>"
        assertEquals(expected, actual)
    }
    @Test
    void testComplexEquation(){
        def root = createRhs(
                createBinop(
                        BinaryOperator.MINUS,
                        createSymbRef("sm", "RATEIN"),
                        createBinop(
                                BinaryOperator.DIVIDE,
                                createBinop(
                                        BinaryOperator.TIMES,
                                        createSymbRef("pm", "CL"),
                                        createSymbRef("CENTRAL")
                                ),
                                createSymbRef("pm", "V")
                        )
                )
        )

        def actual = MathMlExtractor.extract(root)
        // (sm.RATEIN - ((pm.CL * CENTRAL)/pm.V))
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mi>sm.RATEIN</mi><mo>-</mo><mfrac><mrow><mi>pm.CL</mi><mo>&#x022C5;</mo><mi>CENTRAL</mi></mrow><mrow><mi>pm.V</mi></mrow></mfrac></math>"
        assertEquals(expected, actual)
    }

    @Test
    void testPricewiseEquation(){
        def root = createRhs(
                createPiecewise(
                        createPiece(
                                createCondition(
                                        createLogicBinop(
                                                LogicalBinOperator.GEQ,
                                                createSymbRef("T"),
                                                createSymbRef("pm", "TLAG")
                                        )
                                ),
                                createBinop(
                                        BinaryOperator.TIMES,
                                        createSymbRef("sm", "GUT"),
                                        createSymbRef("pm", "KA")
                                )
                        ),
                        createPiece(
                                createCondition(createOtherwise()),
                                createInt(0)
                        )
                )
        )

        def actual = MathMlExtractor.extract(root)
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mrow><mo>{</mo><mtable columnalign=\"left\"><mtr><mtd><mrow><mi>sm.GUT</mi><mo>&#x022C5;</mo><mi>pm.KA</mi></mrow></mtd><mtd><mtext>if</mtext></mtd><mtd><mrow><mi>T</mi><mo>&#x2265;</mo><mi>pm.TLAG</mi></mrow></mtd></mtr><mtr><mtd><mrow><mn>0</mn></mrow></mtd><mtd><mtext>otherwise</mtext></mtd><mtd></mtd></mtr></mtable></mrow></math>"
        assertEquals(expected, actual)
    }

    /*
    				<math:Piecewise>
					<math:Piece>
						<ct:SymbRef blkIdRef="sm" symbIdRef="GUT"/>
						<math:Condition>
							<math:LogicBinop op="gt">
								<ds:ColumnRef columnIdRef="AMT"/>
								<ct:Int>0</ct:Int>
							</math:LogicBinop>
						</math:Condition>
					</math:Piece>
				</math:Piecewise>

     */

    @Test
    void testPricewiseSingleColumnRefEquation(){
        def root = createRhs(
                createPiecewise(
                        createPiece(
                                createCondition(
                                        createLogicBinop(
                                                LogicalBinOperator.GT,
                                                createColumnRef("AMT"),
                                                createInt(0)
                                        )
                                ),
                                createSymbRef("sm", "GUT"),
                        )
                )
        )

        def actual = MathMlExtractor.extract(root)
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mrow><mo>{</mo><mtable columnalign=\"left\"><mtr><mtd><mrow><mi>sm.GUT</mi></mrow></mtd><mtd><mtext>if</mtext></mtd><mtd><mrow><mi>AMT</mi><mo>></mo><mn>0</mn></mrow></mtd></mtr></mtable></mrow></math>"
        assertEquals(expected, actual)
    }

    @Test
    void testVectorDefinition(){
        def root = createRhs(
                createVector(
                        createReal(0),
                        createReal(1),
                        createReal(2)
                )
        )

        def actual = MathMlExtractor.extract(root, false)
        def expected = "<mrow><mfenced><mrow><mn>0.0</mn></mrow><mrow><mn>1.0</mn></mrow><mrow><mn>2.0</mn></mrow></mfenced></mrow>"
        assertEquals(expected, actual)
    }


    @Test
    void testVectorSelector(){
        def root = createRhs(
                createVectorSelector(
                        createSymbRefType("v"),
                        createRhs(
                                createInt(3)
                        )
                )
        )

        def actual = MathMlExtractor.extract(root, false)
        def expected = "<mrow><mi>v</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mn>3</mn></mrow></mfenced></mrow>"
        assertEquals(expected, actual)
    }

    @Test
    void testFullMatrixDefinition(){
        def root = createRhs(
                createMatrix(
                        MatrixTypeType.ANY,
                        createMatrixRow(
                                createReal(11),
                                createReal(12),
                                createReal(13)
                        ),
                        createMatrixRow(
                                createReal(21),
                                createReal(22),
                                createReal(23)
                        )
                )
        )

        def actual = MathMlExtractor.extract(root, false)
        def expected = "<mrow><mo>(</mo><mtable><mtr><mtd><mn>11.0</mn></mtd><mtd><mn>12.0</mn></mtd><mtd><mn>13.0</mn></mtd></mtr><mtr><mtd><mn>21.0</mn></mtd><mtd><mn>22.0</mn></mtd><mtd><mn>23.0</mn></mtd></mtr></mtable><mo>)</mo></mrow>"
        assertEquals(expected, actual)
    }


    @Test
    void testMixOfLogicAndNumeric(){
        def root = createRhs(
                createLogicUniop(
                        LogicalUniOperator.NOT,
                        createLogicBinop(
                                LogicalBinOperator.AND,
                                createLogicBinop(
                                        LogicalBinOperator.EQ,
                                        createBinop(
                                                BinaryOperator.PLUS,
                                                createSymbRef("pm", "CL"),
                                                createSymbRef("CENTRAL")
                                        ),
                                        createInt(0)
                                ),
                                createLogicUniop(
                                        LogicalUniOperator.IS_DEFINED,
                                        createSymbRef("sm", "RATEIN"),
                                )
                        )
                )
        )

        def actual = MathMlExtractor.extract(root)
        // (sm.RATEIN - ((pm.CL * CENTRAL)/pm.V))
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mo>!</mo><mfenced><mrow><mi>pm.CL</mi><mo>+</mo><mi>CENTRAL</mi><mo>=</mo><mn>0</mn><mo>&#x2227;</mo><mi>isDefined</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>sm.RATEIN</mi></mrow></mfenced></mrow></mfenced></math>"
        assertEquals(expected, actual)
    }

    @Test
    void testLogicOpPrecedenceNoParen(){
        def root = createRhs(
            createLogicBinop(
                LogicalBinOperator.OR,
                createLogicBinop(
                    LogicalBinOperator.AND,
                        createSymbRef("a"),
                        createSymbRef("b")
                ),
                createSymbRef("c"),
            )
        )

        def actual = MathMlExtractor.extract(root)
        // (sm.RATEIN - ((pm.CL * CENTRAL)/pm.V))
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mi>a</mi><mo>&#x2227;</mo><mi>b</mi><mo>&#x2228;</mo><mi>c</mi></math>"
        assertEquals(expected, actual)
    }

    @Test
    void testLogicOpPrecedenceSamePrecedence(){
        def root = createRhs(
                createLogicBinop(
                        LogicalBinOperator.OR,
                        createLogicBinop(
                                LogicalBinOperator.OR,
                                createSymbRef("a"),
                                createSymbRef("b")
                        ),
                        createSymbRef("c"),
                )
        )

        def actual = MathMlExtractor.extract(root)
        // (sm.RATEIN - ((pm.CL * CENTRAL)/pm.V))
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mi>a</mi><mo>&#x2228;</mo><mi>b</mi><mo>&#x2228;</mo><mi>c</mi></math>"
        assertEquals(expected, actual)
    }

    @Test
    void testLogicOpPrecedenceLeftPrecedence(){
        def root = createRhs(
                createLogicBinop(
                        LogicalBinOperator.AND,
                        createLogicBinop(
                                LogicalBinOperator.OR,
                                createSymbRef("a"),
                                createSymbRef("b")
                        ),
                        createSymbRef("c"),
                )
        )

        def actual = MathMlExtractor.extract(root)
        // (sm.RATEIN - ((pm.CL * CENTRAL)/pm.V))
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mfenced><mrow><mi>a</mi><mo>&#x2228;</mo><mi>b</mi></mrow></mfenced><mo>&#x2227;</mo><mi>c</mi></math>"
        assertEquals(expected, actual)
    }

    @Test
    void testInverMatrixOp(){
        def root = createRhs(
            createMatrixUniOp(
                    MatrixUniOperatorType.INVERSE,
                    createSymbRef("A")
            )
        )

        def actual = MathMlExtractor.extract(root)
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><msup><mrow><mi>A</mi></mrow><mrow><mn>-1</mn></mrow></msup></math>"
        assertEquals(expected, actual)

    }

    @Test
    void testTransposeMatrixOp(){
        def root = createRhs(
                createMatrixUniOp(
                        MatrixUniOperatorType.TRANSPOSE,
                        createSymbRef("A")
                )
        )

        def actual = MathMlExtractor.extract(root)
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><msup><mrow><mi>A</mi></mrow><mrow><mo>T</mo></mrow></msup></math>"
        assertEquals(expected, actual)

    }

    @Test
    void testTraceMatrixOp(){
        def root = createRhs(
                createMatrixUniOp(
                        MatrixUniOperatorType.TRACE,
                        createSymbRef("A")
                )
        )

        def actual = MathMlExtractor.extract(root)
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mrow><mo>Tr</mo><mi>A</mi></mrow></math>"
        assertEquals(expected, actual)

    }

    @Test
    void testDeterminantMatrixOp(){
        def root = createRhs(
                createMatrixUniOp(
                        MatrixUniOperatorType.DETERMINANT,
                        createSymbRef("A")
                )
        )

        def actual = MathMlExtractor.extract(root)
        def expected = "<math xmlns=\"http://www.w3.org/1998/Math/MathML\"><mrow><mo>det</mo><mi>A</mi></mrow></math>"
        assertEquals(expected, actual)

    }

}
