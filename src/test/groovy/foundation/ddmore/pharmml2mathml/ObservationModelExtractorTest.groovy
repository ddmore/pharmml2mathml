/*
 *  Copyright (c) 2017.  DDMoRe Foundation, Utrecht, NL.
 *
 *  Licensed under the Apache License, Version 2.0 (the
 *  "License"); you may not use this file except in
 *  compliance with the License.  You may obtain a copy of
 *  the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on
 *  an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 *  KIND, either express or implied. See the License for the
 *  specific language governing permissions and limitations
 *  under the License.
 *
 */

package foundation.ddmore.pharmml2mathml

import foundation.ddmore.pharmml08.dom.maths.BinaryOperator
import foundation.ddmore.pharmml08.dom.maths.UnaryOperator
import foundation.ddmore.pharmml08.dom.modeldefn.ModelDefinitionType
import foundation.ddmore.pharmml08.dom.modeldefn.TransformationType
import foundation.ddmore.pharmml08.dom.probonto.DistributionParameterType
import foundation.ddmore.pharmml08.dom.probonto.DistroNameType
import foundation.ddmore.pharmml08.dom.probonto.DistroType
import foundation.ddmore.pharmml08.dom.probonto.ParameterNameType
import foundation.ddmore.pharmml08.dom.probonto.ProbOntoType
import org.junit.Test

import static foundation.ddmore.pharmml08.builder.MathsBuilder.*
import static foundation.ddmore.pharmml08.builder.ProbontoBuilder.*
import static foundation.ddmore.pharmml08.builder.ParameterModelBuilder.*
import static foundation.ddmore.pharmml08.builder.ObservationModelBuilder.*
import static foundation.ddmore.pharmml08.builder.PharmMlBuilder.*
import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertNotNull
import static org.junit.Assert.assertNotNull
import static org.junit.Assert.assertNotNull

/**
 * Created by stumoodie on 05/01/2017.
 */
class ObservationModelExtractorTest {

    @Test
    void testContinuousObs(){
        def root = createModelDefinition(
                createObservationModel("om1",
                createContinuousObservation(
                        createStructuredObs("Y",
                                createSymbRefType("sm", "CC"),
                                createRhs(
                                        createFunctionCall(
                                                createSymbRefType("combinedError1"),
                                                createFunctionArgument("additive", createSymbRef("pm", "RUV_ADD"))
                                        )
                                ),
                                createSymbRefType("pm", "EPS_Y")
                        )
                )
        )
        )

        def actual = ObservationModelExtractor.extractObservationModels(root)

        assertEquals(1, actual.observationModels.size())

        def defnList = actual.observationModels.head()

        def expectedDefns = [
                [
                        symbId : "Y",
                        maths : "<mrow><mi>Y</mi><mo>=</mo><mi>sm.CC</mi><mo>+</mo><mi>combinedError1</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>additive</mi><mo>=</mo><mi>pm.RUV_ADD</mi></mrow></mfenced><mo>+</mo><mi>pm.EPS_Y</mi></mrow>",
                        type : "structured"
                ]
        ]

        assertEquals("om1", defnList.blkId)

        for(int i; i < expectedDefns.size(); i++){
            assertEquals(expectedDefns[i].symbId, defnList.continuousObs.observation.symbId)
            assertEquals(expectedDefns[i].maths, defnList.continuousObs.observation.maths)
            assertEquals(expectedDefns[i].type, defnList.continuousObs.observation.type)
        }

    }


    @Test
    void testContinuousObsWithParametersAndCorrelation(){
        def root = createModelDefinition(
                createObservationModel("om1",
                        createContinuousObservation(
                                createStructuredObs("Y",
                                        createSymbRefType("sm", "CC"),
                                        createRhs(
                                                createFunctionCall(
                                                        createSymbRefType("combinedError1"),
                                                        createFunctionArgument("additive", createSymbRef("pm", "RUV_ADD"))
                                                )
                                        ),
                                        createSymbRefType("pm", "EPS_Y")
                                ),
                                createPopulationParam("a"),
                                createPopulationParam("b"),
                                createRandomVariable(
                                        "eps_a",
                                        createDistribution(
                                            createProbonto(DistroNameType.NORMAL_1, DistroType.UNIVARIATE,
                                                createDistnParameter(ParameterNameType.MEAN, createRhs(createReal(0))),
                                                createDistnParameter(ParameterNameType.VAR, createRhs(createReal(1.0)))
                                            ),
                                        ),
                                        [ createVariabilityRef("pm", "obs") ]
                                ),
                                createRandomVariable(
                                        "eps_b",
                                        createDistribution(
                                            createProbonto(DistroNameType.NORMAL_1, DistroType.UNIVARIATE,
                                                createDistnParameter(ParameterNameType.MEAN, createRhs(createReal(0))),
                                                createDistnParameter(ParameterNameType.VAR, createRhs(createReal(1.0)))
                                            ),
                                        ),
                                        [ createVariabilityRef("pm", "obs") ]
                                ),
                                createRvCorrelationCorr(
                                        createRhs(
                                                createSymbRef("rho")
                                        ),
                                        createSymbRefType("eps_a"),
                                        createSymbRefType("eps_b")
                                )
                        )
                )
        )

        def actual = ObservationModelExtractor.extractObservationModels(root)

        assertEquals(1, actual.observationModels.size())
        assertEquals(4, actual.observationModels[0].continuousObs.parameters.size())
        assertEquals(1, actual.observationModels[0].continuousObs.correlations.size())

        def defnList = actual.observationModels.head()

        def expectedParams = [
                [
                        symbId : "a",
                        maths : "<mrow><mi>a</mi></mrow>",
                        type : "popParam"
                ],
                [
                        symbId : "b",
                        maths : "<mrow><mi>b</mi></mrow>",
                        type : "popParam"
                ],
                [
                        symbId : "eps_a",
                        maths : "<mrow><msub><mi>eps_a</mi><mrow><mfenced open=\"\" close=\"\"><mrow><mi>pm.obs</mi></mrow></mfenced></mrow></msub><mo>~</mo><mrow><mi>Normal1</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>mean</mi><mo>=</mo><mn>0.0</mn></mrow><mrow><mi>var</mi><mo>=</mo><mn>1.0</mn></mrow></mfenced></mrow></mrow>",
                        type : "rvParam"
                ],
                [
                        symbId : "eps_b",
                        maths : "<mrow><msub><mi>eps_b</mi><mrow><mfenced open=\"\" close=\"\"><mrow><mi>pm.obs</mi></mrow></mfenced></mrow></msub><mo>~</mo><mrow><mi>Normal1</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>mean</mi><mo>=</mo><mn>0.0</mn></mrow><mrow><mi>var</mi><mo>=</mo><mn>1.0</mn></mrow></mfenced></mrow></mrow>",
                        type : "rvParam"
                ]
        ]

        def expectedCorrs = [
                [
                        maths : "<mrow><mi>corr</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>eps_a</mi></mrow><mrow><mi>eps_b</mi></mrow></mfenced><mo>=</mo><mi>rho</mi></mrow>",
                        type : "corr"
                ]
        ]
        def expectedDefns = [
                [
                        symbId : "Y",
                        maths : "<mrow><mi>Y</mi><mo>=</mo><mi>sm.CC</mi><mo>+</mo><mi>combinedError1</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>additive</mi><mo>=</mo><mi>pm.RUV_ADD</mi></mrow></mfenced><mo>+</mo><mi>pm.EPS_Y</mi></mrow>",
                        type : "structured"
                ]
        ]

        assertEquals("om1", defnList.blkId)

        for(int i; i < expectedDefns.size(); i++){
            assertEquals(expectedDefns[i].symbId, defnList.continuousObs.observation.symbId)
            assertEquals(expectedDefns[i].maths, defnList.continuousObs.observation.maths)
            assertEquals(expectedDefns[i].type, defnList.continuousObs.observation.type)
        }

        for(int i; i < expectedParams.size(); i++){
            assertEquals(expectedParams[i].symbId, defnList.continuousObs.parameters[i].symbId)
            assertEquals(expectedParams[i].maths, defnList.continuousObs.parameters[i].maths)
            assertEquals(expectedParams[i].type, defnList.continuousObs.parameters[i].type)
        }

        for(int i; i < expectedCorrs.size(); i++){
            assertEquals(expectedCorrs[i].symbId, defnList.continuousObs.correlations[i].symbId)
            assertEquals(expectedCorrs[i].maths, defnList.continuousObs.correlations[i].maths)
            assertEquals(expectedCorrs[i].type, defnList.continuousObs.correlations[i].type)
        }
    }


    @Test
    void testContinuousObsGeneral(){
        def root = createModelDefinition(
                createObservationModel("om1",
                        createContinuousObservation(
                                createGeneralObs("Y",
                                        createRhs(
                                                createSymbRef("pm", "EPS_Y")
                                        )
                                )
                        )
                )
        )

        def actual = ObservationModelExtractor.extractObservationModels(root)

        assertEquals(1, actual.observationModels.size())

        def defnList = actual.observationModels.head()

        def expectedDefns = [
                [
                        symbId : "Y",
                        maths : "<mrow><mi>Y</mi><mo>=</mo><mi>pm.EPS_Y</mi></mrow>",
                        type : "general"
                ]
        ]

        assertEquals("om1", defnList.blkId)

        for(int i; i < expectedDefns.size(); i++){
            assertEquals(expectedDefns[i].symbId, defnList.continuousObs.observation.symbId)
            assertEquals(expectedDefns[i].maths, defnList.continuousObs.observation.maths)
            assertEquals(expectedDefns[i].type, defnList.continuousObs.observation.type)
        }

    }


    @Test
    void testContinuousObsGeneralWithTrans(){
        def root = createModelDefinition(
                createObservationModel("om1",
                        createContinuousObservation(
                                createGeneralObs("Y",
                                        createRhs(
                                                createSymbRef("pm", "EPS_Y")
                                        ),
                                        TransformationType.LOG
                                )
                        )
                )
        )

        def actual = ObservationModelExtractor.extractObservationModels(root)

        assertEquals(1, actual.observationModels.size())

        def defnList = actual.observationModels.head()

        def expectedDefns = [
                [
                        symbId : "Y",
                        maths : "<mrow><mi>ln</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>Y</mi></mrow></mfenced><mo>=</mo><mi>pm.EPS_Y</mi></mrow>",
                        type : "general"
                ]
        ]

        assertEquals("om1", defnList.blkId)

        for(int i; i < expectedDefns.size(); i++){
            assertEquals(expectedDefns[i].symbId, defnList.continuousObs.observation.symbId)
            assertEquals(expectedDefns[i].maths, defnList.continuousObs.observation.maths)
            assertEquals(expectedDefns[i].type, defnList.continuousObs.observation.type)
        }

    }


    @Test
    void testContinuousMultiObs(){
        def root = createModelDefinition(
                createObservationModel("om1",
                        createContinuousObservation(
                                createStructuredObs("Y",
                                        createSymbRefType("sm", "CC"),
                                        createRhs(
                                                createFunctionCall(
                                                        createSymbRefType("combinedError1"),
                                                        createFunctionArgument("additive", createSymbRef("pm", "RUV_ADD"))
                                                )
                                        ),
                                        createSymbRefType("pm", "EPS_Y")
                                )
                        )
                ),
                createObservationModel("om2",
                        createContinuousObservation(
                                createStructuredObs("Z",
                                        createSymbRefType("sm", "DD"),
                                        createRhs(
                                                createFunctionCall(
                                                        createSymbRefType("combinedError1"),
                                                        createFunctionArgument("additive", createSymbRef("pm", "RUV_ADD"))
                                                )
                                        ),
                                        createSymbRefType("pm", "EPS_Z"),
                                        TransformationType.LOG
                                )
                        )
                )
        )

        def actual = ObservationModelExtractor.extractObservationModels(root)

        def defnList = actual.observationModels

        def expectedDefns = [
                [
                        blkId : "om1",
                        symbId : "Y",
                        maths : "<mrow><mi>Y</mi><mo>=</mo><mi>sm.CC</mi><mo>+</mo><mi>combinedError1</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>additive</mi><mo>=</mo><mi>pm.RUV_ADD</mi></mrow></mfenced><mo>+</mo><mi>pm.EPS_Y</mi></mrow>",
                        type : "structured"
                ],
                [
                        blkId : "om2",
                        symbId : "Z",
                        maths : "<mrow><mi>ln</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>Z</mi></mrow></mfenced><mo>=</mo><mi>ln</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>sm.DD</mi></mrow></mfenced><mo>+</mo><mi>combinedError1</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>additive</mi><mo>=</mo><mi>pm.RUV_ADD</mi></mrow></mfenced><mo>+</mo><mi>pm.EPS_Z</mi></mrow>",
                        type : "structured"
                ]
        ]

        assertEquals(expectedDefns.size(), actual.observationModels.size())


        for(int i; i < expectedDefns.size(); i++){
            assertEquals(expectedDefns[i].blkId, defnList[i].blkId)
            assertEquals(expectedDefns[i].symbId, defnList[i].continuousObs.observation.symbId)
            assertEquals(expectedDefns[i].maths, defnList[i].continuousObs.observation.maths)
            assertEquals(expectedDefns[i].type, defnList[i].continuousObs.observation.type)
        }

    }


    @Test
    void testCountObs(){
        def root = createModelDefinition(
                createObservationModel("om1",
                        createDiscreteObservation(
                                createCountObs(
                                        "Y",
                                        createDistribution(
                                                createProbonto(DistroNameType.POISSON_1, DistroType.UNIVARIATE,
                                                        createDistnParameter(ParameterNameType.RATE, createRhs(createSymbRef("sm", "lambda")))
                                                )
                                        )
                                )
                        )
                )
        )

        def actual = ObservationModelExtractor.extractObservationModels(root)

        assertEquals(1, actual.observationModels.size())

        def defnList = actual.observationModels.head()

        def expectedDefns = [
                [
                        symbId : "Y",
                        maths : "<mrow><mi>Y</mi><mo>~</mo><mrow><mi>Poisson1</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>rate</mi><mo>=</mo><mi>sm.lambda</mi></mrow></mfenced></mrow></mrow>",
                        type : "count"
                ]
        ]

        assertEquals("om1", defnList.blkId)

        for(int i; i < expectedDefns.size(); i++){
            assertEquals(expectedDefns[i].symbId, defnList.discreteObs.observation.symbId)
            assertEquals(expectedDefns[i].maths, defnList.discreteObs.observation.maths)
            assertEquals(expectedDefns[i].type, defnList.discreteObs.observation.type)
        }

    }


    @Test
    void testCategoricalObs(){
        def root = createModelDefinition(
                createObservationModel("om1",
                    createDiscreteObservation(
                            createCategoricalObs("Y",
                                    createDistribution(
                                            createProbonto(DistroNameType.BERNOULLI_1, DistroType.UNIVARIATE,
                                                    createDistnParameter(ParameterNameType.PROBABILITY,
                                                            createRhs(createSymbRef("sm", "P1"))
                                                    )
                                            )
                                    ),
                                    "success", "fail"
                            )
                    )
                )
        )

        def actual = ObservationModelExtractor.extractObservationModels(root)

        assertEquals(1, actual.observationModels.size())

        def defnList = actual.observationModels.head()

        def expectedDefns = [
                [
                        symbId : "Y",
                        maths : "<mrow><mi>Y</mi><mo>~</mo><mrow><mi>Bernoulli1</mi><mo>&ApplyFunction;</mo><mfenced><mrow><mi>probability</mi><mo>=</mo><mi>sm.P1</mi></mrow></mfenced></mrow></mrow>",
                        type : "categorical"
                ]
        ]

        assertEquals("om1", defnList.blkId)

        for(int i; i < expectedDefns.size(); i++){
            assertEquals(expectedDefns[i].symbId, defnList.discreteObs.observation.symbId)
            assertEquals(expectedDefns[i].maths, defnList.discreteObs.observation.maths)
            assertEquals(expectedDefns[i].type, defnList.discreteObs.observation.type)
        }
    }


    @Test
    void testTteObs(){
        def root = createModelDefinition(
                createObservationModel("om1",
                    createDiscreteObservation(
                            createTTEObs(
                                    "Y",
                                    "HAZ",
                                    createRhs(
                                            createSymbRef("sm", "HAZ")
                                    )
                            )
                    )
                )
        )
        def actual = ObservationModelExtractor.extractObservationModels(root)

        assertEquals(1, actual.observationModels.size())

        def defnList = actual.observationModels.head()

        def expectedDefns = [
                [
                        symbId : "Y",
                        maths : "<mrow><mi>HAZ</mi><mo>=</mo><mi>sm.HAZ</mi></mrow>",
                        type : "tte"
                ]
        ]

        assertEquals("om1", defnList.blkId)

        for(int i; i < expectedDefns.size(); i++){
            assertEquals(expectedDefns[i].symbId, defnList.discreteObs.observation.symbId)
            assertEquals(expectedDefns[i].maths, defnList.discreteObs.observation.maths)
            assertEquals(expectedDefns[i].type, defnList.discreteObs.observation.type)
        }
    }
}
